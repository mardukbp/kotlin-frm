import java.net.URI

allprojects {
    buildscript {
        repositories {
	maven { url = URI("https://animusdesign-repository.appspot.com") }
            mavenCentral()
            mavenLocal()
            jcenter()
            google()
            maven { url = URI("https://kotlin.bintray.com/kotlinx") }
            maven { url = URI("https://dl.bintray.com/kotlin/kotlin-js-wrappers") }
            maven { url = URI("https://kotlin.bintray.com/kotlinx") }
            maven { url = URI("https://maven.google.com") }
            maven { url = URI("https://plugins.gradle.org/m2/") }
            maven { url = URI("http://dl.bintray.com/kotlin/kotlin-dev") }
            maven { url = URI("https://dl.bintray.com/kotlin/kotlin-eap") }
            maven { url = URI("https://maven.fabric.io/public") }
            maven { url = URI("https://nexus.animus.design/repository/AnimusDesignSnapshot/") }
            maven { url = URI("https://nexus.animus.design/repository/AnimusDesign/") }
        }
    }


    repositories {
	maven { url = URI("https://animusdesign-repository.appspot.com") }
        mavenCentral()
        jcenter()
        google()
        maven { url = URI("https://kotlin.bintray.com/kotlinx") }
        maven { url = URI("https://dl.bintray.com/kotlin/kotlin-js-wrappers") }
        maven { url = URI("https://kotlin.bintray.com/kotlinx") }
        maven { url = URI("https://maven.google.com") }
        maven { url = URI("https://plugins.gradle.org/m2/") }
        maven { url = URI("http://dl.bintray.com/kotlin/kotlin-dev") }
        maven { url = URI("https://dl.bintray.com/kotlin/kotlin-eap") }
        mavenLocal()
    }

    allprojects.filter { it.name.contains("sql") }.forEach { project ->
        println(project.name)
        project.subprojects {
            apply("$rootDir/versions.gradle.kts")
            apply("$rootDir/gradle/common/publishing-repos.gradle.kts")
            apply(plugin = "maven-publish")
            group = "${extra["baseGroup"]}.sql"
            version = "${extra["projectVersion"]}"
        }
    }
}

