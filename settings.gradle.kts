import java.net.URI

enableFeaturePreview("GRADLE_METADATA")
rootProject.name = "kotlin-frm"

pluginManagement {
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id.contains("org.flywaydb.flyway")) {
                useVersion("6.0.7")
            }
            if (requested.id.id.startsWith("org.jetbrains.dokka")) {
                // Update Version Build Source if being changed.
                useVersion("0.10.0")
            }
            if (requested.id.id.startsWith("org.jetbrains.kotlin.") || requested.id.id.startsWith("org.jetbrains.kotlin.plugin.serialization") ) {
                // Update Version Build Source if being changed.
                useVersion("1.3.61")
            }
        }
    }
    repositories {
        maven { url = java.net.URI("https://dl.bintray.com/kotlin/kotlinx") }
        mavenCentral()
        mavenLocal()
        jcenter()
        gradlePluginPortal()
    }
}


// Query DSL  FRM
include(":sql:query:query-common:")
include(":sql:query:query-postgresql:")
include(":sql:query:query-mariadb:")

// Schema Builder
include(":sql:schema:schema-common:")
include(":sql:schema:schema-postgresql:")
include(":sql:schema:schema-mariadb:")

// Query Compiler
//include(":sql:query:query-compiler:")

// Executors
include(":sql:executors:jvm:jasync-base")
include(":sql:executors:jvm:jasync-postgres")
include(":sql:executors:jvm:jasync-mysql")
include(":sql:executors:jvm:vertx-base")
include(":sql:executors:jvm:vertx-postgres")
include(":sql:executors:jvm:vertx-mysql")

// Examples
//include(":sql:example:jvm:JaSyncMariaDB")
//include(":sql:example:jvm:JaSyncPostgres")
//include(":sql:example:jvm:VertxPostgres")
//include(":sql:example:jvm:VertxMariaDB")

//include(":sql:benchmarks:jvm:jasync-postgres")

