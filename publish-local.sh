#!/usr/bin/env bash
./gradlew clean
./gradlew :sql:query:query-common:publishToMavenLocal
./gradlew :sql:query:query-postgresql:publishToMavenLocal
./gradlew :sql:query:query-mariadb:publishToMavenLocal
./gradlew :sql:schema:schema-common:publishToMavenLocal
./gradlew :sql:schema:schema-postgresql:publishToMavenLocal
./gradlew :sql:schema:schema-mariadb:publishToMavenLocal
