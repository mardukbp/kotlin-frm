@file:Suppress("UnstableApiUsage")
import kotlinx.benchmark.gradle.*
import org.jetbrains.kotlin.allopen.gradle.*
import org.jetbrains.kotlin.gradle.tasks.*

plugins {
    kotlin("jvm")
    id("design.animus.kotlin.frm.sql.schema.postgresql") version "0.1.0-SNAPSHOT"
    id("org.flywaydb.flyway") version "6.0.7"
    kotlin("plugin.allopen") version "1.3.60"
    id("kotlinx.benchmark") version "0.2.0-dev-6"
}
configure<AllOpenExtension> {
    annotation("org.openjdk.jmh.annotations.State")
}
repositories {
    maven("https://dl.bintray.com/kotlin/kotlinx")
}


kotlin {
    target {

    }

    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated", "benchmarks")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx.benchmark.runtime:0.2.0-dev-6")
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
                implementation(project(":sql:executors:jvm:jasync-postgres:"))
                implementation("io.github.microutils:kotlin-logging:1.7.6")
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.2")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.13.0")
                implementation("org.postgresql:postgresql:42.2.8")
                implementation(project(":sql:query:query-common:"))
                implementation("com.github.jasync-sql:jasync-postgresql:1.0.11")
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit5"))
                implementation(kotlin("test"))
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
                implementation(project(":sql:executors:jvm:jasync-postgres:"))
                implementation("com.github.jasync-sql:jasync-postgresql:1.0.11")

            }
        }
    }
}

val dbHost = System.getenv("pgDataBaseHost") ?: "localhost"
val db = System.getenv("pgDataBase") ?: "postgres"
val dbPort = (System.getenv("pgDataBasePort") ?: "5432").toInt()
val dbUser = System.getenv("pgDataBaseUser") ?: "postgres"
val dbPassword = System.getenv("pgDataBasePassword") ?: "postgres"

flyway {
    url = "jdbc:postgresql://$dbHost/$db?user=$dbUser&password=$dbPassword"
    schemas = arrayOf("example")
    locations = arrayOf("filesystem:$projectDir/main/resources")
}

databaseGeneratorConfig {
    dataBases = listOf(db)
    dataBaseHost = dbHost
    dataBaseUser = dbUser
    dataBasePassword = dbPassword
    dataBasePort = dbPort
    namespace = "design.animus.kotlin.frm.sql.example.jvm.postgres.generated"
    outputDir = "$projectDir/generated"
    dataBaseSchema = "example"
    platforms = setOf(design.animus.kotlin.frm.sql.schema.common.JVM)
    excludeTables = setOf("flyway_schema_history")
}

benchmark {
    configurations {
        named("main") {
            iterationTime = 5
            iterationTimeUnit = "sec"
            iterations = 10
            warmups = 2
        }
    }
    targets {
        register("main") {
            this as JvmBenchmarkTarget
            jmhVersion = "1.21"
        }
    }
}
