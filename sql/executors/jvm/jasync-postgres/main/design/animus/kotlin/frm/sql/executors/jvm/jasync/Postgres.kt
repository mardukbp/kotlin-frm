package design.animus.kotlin.frm.sql.executors.jvm.jasync

import com.github.jasync.sql.db.asSuspending
import com.github.jasync.sql.db.postgresql.PostgreSQLConnectionBuilder
import design.animus.kotlin.frm.executors.jvm.jasync.base.AJaSyncBase
import kotlin.reflect.full.primaryConstructor

typealias SQLQuery = String

@Suppress("UNCHECKED_CAST")
class JaSyncPostgresExecutor : AJaSyncBase<JaSyncPostgresConfig>() {
    // Connection
    override suspend fun connect(config: JaSyncPostgresConfig): Boolean {
        val baseUrl = "jdbc:postgresql://${config.databaseHost}:${config.databasePort}/${config.database}"
        try {
            println("Establishing connection to $baseUrl")
            println("$baseUrl?user=${config.databaseUser}&password=${config.databasePassword}")
            logger.debug { "Establishing connection to $baseUrl" }
            val pool = PostgreSQLConnectionBuilder.createConnectionPool(
                "$baseUrl?user=${config.databaseUser}&password=${config.databasePassword}"
            ).asSuspending
            connectionPool = pool
            logger.debug { "Connection pool -> $baseUrl created" }
            connected = true
            return true
        } catch (e: Exception) {
            logger.error(e) { "Failed to establish connection to database due to: ${e.message}" }
            throw e
        }
    }

    override suspend fun disconnect() = try {
        logger.debug { "Disconnecting from database." }
        connectionPool.disconnect()
        logger.debug { "Disconnected from database." }
        true
    } catch (e: Exception) {
        logger.error(e) { "Failed to disconnect to database due to: ${e.message}" }
        false
    }
    // End Connection
}
