package design.animus.kotlin.frm.executors.jvm.jasync.base

import com.github.jasync.sql.db.QueryResult
import com.github.jasync.sql.db.ResultSet
import com.github.jasync.sql.db.RowData
import com.github.jasync.sql.db.SuspendingConnection
import design.animus.kotlin.frm.sql.executors.common.*
import design.animus.kotlin.frm.sql.executors.common.exceptions.EmptyResultSet
import design.animus.kotlin.frm.sql.executors.common.exceptions.NotConnected
import design.animus.kotlin.frm.sql.executors.common.exceptions.TooManyResults
import design.animus.kotlin.frm.sql.query.common.builder.SQLQuery
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.NamedTable
import design.animus.kotlin.frm.sql.query.common.query.Table
import design.animus.kotlin.frm.sql.query.common.query.type.*
import design.animus.kotlin.frm.sql.query.common.query.verbs.IDeleteQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlinx.coroutines.withContext
import mu.KotlinLogging
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor

@Suppress("UNCHECKED_CAST")
abstract class AJaSyncBase<C : IExecutorConfig> : IAsyncExecutor<C, QueryResult> {
    lateinit var connectionPool: SuspendingConnection
    override val logger = KotlinLogging.logger {}
    override var connected = false
    override val preparedPlaceHolder = SimpleCharacter("?")

    // Begin Helpers
    /**
     * @throws NotConnected When executor is not connected to the database.
     */
    private suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, RSP>
            sanityCheck(query: IQuery<D, R, T>, block: suspend (SQLQuery) -> RSP) = if (!connected) {
        throw NotConnected("Cannot execute query not connected to the database.")
    } else {
        val sqlQuery = query.buildSQLString()
        logger.debug { "Built SQL Query of: $sqlQuery" }
        block(sqlQuery)
    }

    private suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, RSP>
            preparedSanityCheck(query: IPreparedQuery<D, R, T>, block: suspend (SQLQuery) -> RSP) = if (!connected) {
        throw NotConnected("Cannot execute query not connected to the database.")
    } else {
        val sqlQuery = query.buildSQLString(this.preparedPlaceHolder)
        logger.debug { "Built SQL Query of: $sqlQuery" }
        block(sqlQuery)
    }

    private suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, RSP>
            savedSanityCheck(query: ISaveQuery<D, R, T>, block: suspend (SQLQuery) -> RSP) = if (!connected) {
        throw NotConnected("Cannot execute query not connected to the database.")
    } else {
        val sqlQuery = query.buildSQLString()
        logger.debug { "Built SQL Query of: $sqlQuery" }
        block(sqlQuery)
    }

    private fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> getTable(query: IBaseQuery<D, R, T>) =
            when (query.table) {
                is Table<*, *, *> -> (query.table as Table<*, *, *>).table
                is NamedTable<*, *, *> -> (query.table as NamedTable<*, *, *>).table
            }

    private suspend fun <R> validateSingleRow(rows: ResultSet, block: suspend (RowData) -> R): R {
        return when {
            rows.size > 1 -> {
                logger.warn { "Expecting one result but found more than one, there were ${rows.size} rows returned. Taking the first" }
                throw TooManyResults("Expecting one result but found more than one, there were ${rows.size} rows returned. Taking the first")
            }
            rows.size < 1 -> {
                logger.error { "No rows returned" }
                throw EmptyResultSet("Query returned an empty result set.")
            }
            else -> block(rows.first())
        }
    }
    // End Helpers

    // Prepared Fetch
    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN, TargetClass : Any, RSP> preparedFetchAsync(
            preparedQuery: IPreparedQuery<D, R, T>,
            responseType: ExpectedResponses,
            values: BoundArguments<D, R, T>,
            ctx: CoroutineContext,
            block: suspend (IN) -> RSP
    ): AsyncDBResponse<D, R, T, C, QueryResult, RSP> = withContext(ctx) {
        val sqlQuery = preparedQuery.buildSQLString(this@AJaSyncBase.preparedPlaceHolder)
        val passedValues = values.map { it.expectedValue }
        logger.debug { "Built prepared sql query of: $sqlQuery with values: $passedValues" }
        val queryResult = connectionPool.sendPreparedStatement(
                sqlQuery,
                passedValues
        )
        GenericWrapper<D, R, T, C, QueryResult, IN, TargetClass, RSP>(
                queryResult,
                this@AJaSyncBase,
                responseType,
                preparedQuery,
                block
        )
    }
    // End Prepared Fetch

    // Single Response
    @Throws(NotConnected::class)
    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN, TargetClass : Any, RSP> fetchAsync(
            query: IQuery<D, R, T>,
            responseType: ExpectedResponses,
            ctx: CoroutineContext,
            block: suspend (IN) -> RSP
    ): AsyncDBResponse<D, R, T, C, QueryResult, RSP> = withContext(ctx) {
        sanityCheck(query) { sqlQuery ->
            val queryResponse = connectionPool.sendQuery(sqlQuery)
            GenericWrapper<D, R, T, C, QueryResult, IN, TargetClass, RSP>(
                    queryResponse,
                    this@AJaSyncBase,
                    responseType,
                    query,
                    block
            )
        }
    }


    @Throws(EmptyResultSet::class, TooManyResults::class, NotConnected::class)
    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN>
            unpackDBFuture(result: QueryResult, query: IBaseQuery<D, R, T>): IN {
        val table = getTable(query)
        val rows = result.rows
        return validateSingleRow(rows) {
            val rowAsMap = (table.reflectedColumns zip it.toList()).toMap()
            logger.debug { "Constructed row into a HashMap of: $rowAsMap" }
            val rsp = table.reflectedRecordClass.primaryConstructor!!.call(rowAsMap)
            logger.debug { "Cast to a Record of $rsp" }
            rsp
        } as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN> unpackDBFutures(
            result: QueryResult,
            query: IBaseQuery<D, R, T>
    ): IN {
        val table = getTable(query)
        return result.rows.map { row ->
            table.reflectedRecordClass.primaryConstructor!!.call(
                    (table.reflectedColumns zip row.toList()).toMap()
            )
        } as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN> unpackDBFutureAsMap(
            result: QueryResult,
            query: IBaseQuery<D, R, T>
    ): IN {
        return validateSingleRow(result.rows) { row ->

            val columns = getColumnNames(query)

            (columns zip row.toList()).toMap()
        } as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN> unpackDBFutureAsMultiMap(
            result: QueryResult,
            query: IBaseQuery<D, R, T>
    ): IN {
        val table = getTable(query)
        return result.rows.map { row ->
            val columns = getColumnNames(query)
            (columns zip row.toList()).toMap()
        } as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, TargetClass : Any, IN, RSP> unpackDBFutureAsCustomClass(
            result: QueryResult,
            targetClass: KClass<TargetClass>,
            query: IBaseQuery<D, R, T>
    ): IN {
        val table = getTable(query)
        return validateSingleRow(result.rows) { row ->
            val columns = getColumnNames(query)
            targetClass.primaryConstructor!!.call(
                    (columns zip row.toList()).toMap()
            )
        } as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, TargetClass : Any, IN, RSP> unpackDBFutureAsListOfCustomClass(
            result: QueryResult,
            targetClass: KClass<TargetClass>,
            query: IBaseQuery<D, R, T>
    ): IN {
        val table = getTable(query)
        return result.rows.map { row ->
            val columns = getColumnNames(query)
            targetClass.primaryConstructor!!.call(
                    (columns zip row.toList()).toMap()
            )
        } as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
            save(query: ISaveQuery<D, R, T>)
            : AsyncDBResponse<D, R, T, C, QueryResult, Boolean> =
            sanityCheck(query) { sqlQuery ->
                val queryResponse = connectionPool.sendQuery(sqlQuery)
                GenericWrapper<D, R, T, C, QueryResult, Boolean, Any, Boolean>(
                        queryResponse, this, Save, query, { _ -> true }
                )
            }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, TargetClass : Any, IN, RSP> unpackDBSave(
            result: QueryResult
    ): IN {
        val response = result
        return true as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, TargetClass : Any, IN, RSP>
            unpackDBDelete(
            result: QueryResult,
            query: IDeleteQuery<D, R, T>
    ): IN {
        val response = result
        return true as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
            delete(query: IDeleteQuery<D, R, T>)
            : AsyncDBResponse<D, R, T, C, QueryResult, Boolean> =
            sanityCheck(query) { sqlQuery ->
                val queryResponse = connectionPool.sendQuery(sqlQuery)
                GenericWrapper<D, R, T, C, QueryResult, Boolean, Any, Boolean>(
                        queryResponse, this, Delete, query, { true }
                )
            }
}