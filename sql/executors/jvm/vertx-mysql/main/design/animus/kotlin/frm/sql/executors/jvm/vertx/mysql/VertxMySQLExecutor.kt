package design.animus.kotlin.frm.sql.executors.jvm.vertx.mysql

import design.animus.kotlin.frm.sql.executors.jvm.vertx.base.AVertxBase
import design.animus.kotlin.frm.sql.query.common.query.type.NumberedCharacter
import design.animus.kotlin.frm.sql.query.common.query.type.SimpleCharacter
import io.vertx.core.Vertx
import io.vertx.kotlin.mysqlclient.mySQLConnectOptionsOf
import io.vertx.kotlin.sqlclient.poolOptionsOf
import io.vertx.mysqlclient.MySQLPool

class VertxMySQLExecutor(vertx: Vertx) : AVertxBase<VertxMySQLConfig, MySQLPool>(vertx) {
    override val preparedPlaceHolder = SimpleCharacter("?")
    // Connect
    override suspend fun connect(config: VertxMySQLConfig) = try {
        val connectOptions = mySQLConnectOptionsOf(
            database = config.database,
            host = config.databaseHost,
            password = config.databasePassword,
            port = config.databasePort,
            user = config.databaseUser
        )
        val poolOptions = poolOptionsOf(
            maxSize = config.poolSize
        )
        val client = MySQLPool.pool(vertx, connectOptions, poolOptions)
        logger.info { "Established connection to database: Host:${config.databaseHost} DB:${config.database}" }
        connectionPool = client
        connected = true
        true
    } catch (e: Exception) {
        logger.error(e) { "Encountered an exception connection to: ${config.databaseHost}/${config.database} of: ${e.message}" }
        throw e
    }

    override suspend fun disconnect() = try {
        connectionPool.close()
        true
    } catch (e: Exception) {
        logger.error(e) { "Encountered error while disconnecting/closing pool: ${e.message}" }
        false
    }
    // End Connect
}
