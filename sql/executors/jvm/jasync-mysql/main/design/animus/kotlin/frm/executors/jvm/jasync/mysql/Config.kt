package design.animus.kotlin.frm.executors.jvm.jasync.mysql

import design.animus.kotlin.frm.sql.executors.common.IExecutorConfig

data class JaSyncMySQLConfig(
    override val databaseHost: String,
    override val database: String,
    override val databaseUser: String,
    override val databasePassword: String,
    override val databasePort: Int
) : IExecutorConfig