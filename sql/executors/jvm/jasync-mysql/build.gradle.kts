import design.animus.kotlin.frm.Versions.Dependencies

plugins {
    kotlin("jvm")
    id("maven-publish")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

kotlin {
    target {
        mavenPublication {
            artifactId = "executor-jasync-mysql"
        }
    }
    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-mariadb:"))
                implementation(project(":sql:executors:jvm:jasync-base:"))
                implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Dependencies.serialization}")
                implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
                implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
                implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
                implementation("com.github.jasync-sql:jasync-common:${Dependencies.jasync}")
                implementation("com.github.jasync-sql:jasync-mysql:${Dependencies.jasync}")
                implementation("org.mariadb.jdbc:mariadb-java-client:${Dependencies.mariaDBDriver}")
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-mariadb:"))
            }
        }
    }
}