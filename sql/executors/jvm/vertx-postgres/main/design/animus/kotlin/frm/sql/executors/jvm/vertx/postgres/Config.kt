package design.animus.kotlin.frm.sql.executors.jvm.vertx.postgres

import design.animus.kotlin.frm.sql.executors.common.IExecutorConfig

data class VertxPostgresConfig(
    override val databaseHost: String,
    override val database: String,
    override val databaseUser: String,
    override val databasePassword: String,
    override val databasePort: Int,
    val schema: String,
    val poolSize: Int = 25
) : IExecutorConfig
