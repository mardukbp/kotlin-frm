package design.animus.kotlin.frm.sql.executors.jvm.vertx.postgres

import design.animus.kotlin.frm.sql.executors.jvm.vertx.base.AVertxBase
import design.animus.kotlin.frm.sql.query.common.query.type.NumberedCharacter
import io.vertx.core.Vertx
import io.vertx.kotlin.pgclient.pgConnectOptionsOf
import io.vertx.kotlin.sqlclient.poolOptionsOf
import io.vertx.pgclient.PgPool
import io.vertx.sqlclient.Row
import io.vertx.sqlclient.RowSet
import kotlin.reflect.full.primaryConstructor

typealias SQLQuery = String
typealias VertxResponse = RowSet<Row>

@Suppress("UNCHECKED_CAST", "UNCHECKED_CAST")
class VertxPostgresExecutor(vertx: Vertx) : AVertxBase<VertxPostgresConfig, PgPool>(vertx) {
    override val preparedPlaceHolder = NumberedCharacter(1, """$""")
    // Connect
    override suspend fun connect(config: VertxPostgresConfig) = try {
        val connectOptions = pgConnectOptionsOf(
            port = config.databasePort,
            host = config.databaseHost,
            database = config.database,
            user = config.databaseUser,
            password = config.databasePassword
        )
        val poolOptions = poolOptionsOf(
            maxSize = config.poolSize
        )
        val client = PgPool.pool(vertx, connectOptions, poolOptions)
        logger.info { "Established connection to database: Host:${config.databaseHost} DB:${config.database}" }
        connectionPool = client
        connected = true
        true
    } catch (e: Exception) {
        logger.error(e) { "Encountered an exception connection to: ${config.databaseHost}/${config.database} of: ${e.message}" }
        throw e
    }

    override suspend fun disconnect() = try {
        connectionPool.close()
        true
    } catch (e: Exception) {
        logger.error(e) { "Encountered error while disconnecting/closing pool: ${e.message}" }
        false
    }
    // End Connect
}
