package design.animus.kotlin.frm.sql.executors.jvm.vertx.base

import design.animus.kotlin.frm.sql.executors.common.*
import design.animus.kotlin.frm.sql.executors.common.exceptions.EmptyResultSet
import design.animus.kotlin.frm.sql.executors.common.exceptions.NotConnected
import design.animus.kotlin.frm.sql.executors.common.exceptions.TooManyResults
import design.animus.kotlin.frm.sql.executors.common.exceptions.UnexpectedException
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.NamedTable
import design.animus.kotlin.frm.sql.query.common.query.Table
import design.animus.kotlin.frm.sql.query.common.query.type.*
import design.animus.kotlin.frm.sql.query.common.query.verbs.IDeleteQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import io.vertx.core.Vertx
import io.vertx.kotlin.sqlclient.getConnectionAwait
import io.vertx.kotlin.sqlclient.preparedQueryAwait
import io.vertx.kotlin.sqlclient.queryAwait
import io.vertx.sqlclient.*
import mu.KotlinLogging
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor


typealias SQLQuery = String
typealias VertxResponse = RowSet<Row>

abstract class AVertxBase<C : IExecutorConfig, P : Pool>(val vertx: Vertx) : IAsyncExecutor<C, VertxResponse> {
    lateinit var connectionPool: P
    override val logger = KotlinLogging.logger {}
    override var connected = false

    // Helpers
    private fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> getTable(query: IBaseQuery<D, R, T>) =
            when (query.table) {
                is Table<*, *, *> -> (query.table as Table<*, *, *>).table
                is NamedTable<*, *, *> -> (query.table as NamedTable<*, *, *>).table
            }

    private fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> getTable(query: LockedQuery<D, R, T>) =
            when (query.table) {
                is Table<*, *, *> -> (query.table as Table<*, *, *>).table
                is NamedTable<*, *, *> -> (query.table as NamedTable<*, *, *>).table
            }

    private suspend fun <R> validateSingleRow(rows: List<List<Any?>>, block: suspend (List<Any?>) -> R): R {
        return when {
            rows.size > 1 -> {
                logger.warn { "Expecting one result but found more than one, there were ${rows.size} rows returned. Taking the first" }
                throw TooManyResults("Expecting one result but found more than one, there were ${rows.size} rows returned. Taking the first")
            }
            rows.isEmpty() -> {
                logger.error { "No rows returned" }
                throw EmptyResultSet("Query returned an empty result set.")
            }
            else -> block(rows.first())
        }
    }

    private suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, RSP>
            withConnection(query: IQuery<D, R, T>, block: suspend (SqlConnection, SQLQuery) -> RSP) =
            if (!connected) {
                logger.warn { "Attempted to query the database, but not connected." }
                throw NotConnected("Could not run query not connected to database.")
            } else {
                lateinit var conn: SqlConnection
                try {
                    conn = connectionPool.getConnectionAwait()
                    logger.debug { "Grabbed connection from pool" }
                    val sqlQuery = query.buildSQLString()
                    logger.debug { "Built SQL Query of: $sqlQuery" }
                    block(conn, sqlQuery)
                } catch (e: Exception) {
                    logger.error(e) { "Encountered an unanticipated error while making a query: $e.message" }
                    throw UnexpectedException(e.message ?: "", e)
                } finally {
                    conn.close()
                    logger.debug { "Closing sql connection'" }
                }
            }

    private suspend fun pullOutAsyncResult(sqlQuery: String, conn: SqlConnection): RowSet<Row> {
        return conn.queryAwait(sqlQuery)
    }

    private suspend fun rowToList(row: Row): List<Any?> {
        val items = mutableListOf<Any?>()
        (0 until row.size()).forEach { idx ->
            items.add(row.getValue(idx))
        }
        return items.toList()
    }
    // End Helpers

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN, TargetClass : Any, RSP> fetchAsync(
            query: IQuery<D, R, T>,
            responseType: ExpectedResponses,
            ctx: CoroutineContext,
            block: suspend (IN) -> RSP
    ): AsyncDBResponse<D, R, T, C, VertxResponse, RSP> = withConnection(query) { conn, sqlQuery ->
        GenericWrapper<D, R, T, C, VertxResponse, IN, TargetClass, RSP>(
                pullOutAsyncResult(sqlQuery, conn), this, responseType, query, block
        )
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN, TargetClass : Any, RSP> preparedFetchAsync(
            preparedQuery: IPreparedQuery<D, R, T>,
            responseType: ExpectedResponses,
            values: BoundArguments<D, R, T>,
            ctx: CoroutineContext,
            block: suspend (IN) -> RSP
    ): AsyncDBResponse<D, R, T, C, VertxResponse, RSP> {
        val sqlQuery = preparedQuery.buildSQLString(this.preparedPlaceHolder)
        val passedValues = values.map { it.expectedValue }
        logger.debug { "Built prepared sql query of: $sqlQuery with values: $passedValues" }
        val items = Tuple.wrap(passedValues.toMutableList())
        val queryResult = connectionPool.preparedQueryAwait(
                sqlQuery,
                items
        )
        return GenericWrapper<D, R, T, C, VertxResponse, IN, TargetClass, RSP>(
                queryResult,
                this,
                responseType,
                preparedQuery,
                block
        )
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> save(query: ISaveQuery<D, R, T>)
            : AsyncDBResponse<D, R, T, C, VertxResponse, Boolean> =
            withConnection(query) { conn, sqlQuery ->
                val queryResponse = pullOutAsyncResult(sqlQuery, conn)
                GenericWrapper<D, R, T, C, VertxResponse, Boolean, Any, Boolean>(
                        queryResponse, this, Save, query, { _ -> true }
                )
            }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> delete(query: IDeleteQuery<D, R, T>)
            : AsyncDBResponse<D, R, T, C, VertxResponse, Boolean> =
            withConnection(query) { conn, sqlQuery ->
                val queryResponse = pullOutAsyncResult(sqlQuery, conn)
                GenericWrapper<D, R, T, C, VertxResponse, Boolean, Any, Boolean>(
                        queryResponse, this, Delete, query, { true }
                )
            }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN> unpackDBFuture(
            result: VertxResponse,
            query: IBaseQuery<D, R, T>
    ): IN {
        val table = getTable(query)
        val rowAsList = result.map { row -> rowToList(row) }
        logger.debug { "Received ${result.rowCount()} rows from the database." }
        logger.debug { "Received ${rowAsList.size} rows from the database." }
        return validateSingleRow(rowAsList) {
            val rowAsMap = (table.reflectedColumns zip it).toMap()
            logger.debug { "Constructed row into a HashMap of: $rowAsMap" }
            val rsp = table.reflectedRecordClass.primaryConstructor!!.call(rowAsMap)
            logger.debug { "Cast to a Record of $rsp" }
            rsp as IN
        }
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN> unpackDBFutures(
            result: VertxResponse,
            query: IBaseQuery<D, R, T>
    ): IN {
        val table = getTable(query)
        return result.map { row ->
            table.reflectedRecordClass.primaryConstructor!!.call(
                    (table.reflectedColumns zip rowToList(row)).toMap()
            )
        } as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN> unpackDBFutureAsMap(
            result: VertxResponse,
            query: IBaseQuery<D, R, T>
    ): IN {

        val table = getTable(query)
        val rowAsList = result.map { row -> rowToList(row) }
        return validateSingleRow(rowAsList) { row ->
            val columns = getColumnNames(query)
            val data = (columns zip row).toMap()
            data
        } as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN> unpackDBFutureAsMultiMap(
            result: VertxResponse,
            query: IBaseQuery<D, R, T>
    ): IN {
        val table = getTable(query)
        return result.map { row ->
            val columns = getColumnNames(query)
            val data = (columns zip rowToList(row)).toMap()
            data
        } as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, TargetClass : Any, IN, RSP> unpackDBFutureAsCustomClass(
            result: VertxResponse,
            targetClass: KClass<TargetClass>,
            query: IBaseQuery<D, R, T>
    ): IN {
        val table = getTable(query)
        val rowAsList = result.map { row -> rowToList(row) }
        return validateSingleRow(rowAsList) { row ->
            val columns = getColumnNames(query)
            val data = (columns zip row).toMap()
            targetClass.primaryConstructor!!.call(
                    data
            ) as IN
        }
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, TargetClass : Any, IN, RSP> unpackDBFutureAsListOfCustomClass(
            result: VertxResponse,
            targetClass: KClass<TargetClass>,
            query: IBaseQuery<D, R, T>
    ): IN {
        val table = getTable(query)
        return result.map { row ->
            val columns = getColumnNames(query)
            targetClass.primaryConstructor!!.call(
                    (columns zip rowToList(row)).toMap()
            )
        } as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, TargetClass : Any, IN, RSP> unpackDBSave(
            result: VertxResponse
    ): IN {
        return true as IN
    }

    override suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, TargetClass : Any, IN, RSP> unpackDBDelete(
            result: VertxResponse,
            query: IDeleteQuery<D, R, T>
    ): IN {
        return true as IN
    }

}