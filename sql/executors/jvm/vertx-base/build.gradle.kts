import design.animus.kotlin.frm.Versions.Dependencies

plugins {
    kotlin("jvm")
    id("maven-publish")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

kotlin {
    target {
        mavenPublication {
            artifactId = "executor-vertx-base"
        }
    }
    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation(project(":sql:query:query-common:"))
                implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Dependencies.serialization}")
                implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
                implementation("org.postgresql:postgresql:${Dependencies.postgreSQL}")
                implementation("io.vertx:vertx-core:${Dependencies.vertx}")
                implementation("io.vertx:vertx-lang-kotlin:${Dependencies.vertx}")
                implementation("io.vertx:vertx-lang-kotlin-coroutines:${Dependencies.vertx}")
                implementation("io.vertx:vertx-mysql-client:${Dependencies.vertx}")
                implementation("io.vertx:vertx-pg-client:${Dependencies.vertx}")
                implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
                implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
            }
        }
    }
}