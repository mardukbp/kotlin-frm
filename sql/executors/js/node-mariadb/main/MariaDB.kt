//package design.animus.kotlin.frm.sql.executors.js.node.postgres
//
//import PoolConfig
//import QueryOptions
//import createPool
//import design.animus.kotlin.frm.sql.executors.common.AsyncDBResponse
//import design.animus.kotlin.frm.sql.executors.common.IAsyncExecutor
//import design.animus.kotlin.frm.sql.executors.common.IExecutorConfig
//import design.animus.kotlin.frm.sql.executors.common.NotConnected
//import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLQuery
//import design.animus.kotlin.frm.sql.query.common.builder.sqlQuery
//import design.animus.kotlin.frm.sql.query.common.query.Database
//import design.animus.kotlin.frm.sql.query.common.query.Field
//import design.animus.kotlin.frm.sql.query.common.query.ITable
//import design.animus.kotlin.frm.sql.query.common.query.PrimaryKey
//import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
//import kotlinx.coroutines.GlobalScope
//import kotlinx.coroutines.launch
//import mu.KotlinLogging
//
//
//data class NodeMariaDBConfig(
//        override val databaseHost: String,
//        override val database: String,
//        override val databaseUser: String,
//        override val databasePassword: String,
//        override val databasePort: Int,
//        val connectionPoolLimit: Number
//) : IExecutorConfig {
//    internal suspend fun toPoolConfig(config: NodeMariaDBConfig) = NodeMariaDBPoolConfig(
//            config.databaseHost,
//            config.databaseUser,
//            config.databasePassword,
//            config.connectionPoolLimit
//    )
//}
//
//internal data class NodeMariaDBPoolConfig(
//        override var host: String?,
//        override var user: String?,
//        override var password: String?,
//        override var connectionLimit: Number?,
//        override var nestTables: dynamic = null,
//        override var ssl: dynamic = null,
//        override var initSql: dynamic = null
//) : PoolConfig
//
//class NodeMariaDBExecutor : IAsyncExecutor<NodeMariaDBConfig, Any> {
//    override val logger = KotlinLogging.logger {}
//    override var connected = false
//    lateinit var pool: Pool
//
//    override suspend fun connect(config: NodeMariaDBConfig) {
//        logger.debug { "Establishing connection to maria db." }
//        pool = createPool(config.toPoolConfig())
//        logger.debug { "Connected to db pool." }
//    }
//
//    data class SQL(override var sql: String) : QueryOptions {
//        override var nestTables: dynamic = null
//
//    }
//
//    @Suppress("UNCHECKED_CAST")
//    override suspend fun <D : Database, R : ADatabaseRecord, T : ITable<D, R>, RSP> fetchAsync(query: CommonSQLQuery<D, R, T>, block: (R) -> RSP): AsyncDBResponse<D, R, T, NodeMariaDBConfig, Any, RSP> {
//        if (!connected) throw NotConnected("Not connected to the MariaDB database.")
//        val sqlString = SQL(query.buildSQLString())
//        val conn = pool.getConnection().await()
//        val rows = (conn.query(sqlString).await() as List<Map<String, Any>>).first()
//        println(rows)
//    }
//
//    override suspend fun <D : Database, R : ADatabaseRecord, T : ITable<D, R>, RSP> fetchMultipleAsync(query: CommonSQLQuery<D, R, T>, block: (List<R>) -> RSP): AsyncDBResponse<D, R, T, NodeMariaDBConfig, Any, RSP> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override suspend fun <D : Database, R : ADatabaseRecord, T : ITable<D, R>> unpackDBFuture(result: Any, query: CommonSQLQuery<D, R, T>): R {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override suspend fun <D : Database, R : ADatabaseRecord, T : ITable<D, R>> unpackDBFutures(result: Any, query: CommonSQLQuery<D, R, T>): List<R> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//}
//
//object ExampleDatabase : Database {
//    override val database = "example"
//    override val schema = "public"
//}
//
//object Users : ITable<ExampleDatabase, UsersRecord> {
//    override val recordClass = UsersRecord::class
//    override val table = this
//    override val name = "users"
//    override val database = ExampleDatabase
//    override val columns = listOf("id", "first_name", "last_name")
//    val id: PrimaryKey<Long, ExampleDatabase, UsersRecord, Users> = object : PrimaryKey<Long, ExampleDatabase, UsersRecord, Users>("id", "bigint", database.schema, name) {}
//    val firstName: Field<String, ExampleDatabase, UsersRecord, Users> = object : Field<String, ExampleDatabase, UsersRecord, Users>("first_name", "varchar(24)", database.schema, name) {}
//    val lastName: Field<String, ExampleDatabase, UsersRecord, Users> = object : Field<String, ExampleDatabase, UsersRecord, Users>("last_name", "varchar(24)", database.schema, name) {}
//
//}
//
//data class UsersRecord(
//        val map: Map<String, Any>
//) : ADatabaseRecord(map) {
//    val id: Long by map
//    val firstName by map
//}
//
//
//suspend fun main(args: Array<String>) = GlobalScope.launch {
//    val config = NodeMariaDBConfig(
//            "127.0.0.1", "example", "mariadb", "mariadb", 3306, 15
//    )
//    val executor = NodeMariaDBExecutor()
//    executor.connect(config)
//    println(executor.connected)
//    val query = sqlQuery<ExampleDatabase, UsersRecord, Users> {
//        select() from Users
//    }
//    val future = executor.fetchAsync(query) {
//        println(it)
//    }
//    future.fetch()
//}.join()
