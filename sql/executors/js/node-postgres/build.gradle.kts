plugins {
    kotlin("js")
}

kotlin {
    target {
        useCommonJs()
        nodejs()
    }

    sourceSets {
        main {
            kotlin.srcDir("main")
            resources.srcDir("main/resources")
            dependencies {
                implementation(project(":sql:query:query-common:"))
                implementation("io.github.microutils:kotlin-logging-js:1.7.6")
                implementation(kotlin("stdlib-js"))
                implementation(npm("pg-promise", "10.2.1"))
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-js"))
                implementation(kotlin("test"))
                implementation(npm("pg", "7.12.1"))

            }
        }
    }

}

repositories {
    mavenLocal()
}