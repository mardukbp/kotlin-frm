package design.animus.kotlin.frm.sql.executors.js.node.postgres

import design.animus.kotlin.frm.sql.executors.common.AsyncDBResponse
import design.animus.kotlin.frm.sql.executors.common.IAsyncExecutor
import design.animus.kotlin.frm.sql.executors.common.IExecutorConfig
import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLQuery
import design.animus.kotlin.frm.sql.query.common.query.Database
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import mu.KotlinLogging


data class NodePostgresConfig(
    override val databaseHost: String,
    override val database: String,
    override val databaseUser: String,
    override val databasePassword: String,
    override val databasePort: Int,
    val schema: String
) : IExecutorConfig

class NodePostgresExecutor : IAsyncExecutor<NodePostgresConfig, Any> {
    override val logger = KotlinLogging.logger {}
    override var connected = false

    override suspend fun connect(config: NodePostgresConfig) {
        TODO()
    }

    override suspend fun <D : Database, R : ADatabaseRecord, T : ITable<D, R>, RSP> fetchAsync(
        query: CommonSQLQuery<D, R, T>,
        block: (R) -> RSP
    ): AsyncDBResponse<D, R, T, NodePostgresConfig, Any, RSP> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun <D : Database, R : ADatabaseRecord, T : ITable<D, R>, RSP> fetchMultipleAsync(
        query: CommonSQLQuery<D, R, T>,
        block: (List<R>) -> RSP
    ): AsyncDBResponse<D, R, T, NodePostgresConfig, Any, RSP> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun <D : Database, R : ADatabaseRecord, T : ITable<D, R>> unpackDBFuture(
        result: Any,
        query: CommonSQLQuery<D, R, T>
    ): R {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun <D : Database, R : ADatabaseRecord, T : ITable<D, R>> unpackDBFutures(
        result: Any,
        query: CommonSQLQuery<D, R, T>
    ): List<R> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}