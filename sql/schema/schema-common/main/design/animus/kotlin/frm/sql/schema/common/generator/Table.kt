package design.animus.kotlin.frm.sql.schema.common.generator

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.kotlin.frm.sql.executors.common.AsyncDBResponse
import design.animus.kotlin.frm.sql.executors.common.IAsyncExecutor
import design.animus.kotlin.frm.sql.executors.common.IExecutorConfig
import design.animus.kotlin.frm.sql.query.common.SQLTypeProvider
import design.animus.kotlin.frm.sql.schema.common.GeneratedField
import design.animus.kotlin.frm.sql.schema.common.ISChemaGeneratorConfig
import design.animus.kotlin.frm.sql.schema.common.renameClass
import kotlin.coroutines.CoroutineContext

class SQLTableGenerator<C : ISChemaGeneratorConfig>(val typeProvider: SQLTypeProvider) {
    private val tableInterface =
        ClassName(typeProvider.tableInterface.packageName, typeProvider.tableInterface.className)

    // Helpers
    private suspend fun baseTableType(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        rawTableName: String,
        recordClass: ClassName,
        recordClassName: String,
        fields: List<GeneratedField>,
        lateFields: List<GeneratedField>
    ) = TypeSpec.objectBuilder(tableClassName)
        .addSuperinterface(tableInterface.parameterizedBy(databaseClass, recordClass))
        .addProperties(fields.map { field ->
            val fieldClassName = "${tableClassName}${renameClass(
                field.columnName
            )}"
            PropertySpec.builder(field.propertyName, field.fieldClass)
                .initializer("%L", fieldClassName)
                .build()
        }).addProperties(lateFields.map { field ->
            val fieldClassName = "${tableClassName}Lazy${renameClass(
                field.columnName
            )}"
            PropertySpec.builder(field.propertyName, field.fieldClass)
                .initializer("%L", fieldClassName)
                .build()
        })
        .addProperty(
            PropertySpec.builder("reflectedTableName", String::class)
                .initializer("%S", rawTableName)
                .addModifiers(KModifier.OVERRIDE)
                .build()
        )
        .addProperty(
            PropertySpec.builder("reflectedDatabase", databaseClass)
                .initializer("%L", databaseClass.simpleName)
                .addModifiers(KModifier.OVERRIDE)
                .build()
        )
        .addProperty(
            PropertySpec.builder("reflectedColumns", List::class.parameterizedBy(String::class))
                .initializer("%L", "listOf(${fields.joinToString(",") { "\"${it.propertyName}\"" }})")
                .addModifiers(KModifier.OVERRIDE)
                .build()
        )
        .addProperty(
            PropertySpec.builder(
                "reflectedRecordClass",
                ClassName("kotlin.reflect", "KClass").parameterizedBy(recordClass)
            )
                .initializer("%L", "${recordClassName}::class")
                .addModifiers(KModifier.OVERRIDE)
                .build()
        )
        .addProperty(
            PropertySpec.builder(
                "reflectedTable", tableInterface.parameterizedBy(databaseClass, recordClass)
            )
                .initializer("%L", "this")
                .addModifiers(KModifier.OVERRIDE)
                .build()
        )

        .addFunction(
            FunSpec.builder("getAsync")
                .addTypeVariable(TypeVariableName("C", IExecutorConfig::class.asTypeName()))
                .addTypeVariable(TypeVariableName("RSLT"))
                .addParameters(
                    fields.filter { it.primaryKey }.map { field ->
                        ParameterSpec("in${field.propertyName.capitalize()}", field.colType)
                    }
                )
                .addParameter(
                    "executor", IAsyncExecutor::class.asTypeName().parameterizedBy(
                        TypeVariableName("C"), TypeVariableName("RSLT")
                    )
                )
                .addParameter(
                    ParameterSpec.builder("ctx", CoroutineContext::class.asTypeName())
                        .defaultValue("%L", "Dispatchers.Default")
                        .build()
                )
                .addModifiers(KModifier.SUSPEND)
                .addCode("""
                    val query = lazySelect(${tableClassName})
                        .filter {
                            ${fields.filter { it.primaryKey }.joinToString(" and ") { pri ->
                    "(${pri.propertyName} equal in${pri.propertyName.capitalize()})"
                }}
                        }
                    return query.defer(executor)
                    
                """.trimIndent()
                )
                .returns(
                    AsyncDBResponse::class.asTypeName().parameterizedBy(
                        databaseClass,
                        recordClass,
                        TypeVariableName(tableClassName),
                        TypeVariableName("C"),
                        TypeVariableName("RSLT"),
                        recordClass
                    )
                )
                .build()
        )
    // End Helpers

    // Common
    suspend fun buildCommonTable(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        rawTableName: String,
        recordClassName: String,
        recordClass: ClassName,
        fields: List<GeneratedField>,
        lateFields: List<GeneratedField>
    ) =
        baseTableType(
            packageName,
            databaseClass,
            tableClassName,
            rawTableName,
            recordClass,
            recordClassName,
            fields,
            lateFields
        )
            .build()
    // End Common

    // Begin JVM
    suspend fun buildJVMTable(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        rawTableName: String,
        recordClassName: String,
        recordClass: ClassName,
        fields: List<GeneratedField>,
        lateFields: List<GeneratedField>,
        multiPlatform: Boolean
    ): TypeSpec? = if (multiPlatform) {
        null
    } else {
        val tableSpec = baseTableType(
            packageName, databaseClass,
            tableClassName, rawTableName, recordClass, recordClassName, fields, lateFields
        )
        tableSpec.build()
    }
    // End JVM

    // Begin JS
    suspend fun buildJSTable(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        rawTableName: String,
        recordClassName: String,
        recordClass: ClassName,
        fields: List<GeneratedField>,
        lateFields: List<GeneratedField>,
        multiPlatform: Boolean
    ): TypeSpec? =

        if (multiPlatform) {
            null
        } else {
            val tableSpec = baseTableType(
                packageName, databaseClass,
                tableClassName, rawTableName, recordClass, recordClassName, fields, lateFields
            )
            tableSpec.build()
        }
    // End JS
}