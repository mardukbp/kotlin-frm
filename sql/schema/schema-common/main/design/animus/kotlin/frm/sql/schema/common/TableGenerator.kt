package design.animus.kotlin.frm.sql.schema.common

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.ParameterizedTypeName
import com.squareup.kotlinpoet.TypeSpec

data class GeneratedField(
    val columnName: String,
    val propertyName: String,
    val fieldClass: ParameterizedTypeName,
    val type: TypeSpec,
    val primaryKey: Boolean,
    val colType: ClassName
)

interface ITableGenerator<C : ISChemaGeneratorConfig, S : IDBSchema, K : ISchemaKeyReflector> {


    suspend fun buildJVMTable(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        rawTableName: String,
        recordClassName: String,
        recordClass: ClassName,
        config: C,
        fields: List<GeneratedField>,
        lateFields: List<GeneratedField>,
        multiPlatform: Boolean
    ): TypeSpec?

    suspend fun buildJVMRecord(
        databaseClass: ClassName,
        tableClassName: String,
        rawTableName: String,
        recordClassName: String,
        recordClass: ClassName,
        config: C,
        cols: List<S>,
        keys: List<K>,
        multiPlatform: Boolean
    ): TypeSpec


    suspend fun buildJSRecord(
        databaseClass: ClassName,
        tableClassName: String,
        rawTableName: String,
        recordClassName: String,
        recordClass: ClassName,
        config: C,
        cols: List<S>,
        keys: List<K>,
        multiPlatform: Boolean
    ): TypeSpec?

    suspend fun buildJSTable(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        rawTableName: String,
        recordClassName: String,
        recordClass: ClassName,
        config: C,
        fields: List<GeneratedField>,
        lateFields: List<GeneratedField>,
        multiPlatform: Boolean
    ): TypeSpec?

    suspend fun buildCommonTable(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        rawTableName: String,
        recordClassName: String,
        recordClass: ClassName,
        config: C,
        fields: List<GeneratedField>,
        lateFields: List<GeneratedField>,
        multiPlatform: Boolean
    ): TypeSpec

    suspend fun buildCommonRecord(tableClassName: String): TypeSpec
}
