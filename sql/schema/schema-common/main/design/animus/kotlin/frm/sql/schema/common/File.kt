package design.animus.kotlin.frm.sql.schema.common

import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.TypeSpec
import design.animus.kotlin.frm.sql.query.common.*
import design.animus.kotlin.frm.sql.schema.common.utils.*

class CodeWriter<C : ISChemaGeneratorConfig> {
    private suspend fun writeGenericCode(
        packageName: String,
        fileName: String,
        config: C,
        platform: Platform,
        vararg types: TypeSpec
    ) {
        val file = FileSpec.builder(packageName, fileName)
        types.forEach {
            file.addType(it)
        }
        file.build().writeTo(
            when (platform) {
                JavaScript -> getJSMainPath(config)
                JVM -> getJVMMainPath(config)
                Common -> getCommonMainPath(config)
                Native -> getNativeMainPath(config)
            }
        )
    }

    private suspend fun writeSinglePlatformCode(
        packageName: String,
        fileName: String,
        config: C,
        platform: Platform,
        vararg types: TypeSpec
    ) {
        val file = FileSpec.builder(packageName, fileName)
            .addImport(
                "design.animus.kotlin.frm.sql.query.common.builder",
                listOf("sqlQuery", "query")
            )
            .addImport(
                "design.animus.kotlin.frm.sql.query.common.query",
                listOf("AND", "OR", "NOT")
            )
            .addImport(
                "design.animus.kotlin.frm.sql.query.common.frm",
                listOf("lazySelect", "lazyUpdate", "lazyInsert", "lazyDelete")
            )
            .addImport(
                "kotlinx.coroutines",
                listOf("Dispatchers")
            )
        types.forEach {
            file.addType(it)
        }
        file.build().writeTo(getSinglePlatform(config))
    }

    suspend fun platformWriter(
        packageName: String,
        fileName: String,
        config: C,
        vararg types: TypeSpec
    ) = if (config.platforms.size > 1) {
        writeGenericCode(packageName, fileName, config, Common, *types)
    } else {
        writeSinglePlatformCode(packageName, fileName, config, config.platforms.first(), *types)
    }

    suspend fun writeCommonCode(packageName: String, fileName: String, config: C, vararg types: TypeSpec) =
        writeGenericCode(packageName, fileName, config, Common, *types)

    suspend fun writeJVMCode(packageName: String, fileName: String, config: C, vararg types: TypeSpec) =
        writeGenericCode(packageName, fileName, config, JVM, *types)

    suspend fun writeJSCode(packageName: String, fileName: String, config: C, vararg types: TypeSpec) =
        writeGenericCode(packageName, fileName, config, JavaScript, *types)

    suspend fun writeNativeCode(packageName: String, fileName: String, config: C, vararg types: TypeSpec) =
        writeGenericCode(packageName, fileName, config, Native, *types)
}