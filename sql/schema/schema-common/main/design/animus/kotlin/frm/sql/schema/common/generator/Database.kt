package design.animus.kotlin.frm.sql.schema.common.generator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import design.animus.kotlin.frm.sql.query.common.SQLTypeProvider
import design.animus.kotlin.frm.sql.schema.common.CodeWriter
import design.animus.kotlin.frm.sql.schema.common.ISChemaGeneratorConfig
import design.animus.kotlin.frm.sql.schema.common.renameClass

suspend fun <C : ISChemaGeneratorConfig> generateDatabaseCode(
    db: String,
    config: C,
    typeProvider: SQLTypeProvider
): ClassName {
    val codeWriter = CodeWriter<C>()
    val packageName = "${config.namespace}.database.$db"
    val databaseClassName = renameClass(db)
    val databaseInterface =
        ClassName(typeProvider.dataBaseInterface.packageName, typeProvider.dataBaseInterface.className)
    val dbType = TypeSpec.Companion.objectBuilder(databaseClassName)
        .addSuperinterface(databaseInterface)
        .addProperty(
            PropertySpec.builder("database", String::class)
                .initializer("%S", db)
                .addModifiers(KModifier.OVERRIDE)
                .build()
        )
        .build()
    codeWriter.platformWriter(packageName, databaseClassName, config, dbType)
    return ClassName(packageName, databaseClassName)
}
