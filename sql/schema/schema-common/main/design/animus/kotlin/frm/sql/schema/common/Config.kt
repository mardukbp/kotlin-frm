package design.animus.kotlin.frm.sql.schema.common

import design.animus.kotlin.frm.sql.query.common.Platform

interface ISChemaGeneratorConfig {
    var dataBases: List<String>
    var dataBaseHost: String
    var dataBaseUser: String
    var dataBasePassword: String
    var dataBasePort: Int
    var namespace: String
    var outputDir: String
    var logLevel: String
    var platforms: Set<Platform>
    var excludeTables: Set<String>
}