package design.animus.kotlin.frm.sql.schema.common.records

import design.animus.kotlin.frm.sql.query.common.SQLType

typealias TableColumns = List<Column>

data class Column(
    val name: String,
    val type: SQLType,
    val position: Number,
    val defaultValue: Any?,
    val nullable: Boolean,
    val primaryKey: Boolean
)

data class TableSchema(
    val database: String,
    val tableName: String,
    val columns: TableColumns
)