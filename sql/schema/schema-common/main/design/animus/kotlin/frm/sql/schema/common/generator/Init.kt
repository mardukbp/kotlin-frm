package design.animus.kotlin.frm.sql.schema.common.generator


import com.squareup.kotlinpoet.ClassName
import design.animus.kotlin.frm.sql.query.common.*
import design.animus.kotlin.frm.sql.schema.common.CodeWriter
import design.animus.kotlin.frm.sql.schema.common.ISChemaGeneratorConfig
import design.animus.kotlin.frm.sql.schema.common.records.TableSchema
import design.animus.kotlin.frm.sql.schema.common.renameClass

suspend fun <C : ISChemaGeneratorConfig> generateSQLTableCode(
    databaseClass: ClassName,
    schema: TableSchema,
    customTypes: CustomTypes,
    typeProvider: SQLTypeProvider,
    config: C
) {
    val writer = CodeWriter<C>()
    val tableClassName = renameClass(schema.tableName)
    val packageName = "${config.namespace}.database.${schema.database}.tables"
    val recordClassName = "${tableClassName}Record"
    val recordClass = ClassName(packageName, recordClassName)
    val fieldGenerator = SQLFieldGenerator(
        config,
        packageName,
        typeProvider,
        databaseClass,
        recordClass,
        schema,
        tableClassName,
        customTypes
    )
    val lateFieldGenerator = SQLLateFieldGenerator(
        config,
        packageName,
        typeProvider,
        databaseClass,
        recordClass,
        schema,
        tableClassName,
        customTypes
    )
    val tableGenerator = SQLTableGenerator<C>(typeProvider)
    val recordGenerator = SQLRecordGenerator<C>(packageName, typeProvider, customTypes)
    if (config.platforms.size > 1) {
        if (config.platforms.contains(Common)) {
            val commonLateFields = lateFieldGenerator.buildCommonLateFields(schema.columns)
            val commonFields = fieldGenerator.buildCommonFields(schema.columns)
            val commonTable = tableGenerator.buildCommonTable(
                packageName,
                databaseClass,
                tableClassName,
                schema.tableName,
                recordClassName,
                recordClass,
                commonFields,
                commonLateFields
            )
            val commonRecord = recordGenerator.buildCommonRecord(tableClassName)
            writer.writeCommonCode(
                packageName,
                tableClassName,
                config,
                commonTable,
                *commonFields.map { it.type }.toTypedArray(),
                commonRecord,
                *commonLateFields.map { it.type }.toTypedArray()
            )
        }
        if (config.platforms.contains(JVM)) {
            val jvmLateFields = lateFieldGenerator.buildJVMLateFields(true)
            val jvmFields = fieldGenerator.buildJVMFields(true)
            val jvmRecord = recordGenerator.buildJVMRecord(tableClassName, schema, true)
            writer.writeJVMCode(packageName, tableClassName, config,
                *jvmFields.map { it.type }.toTypedArray(), jvmRecord, *jvmLateFields.map { it.type }.toTypedArray()
            )
        }
        if (config.platforms.contains(JavaScript)) {
            val jsLateFields = lateFieldGenerator.buildJSLateFields(true)
            val jsFields = fieldGenerator.buildJVMFields(true)
            val jsRecord = recordGenerator.buildJVMRecord(tableClassName, schema, true)
            writer.writeJSCode(packageName, tableClassName, config,
                *jsFields.map { it.type }.toTypedArray(), jsRecord, *jsLateFields.map { it.type }.toTypedArray()
            )
        }
        if (config.platforms.contains(Native)) {
            val nativeLateFields = lateFieldGenerator.buildNativeLateFields(true)
            val nativeFields = fieldGenerator.buildNativeFields(true)
            val nativeRecord = recordGenerator.buildNativeRecord(tableClassName, schema, true)
            writer.writeNativeCode(
                packageName,
                tableClassName,
                config,
                *nativeFields.map { it.type }.toTypedArray(),
                nativeRecord,
                *nativeLateFields.map { it.type }.toTypedArray()
            )
        }

    } else {
        when (config.platforms.first()) {
            JavaScript -> {
                val fields = fieldGenerator.buildJSFields(true)
                val lateFields = lateFieldGenerator.buildJSLateFields(true)
                val table = tableGenerator.buildJSTable(
                    packageName,
                    databaseClass,
                    tableClassName,
                    schema.tableName,
                    recordClassName,
                    recordClass,
                    fields,
                    lateFields,
                    false
                )
                val record = recordGenerator.buildJSRecord(tableClassName, schema, false)
                writer.platformWriter(packageName, tableClassName, config,
                    table!!, record, *fields.map { it.type }.toTypedArray(), *lateFields.map { it.type }.toTypedArray()
                )
            }
            JVM -> {
                val fields = fieldGenerator.buildJVMFields(false)
                val lateFields = lateFieldGenerator.buildJVMLateFields(false)
                val table = tableGenerator.buildJVMTable(
                    packageName,
                    databaseClass,
                    tableClassName,
                    schema.tableName,
                    recordClassName,
                    recordClass,
                    fields,
                    lateFields,
                    false
                )
                val record = recordGenerator.buildJVMRecord(tableClassName, schema, false)
                writer.platformWriter(packageName, tableClassName, config,
                    table!!, record, *fields.map { it.type }.toTypedArray(), *lateFields.map { it.type }.toTypedArray()
                )
            }
            Native -> {
                val nativeLateFields = lateFieldGenerator.buildNativeLateFields(false)
                val nativeFields = fieldGenerator.buildNativeFields(false)
                val nativeRecord = recordGenerator.buildNativeRecord(tableClassName, schema, false)
                writer.writeNativeCode(
                    packageName,
                    tableClassName,
                    config,
                    *nativeFields.map { it.type }.toTypedArray(),
                    nativeRecord,
                    *nativeLateFields.map { it.type }.toTypedArray()
                )
            }
            Common -> {
                println("Single platform of common")
                println("Which is not supported for single genration.")
            }
        }
    }
}
