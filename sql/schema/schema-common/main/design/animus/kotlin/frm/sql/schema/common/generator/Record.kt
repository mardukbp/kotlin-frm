package design.animus.kotlin.frm.sql.schema.common.generator

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.kotlin.frm.sql.query.common.*
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecordByMap
import design.animus.kotlin.frm.sql.schema.common.ISChemaGeneratorConfig
import design.animus.kotlin.frm.sql.schema.common.records.TableSchema
import design.animus.kotlin.frm.sql.schema.common.renameProperty

class SQLRecordGenerator<C : ISChemaGeneratorConfig>(
    private val packageName: String,
    private val typeProvider: SQLTypeProvider,
    private val customTypes: CustomTypes
) {
    private suspend fun baseRecordType(tableClassName: String, schema: TableSchema, platform: Platform) = TypeSpec
        .classBuilder("${tableClassName}Record")
        .superclass(
            ClassName(
                ADatabaseRecordByMap::class.qualifiedName!!.replace(".ADatabaseRecordByMap", ""),
                "ADatabaseRecordByMap"
            )
        )
        .addSuperclassConstructorParameter("%L", "map")
        .primaryConstructor(
            FunSpec.constructorBuilder()
                .addParameter("map", Map::class.parameterizedBy(String::class, Any::class))
                .build()
        )
        .addProperty(
            PropertySpec.builder("map", Map::class.parameterizedBy(String::class, Any::class))
                .initializer("map")
                .addModifiers(KModifier.PRIVATE)
                .build()
        ).addProperties(
            schema.columns.map { col ->
                val rawType = getRawType(col.type, platform, typeProvider, customTypes)
                PropertySpec.builder(
                    renameProperty(col.name),
                    ClassName(rawType.packageName, rawType.className)
                )
                    .delegate("%L", "map")
                    .build()
            }
        )

    // Common
    suspend fun buildCommonRecord(tableClassName: String) = TypeSpec
        .classBuilder("${tableClassName}Record")
        .addModifiers(KModifier.EXPECT)
        .addSuperinterface(
            ClassName(
                ADatabaseRecordByMap::class.qualifiedName!!.replace(".ADatabaseRecordByMap", ""),
                "ADatabaseRecordByMap"
            )
        )
        .build()

    // End Common
    // JVM
    suspend fun buildJVMRecord(
        tableClassName: String,
        schema: TableSchema,
        multiPlatform: Boolean
    ): TypeSpec {
        val base = baseRecordType(tableClassName, schema, JVM)
        if (multiPlatform) base.addModifiers(KModifier.ACTUAL)
        return base.build()
    }
    // End JVM

    // JS
    suspend fun buildJSRecord(
        tableClassName: String,
        schema: TableSchema,
        multiPlatform: Boolean
    ): TypeSpec {
        val base = baseRecordType(tableClassName, schema, JavaScript)
        if (multiPlatform) base.addModifiers(KModifier.ACTUAL)
        return base.build()
    }
    // End JS

    // Native
    suspend fun buildNativeRecord(
        tableClassName: String,
        schema: TableSchema,
        multiPlatform: Boolean
    ): TypeSpec {
        val base = baseRecordType(tableClassName, schema, Native)
        if (multiPlatform) base.addModifiers(KModifier.ACTUAL)
        return base.build()
    }
    // End Native
}