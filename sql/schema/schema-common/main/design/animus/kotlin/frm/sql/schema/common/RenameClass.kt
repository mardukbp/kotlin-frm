package design.animus.kotlin.frm.sql.schema.common

fun renameClass(clazz: String): String = clazz.split("_").joinToString("") {
    it.capitalize()
}