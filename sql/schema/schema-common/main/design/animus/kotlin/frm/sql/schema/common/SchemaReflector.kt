package design.animus.kotlin.frm.sql.schema.common

import com.github.jasync.sql.db.ConcreteConnection
import com.github.jasync.sql.db.pool.ConnectionPool

interface IDBSchema
interface IEnumSchema
interface ISchemaKeyReflector
interface ISchemaReflector<C : ISChemaGeneratorConfig, R : Any, E : Any> {
    suspend fun <T : ConcreteConnection> reflectSchema(conn: ConnectionPool<T>, config: C, database: String): List<R>
    suspend fun <T : ConcreteConnection> getCustomTypes(conn: ConnectionPool<T>, config: C, database: String): List<E>
}