package design.animus.kotlin.frm.sql.schema.common

import com.squareup.kotlinpoet.ClassName

interface IDatabaseGenerator<C : ISChemaGeneratorConfig> {
    suspend fun generateDatabaseCode(db: String, config: C): ClassName
}
