package design.animus.kotlin.frm.sql.schema.mariadb.reflector

import design.animus.kotlin.frm.sql.query.common.SQLType
import design.animus.kotlin.frm.sql.schema.common.records.Column
import design.animus.kotlin.frm.sql.schema.common.records.TableSchema

data class MariaDBInformationSchema(val map: Map<String, Any?>) {
    val tableCatalog: String by map
    val tableSchema: String by map
    val tableName: String by map
    val columnName: String by map
    val columnPosition: Long by map
    val columnDefault: String by map
    val nullable: String by map
    val dataType: SQLType by map
    val characterMaximumLength: Long by map
    val characterOctectLength: Long by map
    val numericPrecision: Long by map
    val numericScale: Long by map
    val dateTimePrecision: Long by map
    val characterSetName: String by map
    val collationName: String by map
    val columnType: String by map
    val columnKey: String by map
    val extra: String by map
    val privileges: String by map
    val columnComment: String by map
    val isGenerated: String by map
    val generationExpression: String by map
}

internal val mariaDBInformationSchemaColumns = listOf<String>(
    "tableCatalog",
    "tableSchema",
    "tableName",
    "columnName",
    "columnPosition",
    "columnDefault",
    "nullable",
    "dataType",
    "characterMaximumLength",
    "characterOctectLength",
    "numericPrecision",
    "numericScale",
    "dateTimePrecision",
    "characterSetName",
    "collationName",
    "columnType",
    "columnKey",
    "extra",
    "privileges",
    "columnComment",
    "isGenerated",
    "generationExpression"
)

suspend fun normalizeData(tables: List<MariaDBInformationSchema>) = tables.groupBy {
    it.tableName
}.map { (table, cols) ->
    val baseLine = cols.first()
    val tableColumns = cols.map { col ->
        Column(
            name = col.columnName,
            type = col.columnType
                .replace("""\(.*\)""".toRegex(), "")
                .replace("unsigned", "")
                .replace("\\s+".toRegex(), " ")
                .trim(),
            position = col.columnPosition,
            defaultValue = col.columnDefault,
            nullable = col.nullable.toLowerCase() == "yes",
            primaryKey = col.columnKey.contains("PRI")
        )
    }.sortedBy { it.position.toLong() }
    TableSchema(
        database = baseLine.tableSchema,
        tableName = baseLine.tableName,
        columns = tableColumns
    )
}