package design.animus.kotlin.frm.sql.schema.mariadb

import com.github.jasync.sql.db.mysql.MySQLConnectionBuilder

internal fun getMariaDBConnection(database: String, config: MariaDBConfiguration) =
    MySQLConnectionBuilder.createConnectionPool(
        "jdbc:mysql://${config.dataBaseHost}:${config.dataBasePort}/${database}?user=${config.dataBaseUser}&password=${config.dataBasePassword}"
    )