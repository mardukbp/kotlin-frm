package design.animus.kotlin.frm.sql.schema.mariadb.reflector

import com.github.jasync.sql.db.ConcreteConnection
import com.github.jasync.sql.db.pool.ConnectionPool
import design.animus.kotlin.frm.sql.schema.common.ISchemaReflector
import design.animus.kotlin.frm.sql.schema.mariadb.MariaDBConfiguration

object MariaDBReflector : ISchemaReflector<MariaDBConfiguration, MariaDBInformationSchema, Any> {
    override suspend fun <T : ConcreteConnection> reflectSchema(
        conn: ConnectionPool<T>,
        config: MariaDBConfiguration,
        database: String
    ): List<MariaDBInformationSchema> {
        val query = """
            select *
                from information_schema.columns 
                where information_schema.columns.TABLE_SCHEMA = '$database';
        """.trimIndent()
        val result = conn.sendQuery(query).get()
        return result.rows.map { r ->
            MariaDBInformationSchema((mariaDBInformationSchemaColumns zip r.toList()).toMap())
        }

    }

    override suspend fun <T : ConcreteConnection> getCustomTypes(
        conn: ConnectionPool<T>,
        config: MariaDBConfiguration,
        database: String
    ): List<Any> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}