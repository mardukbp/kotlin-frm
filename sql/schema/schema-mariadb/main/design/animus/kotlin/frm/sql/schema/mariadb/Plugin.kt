package design.animus.kotlin.frm.sql.schema.mariadb

import design.animus.kotlin.frm.sql.query.common.Common
import design.animus.kotlin.frm.sql.query.common.JVM
import design.animus.kotlin.frm.sql.query.common.JavaScript
import design.animus.kotlin.frm.sql.query.common.Platform
import design.animus.kotlin.frm.sql.query.mariadb.query.MariaDBTypeProvider
import design.animus.kotlin.frm.sql.schema.common.ISChemaGeneratorConfig
import design.animus.kotlin.frm.sql.schema.common.generator.generateDatabaseCode
import design.animus.kotlin.frm.sql.schema.common.generator.generateSQLTableCode
import design.animus.kotlin.frm.sql.schema.common.renameClass
import design.animus.kotlin.frm.sql.schema.mariadb.generator.generateEnumCode
import design.animus.kotlin.frm.sql.schema.mariadb.reflector.MariaDBReflector
import design.animus.kotlin.frm.sql.schema.mariadb.reflector.normalizeData
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.gradle.api.Plugin
import org.gradle.api.Project

val logger = KotlinLogging.logger { }

open class MariaDBConfiguration : ISChemaGeneratorConfig {
    override var dataBases: List<String> = listOf()
    override var dataBaseHost: String = "localhost"
    override var dataBaseUser: String = "mariadb"
    override var dataBasePassword: String = "mariadb"
    override var dataBasePort: Int = 3306
    override var namespace: String = "design.animus.datasource.sql.generated"
    override var outputDir: String = "/tmp/mariadb"
    override var logLevel = "DEBUG"
    override var platforms: Set<Platform> = setOf(Common, JavaScript, JVM)
    override var excludeTables = setOf<String>()
}

class MariaDBSQLSchemaGenerator : Plugin<Project> {

    override fun apply(project: Project) {
        val extension =
            project.extensions.create<MariaDBConfiguration>("MariaDBGeneratorConfig", MariaDBConfiguration::class.java)
        project.task("checkConnection")
            .doLast { _ ->
                extension.dataBases.forEach { database ->
                    val connection = getMariaDBConnection(database, extension)
                    println("Database: $database => Connected => ${connection.isConnected()}")
                    connection.disconnect()
                }
            }
        project.task("buildSchema")
            .doLast { _ ->
                logger.info("Running build schema")
                extension.dataBases.forEach { db ->
                    runBlocking {
                        val conn = getMariaDBConnection(db, extension)
                        extension.dataBases.forEach { db ->
                            val rawSchema = MariaDBReflector
                                .reflectSchema(conn, extension, extension.dataBases.first())
                                .filter { !extension.excludeTables.contains(it.tableName) }
                            val schema = normalizeData(rawSchema)
                            val databaseClass = generateDatabaseCode(db, extension, MariaDBTypeProvider)
                            val customTypes = schema.map { it.columns }.flatten().filter {
                                it.type.contains("enum")
                            }.map { col ->
                                generateEnumCode(renameClass(col.name), col, extension)
                            }
                            println(customTypes)
                            schema.forEach { table ->
                                generateSQLTableCode(databaseClass, table, customTypes, MariaDBTypeProvider, extension)
                            }
                        }
                        conn.disconnect()
                    }
                }
            }
    }
}