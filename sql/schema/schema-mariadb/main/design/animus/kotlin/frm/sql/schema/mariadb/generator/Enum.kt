package design.animus.kotlin.frm.sql.schema.mariadb.generator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.TypeSpec
import design.animus.kotlin.frm.sql.query.common.CustomType
import design.animus.kotlin.frm.sql.query.common.query.ISQLEnum
import design.animus.kotlin.frm.sql.schema.common.CodeWriter
import design.animus.kotlin.frm.sql.schema.common.records.Column
import design.animus.kotlin.frm.sql.schema.common.renameClass
import design.animus.kotlin.frm.sql.schema.mariadb.MariaDBConfiguration

internal suspend fun buildEnumType(typeName: String, values: Column): TypeSpec {
    val enumSpec = TypeSpec.enumBuilder(typeName)
        .addSuperinterface(
            ClassName(
                ISQLEnum::class.qualifiedName!!.replace(".ISQLEnum", ""),
                ISQLEnum::class.simpleName!!
            )
        )
        .primaryConstructor(
            FunSpec.constructorBuilder()
                .addParameter("friendly", String::class)
                .build()
        )
    val enumValues = values.type.replace("enum(", "").replace(")", "").split(",")

    enumValues.forEach {
        val case = when {
            it.contains("-") -> it.replace("-", "").toUpperCase()
            it.contains("_") -> it.replace("_", "").toUpperCase()
            it.contains(" ") -> it.replace(" ", "").toUpperCase()
            else -> it.toUpperCase()
        }
        enumSpec.addEnumConstant(
            case,
            TypeSpec.anonymousClassBuilder()
                .addSuperclassConstructorParameter("%S", it.toLowerCase())
                .build()
        )
    }
    return enumSpec.build()
}

internal suspend fun generateEnumCode(
    typeName: String, values: Column,
    config: MariaDBConfiguration
): CustomType {
    val writer = CodeWriter<MariaDBConfiguration>()
    val enumClassName = renameClass(typeName)
    val packageName = "${config.namespace}.database.types.enum"
    val type = buildEnumType(enumClassName, values)
    writer.platformWriter(packageName, enumClassName, config, type)
    return CustomType(
        sqlType = typeName,
        packageName = packageName,
        className = enumClassName
    )
}