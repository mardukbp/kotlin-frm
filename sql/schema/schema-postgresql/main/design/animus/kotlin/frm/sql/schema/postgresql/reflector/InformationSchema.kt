package design.animus.kotlin.frm.sql.schema.postgresql.reflector

import design.animus.kotlin.frm.sql.schema.common.IDBSchema
import design.animus.kotlin.frm.sql.schema.common.records.Column
import design.animus.kotlin.frm.sql.schema.common.records.TableSchema

data class RawInformationSchema(val map: Map<String, Any?>) : IDBSchema {
    val database: String by map
    val schema: String by map
    val table: String by map
    val columnName: String by map
    val columnOrder: Int by map
    val columnType: String by map
    val columnNullable: Boolean by map
    val columnDefault: Any? by map
    val primaryKey: String? by map
}

internal val informationSchemaColumns = listOf(
    "database",
    "schema",
    "table",
    "columnName",
    "columnOrder",
    "columnNullable",
    "columnDefault",
    "columnType",
    "primaryKey"
)

suspend fun normalizeData(tables: List<RawInformationSchema>) = tables.groupBy { it.table }.map { (table, cols) ->
    val baseLine = cols.first()
    TableSchema(
        database = baseLine.database,
        tableName = baseLine.table,
        columns = cols.map { col ->
            val genCol = Column(
                name = col.columnName,
                type = col.columnType,
                position = col.columnOrder,
                defaultValue = col.columnDefault,
                nullable = col.columnNullable,
                primaryKey = col.primaryKey?.contains("PRIMARY KEY") ?: false
            )
            genCol
        }
    )
}
