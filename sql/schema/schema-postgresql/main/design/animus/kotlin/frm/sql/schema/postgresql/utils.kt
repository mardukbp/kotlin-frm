package design.animus.kotlin.frm.sql.schema.postgresql

import com.github.jasync.sql.db.postgresql.PostgreSQLConnectionBuilder
import design.animus.kotlin.frm.sql.schema.postgresql.reflector.RawInformationSchema


internal fun getPostgresConnection(database: String, config: PostgresSQLConfiguration) =
    PostgreSQLConnectionBuilder.createConnectionPool(
        "jdbc:postgresql://${config.dataBaseHost}:${config.dataBasePort}/${database}?user=${config.dataBaseUser}&password=${config.dataBasePassword}"
    )

/**
 * @author Animus Null
 * @since 0.1.0
 * @param schemaRows A list of the reflected schema information.
 * @param config The configuration of the Postgres SQL configuration builder.
 * @returns True on success, false on error
 */
internal suspend fun groupByTable(schemaRows: List<RawInformationSchema>, config: PostgresSQLConfiguration) = schemaRows
    .groupBy { it.schema }
    .map { (_, items) ->
        items.groupBy { it.table }.map { (_, columns) ->
            Pair(columns.first(), columns.sortedBy { it.columnOrder })
        }
    }.flatten()
