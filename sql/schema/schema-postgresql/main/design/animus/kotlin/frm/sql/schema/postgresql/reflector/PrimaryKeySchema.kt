package design.animus.kotlin.frm.sql.schema.postgresql.reflector

import design.animus.kotlin.frm.sql.schema.common.ISchemaKeyReflector

data class PrimaryKeySchema(val map: Map<String, Any?>) : ISchemaKeyReflector {
    val tableSchema: String by map
    val tableName: String by map
    val constraintName: String by map
    val position: Int by map
    val columnName: String by map
    val constraintType: String by map
}

internal val primaryKeyColumns = listOf(
    "tableSchema",
    "tableName",
    "constraintName",
    "position",
    "columnName",
    "constraintType"
)