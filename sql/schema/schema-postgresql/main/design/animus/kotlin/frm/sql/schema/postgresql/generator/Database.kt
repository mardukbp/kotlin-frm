package design.animus.kotlin.frm.sql.schema.postgresql.generator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresDatabase
import design.animus.kotlin.frm.sql.schema.common.CodeWriter
import design.animus.kotlin.frm.sql.schema.common.IDatabaseGenerator
import design.animus.kotlin.frm.sql.schema.common.renameClass
import design.animus.kotlin.frm.sql.schema.postgresql.PostgresSQLConfiguration


object DatabaseGenerator : IDatabaseGenerator<PostgresSQLConfiguration> {
    override suspend fun generateDatabaseCode(db: String, config: PostgresSQLConfiguration): ClassName {
        val codeWriter = CodeWriter<PostgresSQLConfiguration>()
        val packageName = "${config.namespace}.database.$db"
        val databaseClassName = renameClass(db)
        val dbType = TypeSpec.Companion.objectBuilder(databaseClassName)
            .addSuperinterface(IPostgresDatabase::class)
            .addProperty(
                PropertySpec.builder("database", String::class)
                    .initializer("%S", db)
                    .addModifiers(KModifier.OVERRIDE)
                    .build()
            )
            .addProperty(
                PropertySpec.builder("schema", String::class)
                    .initializer("%S", config.dataBaseSchema)
                    .addModifiers(KModifier.OVERRIDE)
                    .build()
            )
            .build()
        codeWriter.platformWriter(packageName, databaseClassName, config, dbType)
        return ClassName(packageName, databaseClassName)
    }
}
