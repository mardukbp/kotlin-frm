package design.animus.kotlin.frm.sql.schema.postgresql.reflector

import com.github.jasync.sql.db.ConcreteConnection
import com.github.jasync.sql.db.pool.ConnectionPool
import design.animus.kotlin.frm.sql.schema.postgresql.PostgresSQLConfiguration

/**
 * Get the information schema for a given schema and database
 *
 * @author Animus Null
 * @since 0.1.0
 * @param schema The schema to scan
 * @param database The database to san
 * @param connection An open connection.
 */
suspend fun <T : ConcreteConnection> reflectSchema(
    conn: ConnectionPool<T>,
    config: PostgresSQLConfiguration,
    database: String
): List<RawInformationSchema> {
    val query = """
        SELECT information_schema.COLUMNS.table_catalog as database,
            information_schema.COLUMNS.table_schema as schema,
            information_schema.COLUMNS.table_name as table,
            information_schema.COLUMNS.column_name as column_name,
            information_schema.COLUMNS.ordinal_position as column_order,
            case
                when information_schema.COLUMNS.is_nullable = 'YES' then true 
                else false 
            end as column_nullable,
            information_schema.COLUMNS.column_default,
            information_schema.COLUMNS.udt_name as column_type,
            (select tco.constraint_type
                from information_schema.table_constraints as tco
                left outer join information_schema.key_column_usage kcu 
                    on kcu.column_name = information_schema.COLUMNS.column_name
                    and kcu.table_name = information_schema.COLUMNS.table_name
                where tco.table_name = information_schema.COLUMNS.table_name
                    and tco.constraint_name = kcu.constraint_name
                    and tco.constraint_schema = '${config.dataBaseSchema}'
                    and kcu.constraint_schema = '${config.dataBaseSchema}'
                    and tco.constraint_type = 'PRIMARY KEY'
            ) as primary_key
        from information_schema.COLUMNS
        where information_schema.COLUMNS.table_schema = '${config.dataBaseSchema}' and information_schema.COLUMNS.table_catalog = '$database'
        group by 
          	information_schema.COLUMNS.table_catalog,
          	information_schema.COLUMNS.table_schema,
          	information_schema.COLUMNS.table_name,
          	information_schema.COLUMNS.column_name,
          	information_schema.COLUMNS.ordinal_position,
          	information_schema.COLUMNS.is_nullable,
          	information_schema.COLUMNS.data_type,
            information_schema.COLUMNS.column_default,
          	information_schema.COLUMNS.udt_name,
          	primary_key
          order by
          	information_schema.COLUMNS.table_name,
          	information_schema.COLUMNS.ordinal_position
        """.trimIndent()
    println(query)
    val result = conn.sendQuery(query).get()
    return result.rows.map { r ->
        RawInformationSchema((informationSchemaColumns zip r.toList()).toMap())
    }
}


suspend fun <T : ConcreteConnection> getCustomTypes(
    conn: ConnectionPool<T>,
    config: PostgresSQLConfiguration,
    database: String
): List<EnumSchema> {
    val query = """
         SELECT pg_type.typname AS enumtype, 
            pg_enum.enumlabel AS enumlabel
            FROM pg_type 
            JOIN pg_enum ON pg_enum.enumtypid = pg_type.oid
    """.trimIndent()
    val result = conn.sendQuery(query).get()
    return result.rows.map { r ->
        EnumSchema((enumColumns zip r.toList()).toMap())
    }
}

