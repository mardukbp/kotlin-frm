package design.animus.kotlin.frm.sql.schema.postgresql.reflector

import design.animus.kotlin.frm.sql.schema.common.IEnumSchema

internal val enumColumns = listOf("enumtype", "enumlabel")

data class EnumSchema(val map: Map<String, Any?>) : IEnumSchema {
    val enumtype: String by map
    val enumlabel: String by map
}
