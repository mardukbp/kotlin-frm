import design.animus.kotlin.frm.Versions.Dependencies

plugins {
    kotlin("jvm")
    id("maven-publish")
    id("com.gradle.plugin-publish") version "0.10.1"
    id("java-gradle-plugin")

}

val artifact = "${project.group}.gradle.plugin"
version = "${extra["baseVersion"]}"

kotlin {
    target {
        mavenPublication {
            artifactId = "schema-postgresql"
        }
    }
    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
                implementation(project(":sql:schema:schema-common:"))
                implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dependencies.kotlin}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Dependencies.serialization}")
                implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
                implementation("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
                implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
                implementation("com.github.jasync-sql:jasync-postgresql:${Dependencies.jasync}")
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
            }
        }
    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

gradlePlugin {
    plugins {
        create("schemaPostgresPlugin") {
            id = "design.animus.kotlin.frm.sql.schema.postgresql"
            implementationClass = "design.animus.kotlin.frm.sql.schema.postgresql.PostgresSQLSchemaGenerator"
        }
    }
}

pluginBundle {
    website = "http://kotlin-frm.animus.design"
    vcsUrl = "https://gitlab.com/AnimusDesign/kotlin-frm"
    description = "Kotlin functional relational database layer with multi platform support."
    tags = listOf("kotlin", "database", "multiplatform")
}
