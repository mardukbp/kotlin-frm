package design.animus.kotlin.frm.sql.example.jvm.vertx.mariadb

import design.animus.kotlin.frm.sql.executors.jvm.vertx.mysql.VertxMySQLConfig

internal val FRMTestConfig = VertxMySQLConfig(
    System.getenv("myDataBaseHost"),
    "example_vertx",
    System.getenv("myDataBaseUser"),
    System.getenv("myDataBasePassword"),
    3306
)