package design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres

import design.animus.kotlin.frm.sql.query.common.builder.query
import design.animus.kotlin.frm.sql.query.common.builder.sqlQuery
import design.animus.kotlin.frm.sql.query.common.frm.lazyDelete
import design.animus.kotlin.frm.sql.query.common.frm.lazyInsert
import design.animus.kotlin.frm.sql.query.common.frm.lazySelect
import design.animus.kotlin.frm.sql.query.common.frm.lazyUpdate
import design.animus.kotlin.frm.sql.query.common.query.AND
import design.animus.kotlin.frm.sql.query.common.query.NOT
import design.animus.kotlin.frm.sql.query.common.query.OR
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresDatabase
import kotlin.String
import kotlinx.coroutines.Dispatchers

object Postgres : IPostgresDatabase {
  override val database: String = "postgres"

  override val schema: String = "jasync_example"
}
