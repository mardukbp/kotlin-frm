package design.animus.kotlin.frm.sql.example.jvm.postgres.jasync

import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.UserProfile
import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.Users
import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.UsersRecord
import design.animus.kotlin.frm.sql.executors.jvm.jasync.JaSyncPostgresExecutor
import design.animus.kotlin.frm.sql.query.postgres.builder.nestedQuery
import design.animus.kotlin.frm.sql.query.postgres.frm.lazySelect
import design.animus.kotlin.frm.sql.query.postgres.frm.prepared.lazyPreparedSelect
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertTrue

class NestedQueryTest {
    lateinit var executor: JaSyncPostgresExecutor

    @Before
    fun initialize() {
        runBlocking {
            executor = JaSyncPostgresExecutor()
            executor.connect(FRMTestConfig)
        }
    }

    @After
    fun tearDown() = runBlocking {
        executor.disconnect()
        Unit
    }

    @Test
    fun testNestedQueryInWhere() = runBlocking {
        val delayedQuery = lazySelect(UserProfile)
                .filter {
                    UserProfile.id contains Users.nestedQuery {
                        select(Users.id) from Users where {
                            Users.firstName iLike "%ja%"
                        }
                    }
                }
                .map(executor) { users ->
                    assertTrue { users.size == 3 }
                    val sortedUsers = users.sortedBy { it.id }
                    assertTrue { sortedUsers[0].email == "jdoe@google.com" }
                    assertTrue { sortedUsers[1].email == "jsmith@google.com" }
                    assertTrue { sortedUsers[2].email == "jblack@hotmail.com" }
                }
        delayedQuery.fetch()
        assertTrue { true }
    }

    @Test
    fun testNestedQueryInColumn() = runBlocking {
        val delayedQuery = lazySelect(UserProfile)
                .withColumns(UserProfile.id, UserProfile.email,
                        Users.nestedQuery { select(Users.firstName) from Users where { Users.id equal UserProfile.id } }.toField<UsersRecord, Users>(),
                        Users.nestedQuery { select(Users.lastName) from Users where { Users.id equal UserProfile.id } }.toField<UsersRecord, Users>()
                )
                .filter {
                    UserProfile.id equal 1
                }
                .mapOne(executor) { rec ->
                    assertTrue { rec[UserProfile.id.columnName] == 1 }
                    assertTrue { rec[UserProfile.email.columnName] == "jdoe@google.com" }
                    assertTrue { rec[Users.firstName.columnName] == "jane" }
                    assertTrue { rec[Users.lastName.columnName] == "doe" }
                }
        delayedQuery.fetch()
    }

    @Test
    fun testNestedPreparedQueryInWhere() = runBlocking {
        val delayedQuery = lazyPreparedSelect(UserProfile)
                .withColumns(UserProfile.id, UserProfile.email,
                        Users.nestedQuery { select(Users.firstName) from Users where { Users.id equal UserProfile.id } }.toField<UsersRecord, Users>(),
                        Users.nestedQuery { select(Users.lastName) from Users where { Users.id equal UserProfile.id } }.toField<UsersRecord, Users>()
                )
                .filter {
                    UserProfile.id equal UserProfile.lazyId
                }
                .mapOne(executor, 1) { rec ->
                    assertTrue { rec[UserProfile.id.columnName] == 1 }
                    assertTrue { rec[UserProfile.email.columnName] == "jdoe@google.com" }
                    assertTrue { rec[Users.firstName.columnName] == "jane" }
                    assertTrue { rec[Users.lastName.columnName] == "doe" }
                }
        delayedQuery.fetch()
    }

    @Test
    fun testPreparedQueryInWhere() = runBlocking {
        val delayedQuery = lazyPreparedSelect(UserProfile)
                .filter {

                    (UserProfile.id contains Users.nestedQuery {
                        select(Users.id) from Users where {
                            Users.firstName iLike Users.lazyFirstName
                        }
                    }) and (UserProfile.email equal UserProfile.lazyEmail)


                }
                .mapOne(executor, "%ja%", "jdoe@google.com") { user ->
                    assertTrue { user.id == 1 }
                    assertTrue { user.email == "jdoe@google.com" }
                }
        delayedQuery.fetch()
    }
}