package design.animus.kotlin.frm.sql.example.jvm.postgres.jasync

import design.animus.kotlin.frm.sql.executors.jvm.jasync.JaSyncPostgresConfig

internal val FRMTestConfig = JaSyncPostgresConfig(
    System.getenv("pgDataBaseHost") ?: "localhost",
    System.getenv("pgDataBase") ?: "postgres",
    System.getenv("pgDataBaseUser") ?: "postgres",
    System.getenv("pgDataBasePassword") ?: "postgres",
    5432,
    "jasync_example"
)
