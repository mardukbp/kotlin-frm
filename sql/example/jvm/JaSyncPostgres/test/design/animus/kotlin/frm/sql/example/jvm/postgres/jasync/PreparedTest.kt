package design.animus.kotlin.frm.sql.example.jvm.postgres.jasync

import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.UserProfile
import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.Users
import design.animus.kotlin.frm.sql.executors.common.exceptions.LazyConditionsNotEqualToArguments
import design.animus.kotlin.frm.sql.executors.jvm.jasync.JaSyncPostgresExecutor
import design.animus.kotlin.frm.sql.query.postgres.frm.prepared.*
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertTrue


class PreparedTest {
    lateinit var executor: JaSyncPostgresExecutor

    @Before
    fun initialize() {
        runBlocking {
            executor = JaSyncPostgresExecutor()
            executor.connect(FRMTestConfig)
        }
    }

    @After
    fun tearDown() = runBlocking {
        executor.disconnect()
        Unit
    }

    @Test
    fun testPreparedSelect() = runBlocking {
        val query = lazyPreparedSelect(Users).filter {
            (Users.firstName equal Users.lazyFirstName) and (Users.lastName equal Users.lazyLastName)
        }
        val delayedJaneDoe = query.mapOne(executor, "jane", "doe") {
            assertTrue { it.firstName == "jane" }
            "Hello ${it.firstName} ${it.lastName}"
        }
        val delayedJaneDoeList = query.map(executor, "jane", "doe") {
            assertTrue { it.size == 1 }
            val item = it.first()
            "Hello ${item.firstName} ${item.lastName}"
        }
        val helloJane = delayedJaneDoe.fetch()
        val hellJaneFromList = delayedJaneDoeList.fetch()
        assertTrue { helloJane == "Hello jane doe" }
        assertTrue { hellJaneFromList == "Hello jane doe" }
    }

    @Test
    fun testPreparedSelectJoin() = runBlocking {
        val delayedUserProfiles = lazyPreparedSelectJoin(Users)
            .withColumns(Users.id, Users.firstName, Users.lastName, UserProfile.bio)
            .join(UserProfile) {
                UserProfile.id equal Users.lazyId
            }
            .filter {
                Users.firstName iLike Users.lazyLastName
            }
        try {
            delayedUserProfiles.map(executor) { it.first() }.fetch()
            assertTrue { false }
        } catch (e: LazyConditionsNotEqualToArguments) {
            val expectedMessage =
                "Expecting 1 where conditions and 1 lazy join conditions, for a total of 2 but got 0 arguments"
            assertTrue { e.message == expectedMessage }
        }
        val rsp = delayedUserProfiles.map(executor, 1, "%ja%") {
            assertTrue { it.size == 3 }
            val jane = it.first { it.id == 1 }
            assertTrue { jane.firstName == "jane" && jane.lastName == "doe" }
        }
        rsp.fetch()
    }

    @Test
    fun testPreparedInsert() = runBlocking {
        val newUser = lazyPreparedInsert(Users)
            .add {
                Users.firstName to Users.lazyFirstName
                Users.lastName to Users.lazyLastName
            }
        val delayedNewUser = newUser.save(executor, "NewUser", "One").fetch()
        val delayedDeleteJane = lazyPreparedDelete(Users)
            .deleteOn {
                (Users.firstName equal Users.lazyFirstName) and (Users.lastName equal Users.lazyLastName)
            }.remove(executor, "NewUser", "One")

    }

    @Test
    fun testPreparedUpdate() = runBlocking {
        val newUser = lazyPreparedInsert(Users)
            .add {
                Users.firstName to Users.lazyFirstName
                Users.lastName to Users.lazyLastName
            }
        val delayedNewUser = newUser.save(executor, "UpdateUser", "One").fetch()
        val updatedUser = lazyPreparedUpdate(Users).mutateTo {
            Users.firstName TO Users.lazyFirstName
            Users.lastName TO Users.lazyLastName
        }.changeOn {
            (Users.firstName equal Users.lazyFirstName) and (Users.lastName equal Users.lazyLastName)
        }
        val rsp = updatedUser.save(executor, "Updated", "User", "UpdateUser", "One")
    }
}


