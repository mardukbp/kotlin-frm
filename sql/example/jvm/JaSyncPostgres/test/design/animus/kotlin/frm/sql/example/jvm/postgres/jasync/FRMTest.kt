package design.animus.kotlin.frm.sql.example.jvm.postgres.jasync

import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.UserProfile
import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.Users
import design.animus.kotlin.frm.sql.executors.common.exceptions.EmptyResultSet
import design.animus.kotlin.frm.sql.executors.jvm.jasync.JaSyncPostgresExecutor
import design.animus.kotlin.frm.sql.query.common.response.AJoinRecord
import design.animus.kotlin.frm.sql.query.postgres.frm.lazyDelete
import design.animus.kotlin.frm.sql.query.postgres.frm.lazyInsert
import design.animus.kotlin.frm.sql.query.postgres.frm.lazySelect
import design.animus.kotlin.frm.sql.query.postgres.frm.lazySelectJoin
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

data class UserProfileJoinRecord(val map: Map<String, Any?>) : AJoinRecord(map) {
    val id: Int by map
    val firstName: String by map
    val lastName: String by map
    val email: String by map
}

class FRMTest {
    lateinit var executor: JaSyncPostgresExecutor

    @Before
    fun initialize() {
        runBlocking {
            executor = JaSyncPostgresExecutor()
            executor.connect(FRMTestConfig)
        }
    }

    @After
    fun tearDown() = runBlocking {
        executor.disconnect()
        Unit
    }

    @Test
    fun selectForJA() = runBlocking {
        val result = lazySelect(Users)
                .filter {
                    (Users.firstName equal "jack") and (Users.lastName equal "smith")
                }
                .mapOne(executor) {
                    assertTrue { it.firstName == "jack" }
                    assertTrue { it.lastName == "smith" }
                }
        result.fetch()
    }

    @Test
    fun selectJoin() = runBlocking {
        val delayedResult = lazySelectJoin(Users)
                .withColumns(Users.id, Users.firstName, Users.lastName, UserProfile.email)
                .join(UserProfile) {
                    UserProfile.id equal Users.id
                }
                .filter { Users.firstName iLike "%ja%" }
                .map(executor, this.coroutineContext) {
                    val sorted = it.sortedBy { item -> item["id"] as Int }
                    assertTrue { sorted[0]["id"] == 1 && sorted[0]["email"] == "jdoe@google.com" }
                    assertTrue { sorted[1]["id"] == 2 && sorted[1]["email"] == "jsmith@google.com" }
                    assertTrue { sorted[2]["id"] == 4 && sorted[2]["email"] == "jblack@hotmail.com" }
                }
        delayedResult.fetch()
    }

    @Test
    fun selectJoinInto() = runBlocking {
        val delayedResult = lazySelectJoin(Users)
                .withColumns(Users.id, Users.firstName, Users.lastName, UserProfile.email)
                .join(UserProfile) {
                    UserProfile.id equal Users.id
                }
                .filter { Users.firstName iLike "%ja%" }
                .mapInto(executor, UserProfileJoinRecord::class) {
                    val sorted = it.sortedBy { item -> item.id }
                    assertTrue { sorted[0].id == 1 && sorted[0].email == "jdoe@google.com" }
                    assertTrue { sorted[1].id == 2 && sorted[1].email == "jsmith@google.com" }
                    assertTrue { sorted[2].id == 4 && sorted[2].email == "jblack@hotmail.com" }
                }
        val x = delayedResult.fetch()

    }


    @Test
    fun testDelete() = runBlocking {
        val x = assertFailsWith(EmptyResultSet::class) {
            val delayedInsertResult = lazyInsert(Users)
                    .addOne(listOf(Users.firstName to "Delete", Users.lastName to "Me"))
                    .save(executor)
                    .fetch()
            val delayedDeleteResult = lazyDelete(Users).deleteOn {
                (Users.firstName equal "Delete") and (Users.lastName equal "Me")
            }.remove(executor).fetch()
            val validateGone = lazySelect(Users).filter {
                (Users.firstName equal "Delete") and (Users.lastName equal "Me")
            }.defer(executor)
            val result = validateGone.fetch()
        }
    }
}