import design.animus.kotlin.frm.Versions.Dependencies

plugins {
    kotlin("jvm")
    id("org.jetbrains.kotlin.plugin.serialization")
    id("design.animus.kotlin.frm.sql.schema.postgresql") version "0.1.5-SNAPSHOT"
    id("org.flywaydb.flyway")
}

tasks.register("listrepos") {
    doLast {
        println("Repositories:")
        project.repositories.map{it as MavenArtifactRepository}
            .forEach{
            println("Name: ${it.name}; url: ${it.url}")
        }
    }
}

kotlin {
    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
                implementation(project(":sql:executors:jvm:jasync-base:"))
                implementation(project(":sql:executors:jvm:jasync-postgres:"))
                implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Dependencies.serialization}")
                implementation("org.postgresql:postgresql:${Dependencies.postgreSQL}")
                implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
                implementation(project(":sql:executors:jvm:jasync-postgres:"))
                implementation("com.github.jasync-sql:jasync-postgresql:${Dependencies.jasync}")
            }
        }
    }
}

val dbHost = System.getenv("pgDataBaseHost") ?: "localhost"
val db = System.getenv("pgDataBase") ?: "postgres"
val dbPort = (System.getenv("pgDataBasePort") ?: "5432").toInt()
val dbUser = System.getenv("pgDataBaseUser") ?: "postgres"
val dbPassword = System.getenv("pgDataBasePassword") ?: "postgres"

flyway {
    url = "jdbc:postgresql://$dbHost/$db?user=$dbUser&password=$dbPassword"
    schemas = arrayOf("jasync_example")
    locations = arrayOf("filesystem:$projectDir/main/resources")
}

PostgresGeneratorConfig {
    dataBases = listOf(db)
    dataBaseHost = dbHost
    dataBaseUser = dbUser
    dataBasePassword = dbPassword
    dataBasePort = dbPort
    namespace = "design.animus.kotlin.frm.sql.example.jvm.postgres.generated"
    outputDir = "$projectDir/generated"
    dataBaseSchema = "jasync_example"
    platforms = setOf(design.animus.kotlin.frm.sql.query.common.JVM)
    excludeTables = setOf("flyway_schema_history")
}

tasks.withType<Test>().configureEach {
    useJUnit()
    reports.junitXml.isEnabled = true
}
