package design.animus.kotlin.frm.sql.example.jvm.mysql.jasync

import design.animus.kotlin.frm.executors.jvm.jasync.mysql.JaSyncMySQLConfig


internal val FRMTestConfig = JaSyncMySQLConfig(
    System.getenv("myDataBaseHost") ?: "127.0.0.1",
    "example_jasync",
    System.getenv("myDataBaseUser") ?: "root",
    System.getenv("myDataBasePassword") ?: "mariadb",
    3306
)
