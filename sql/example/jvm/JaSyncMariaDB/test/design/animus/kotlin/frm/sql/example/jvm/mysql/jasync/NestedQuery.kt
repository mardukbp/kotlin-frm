package design.animus.kotlin.frm.sql.example.jvm.mysql.jasync

import design.animus.kotlin.frm.executors.jvm.jasync.mysql.JaSyncMySQLExecutor
import design.animus.kotlin.frm.sql.example.jvm.mysql.generated.database.example_jasync.tables.UserProfile
import design.animus.kotlin.frm.sql.example.jvm.mysql.generated.database.example_jasync.tables.Users
import design.animus.kotlin.frm.sql.example.jvm.mysql.generated.database.example_jasync.tables.UsersRecord
import design.animus.kotlin.frm.sql.query.common.builder.nestedQuery
import design.animus.kotlin.frm.sql.query.common.query.lower
import design.animus.kotlin.frm.sql.query.mariadb.frm.lazyPreparedSelect
import design.animus.kotlin.frm.sql.query.mariadb.frm.lazySelect
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertTrue

class NestedQueryTest {
    lateinit var executor: JaSyncMySQLExecutor

    @Before
    fun initialize() {
        runBlocking {
            println(FRMTestConfig)
            executor = JaSyncMySQLExecutor()
            executor.connect(FRMTestConfig)
        }
    }

    @After
    fun tearDown() = runBlocking {
        executor.disconnect()
        Unit
    }

    @Test
    fun testNestedQueryInWhere() = runBlocking {
        val delayedQuery = lazySelect(UserProfile)
                .filter {
                    UserProfile.id contains Users.nestedQuery {
                        select(Users.id) from Users where {
                            lower(Users.firstName) like "%ja%"
                        }
                    }
                }
                .map(executor) { users ->
                    assertTrue { users.size == 3 }
                    val sortedUsers = users.sortedBy { it.id }
                    assertTrue { sortedUsers[0].email == "jdoe@google.com" }
                    assertTrue { sortedUsers[1].email == "jsmith@google.com" }
                    assertTrue { sortedUsers[2].email == "jblack@hotmail.com" }
                }
        delayedQuery.fetch()
        assertTrue { true }
    }

    @Test
    fun testNestedQueryInColumn() = runBlocking {
        val delayedQuery = lazySelect(UserProfile)
                .withColumns(UserProfile.id, UserProfile.email,
                        Users.nestedQuery { select(Users.firstName) from Users where { Users.id equal UserProfile.id } }.toField<UsersRecord, Users>(),
                        Users.nestedQuery { select(Users.lastName) from Users where { Users.id equal UserProfile.id } }.toField<UsersRecord, Users>()
                )
                .filter {
                    UserProfile.id equal 1
                }
                .mapOne(executor) { rec ->
                    assertTrue { rec[UserProfile.id.columnName] == 1 }
                    assertTrue { rec[UserProfile.email.columnName] == "jdoe@google.com" }
                    assertTrue { rec[Users.firstName.columnName] == "jane" }
                    assertTrue { rec[Users.lastName.columnName] == "doe" }
                }
        delayedQuery.fetch()
    }

    @Test
    fun testNestedPreparedQueryInWhere() = runBlocking {
        val delayedQuery = lazyPreparedSelect(UserProfile)
                .withColumns(UserProfile.id, UserProfile.email,
                        Users.nestedQuery { select(Users.firstName) from Users where { Users.id equal UserProfile.id } }.toField<UsersRecord, Users>(),
                        Users.nestedQuery { select(Users.lastName) from Users where { Users.id equal UserProfile.id } }.toField<UsersRecord, Users>()
                )
                .filter {
                    UserProfile.id equal UserProfile.lazyId
                }
                .mapOne(executor, 1) { rec ->
                    assertTrue { rec[UserProfile.id.columnName] == 1 }
                    assertTrue { rec[UserProfile.email.columnName] == "jdoe@google.com" }
                    assertTrue { rec[Users.firstName.columnName] == "jane" }
                    assertTrue { rec[Users.lastName.columnName] == "doe" }
                }
        delayedQuery.fetch()
    }

    @Test
    fun testPreparedQueryInWhere() = runBlocking {
        val delayedQuery = lazyPreparedSelect(UserProfile)
                .filter {
                    (UserProfile.email equal UserProfile.lazyEmail) and (UserProfile.id contains Users.nestedQuery {
                        select(Users.id) from Users where {
                            lower(Users.firstName) like Users.lazyFirstName
                        }
                    })
                    


                }.mapOne(executor, "jdoe@google.com", "%ja%") { user ->
                    assertTrue { user.id == 1 }
                    assertTrue { user.email == "jdoe@google.com" }
                }
        delayedQuery.fetch()
    }
}