package design.animus.kotlin.frm.sql.example.jvm.mysql.jasync

import design.animus.kotlin.frm.executors.jvm.jasync.mysql.JaSyncMySQLExecutor
import design.animus.kotlin.frm.sql.example.jvm.mysql.generated.database.example_jasync.tables.Users
import design.animus.kotlin.frm.sql.executors.common.exceptions.EmptyResultSet
import design.animus.kotlin.frm.sql.query.mariadb.frm.lazySelect
import design.animus.kotlin.frm.sql.query.mariadb.frm.lazyUpdate
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.assertTrue

class UpdateTest {
    lateinit var executor: JaSyncMySQLExecutor

    @BeforeTest
    fun initialize() = runBlocking {
        executor = JaSyncMySQLExecutor()
        executor.connect(FRMTestConfig)
        Unit
    }

    @AfterTest
    fun tearDown() = runBlocking {
        executor.disconnect()
        Unit
    }

    @Test
    fun testUpdateQuery() = runBlocking {
        val delayedResult = lazyUpdate(Users)
            .changeOn {
                (Users.firstName equal "bob") and
                        (Users.lastName equal "dean")
            }
            .mutateTo {
                Users.firstName TO "Sausage"
                Users.lastName TO "Man"
            }
        val query = delayedResult.query.buildSQLString(delayedResult.sqlBuilder)
        val expected = """
            update users SET users.first_name = 'Sausage',users.last_name = 'Man' where users.first_name = 'bob' AND users.last_name = 'dean'
        """.trimIndent()
        println(query)
        println(expected)
        assertTrue { query == expected }
    }

    @Test
    fun testUpdate() = runBlocking {
        val originalUserQuery = lazySelect(Users).filter {
            (Users.firstName equal "bob") and
                    (Users.lastName equal "dean")
        }
        val delayedOriginalUser = originalUserQuery.defer(executor)
        val delayedResult = lazyUpdate(Users)
            .changeOn {
                (Users.firstName equal "bob") and
                        (Users.lastName equal "dean")
            }
            .mutateTo {
                Users.firstName TO "Sausage"
                Users.lastName TO "Man"
            }
            .save(executor)
        delayedResult.fetch()
        val delayedOldUser = originalUserQuery.defer(executor)
        val delayedNewUser = lazySelect(Users).filter {
            (Users.firstName equal "Sausage") and (Users.lastName equal "Man")
        }.defer(executor)
        try {
            delayedOldUser.fetch()
            assertTrue { false }
        } catch (e: EmptyResultSet) {
            assertTrue { true }
        }
        val newUser = delayedNewUser.fetch()
        val originalUser = delayedOriginalUser.fetch()
        assertTrue {
            originalUser.id == newUser.id &&
                    newUser.firstName == "Sausage" &&
                    newUser.lastName == "Man"
        }
    }
}
