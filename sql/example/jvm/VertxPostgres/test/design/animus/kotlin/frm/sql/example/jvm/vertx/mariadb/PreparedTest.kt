package design.animus.kotlin.frm.sql.example.jvm.vertx.mariadb


import design.animus.kotlin.frm.sql.example.jvm.vertx.postgres.generated.database.postgres.tables.UserProfile
import design.animus.kotlin.frm.sql.example.jvm.vertx.postgres.generated.database.postgres.tables.Users
import design.animus.kotlin.frm.sql.executors.common.exceptions.LazyConditionsNotEqualToArguments
import design.animus.kotlin.frm.sql.executors.jvm.vertx.postgres.VertxPostgresExecutor
import design.animus.kotlin.frm.sql.query.postgres.frm.prepared.lazyPreparedDelete
import design.animus.kotlin.frm.sql.query.postgres.frm.prepared.lazyPreparedInsert
import design.animus.kotlin.frm.sql.query.postgres.frm.prepared.lazyPreparedSelect
import design.animus.kotlin.frm.sql.query.postgres.frm.prepared.lazyPreparedSelectJoin
import io.vertx.core.Vertx
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertTrue


class PreparedTest {
    lateinit var executor: VertxPostgresExecutor
    lateinit var vertx: Vertx

    @Before
    fun initialize() {
        runBlocking {
            vertx = Vertx.vertx()
            executor = VertxPostgresExecutor(vertx)
            executor.connect(FRMTestConfig)
        }
    }

    @After
    fun tearDown() = runBlocking {
        executor.disconnect()
        Unit
    }

    @Test
    fun testPreparedSelect() = runBlocking {
        val query = lazyPreparedSelect(Users).filter {
            (Users.firstName equal Users.lazyFirstName) and (Users.lastName equal Users.lazyLastName)
        }
        val delayedJaneDoe = query.mapOne(executor, "jane", "doe") {
            assertTrue { it.firstName == "jane" }
            "Hello ${it.firstName} ${it.lastName}"
        }
        val delayedJaneDoeList = query.map(executor, "jane", "doe") {
            assertTrue { it.size == 1 }
            val item = it.first()
            "Hello ${item.firstName} ${item.lastName}"
        }
        val helloJane = delayedJaneDoe.fetch()
        val hellJaneFromList = delayedJaneDoeList.fetch()
        assertTrue { helloJane == "Hello jane doe" }
        assertTrue { hellJaneFromList == "Hello jane doe" }
    }

    @Test
    fun testPreparedSelectJoin() = runBlocking {
        val delayedUserProfiles = lazyPreparedSelectJoin(Users)
            .withColumns(Users.id, Users.firstName, Users.lastName, UserProfile.bio)
            .join(UserProfile) {
                UserProfile.id equal Users.lazyId
            }
            .filter {
                Users.firstName iLike Users.lazyLastName
            }
        try {
            delayedUserProfiles.map(executor) { it.first() }.fetch()
            assertTrue { false }
        } catch (e: LazyConditionsNotEqualToArguments) {
            val expectedMessage =
                "Expecting 1 where conditions and 1 lazy join conditions, for a total of 2 but got 0 arguments"
            assertTrue { e.message == expectedMessage }
        }
        val rsp = delayedUserProfiles.map(executor, 1, "%ja%") {
            assertTrue { it.size == 3 }
            val jane = it.first { it.id == 1 }
            assertTrue { jane.firstName == "jane" && jane.lastName == "doe" }
        }
        rsp.fetch()
    }

    @Test
    fun testPreparedInsert() = runBlocking {
        val newUser = lazyPreparedInsert(Users)
            .add {
                Users.firstName to Users.lazyFirstName
                Users.lastName to Users.lazyLastName
            }
        val delayedNewUser = newUser.save(executor, "NewUser", "One").fetch()
        val delayedDeleteJane = lazyPreparedDelete(Users)
            .deleteOn {
                (Users.firstName equal Users.lazyFirstName) and (Users.lastName equal Users.lazyLastName)
            }.remove(executor, "NewUser", "One")

    }

}


