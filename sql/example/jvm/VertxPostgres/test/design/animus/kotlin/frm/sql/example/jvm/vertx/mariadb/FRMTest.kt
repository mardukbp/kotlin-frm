package design.animus.kotlin.frm.sql.example.jvm.vertx.mariadb

import design.animus.kotlin.frm.sql.example.jvm.vertx.postgres.generated.database.postgres.Postgres
import design.animus.kotlin.frm.sql.example.jvm.vertx.postgres.generated.database.postgres.tables.UserProfile
import design.animus.kotlin.frm.sql.example.jvm.vertx.postgres.generated.database.postgres.tables.Users
import design.animus.kotlin.frm.sql.example.jvm.vertx.postgres.generated.database.postgres.tables.UsersRecord
import design.animus.kotlin.frm.sql.executors.common.exceptions.EmptyResultSet
import design.animus.kotlin.frm.sql.executors.jvm.vertx.postgres.VertxPostgresExecutor
import design.animus.kotlin.frm.sql.query.common.frm.InsertPair
import design.animus.kotlin.frm.sql.query.common.response.AJoinRecord
import design.animus.kotlin.frm.sql.query.postgres.frm.lazyDelete
import design.animus.kotlin.frm.sql.query.postgres.frm.lazyInsert
import design.animus.kotlin.frm.sql.query.postgres.frm.lazySelect
import design.animus.kotlin.frm.sql.query.postgres.frm.lazySelectJoin
import io.vertx.core.Vertx
import io.vertx.core.Vertx.vertx
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

data class UserProfileJoinRecord(val map: Map<String, Any?>) : AJoinRecord(map) {
    val id: Int by map
    val firstName: String by map
    val lastName: String by map
    val email: String by map
}

class FRMTest {
    private lateinit var vertx: Vertx
    private lateinit var executor: VertxPostgresExecutor

    @Before
    fun initialize() {
        runBlocking {
            vertx = vertx()
            executor = VertxPostgresExecutor(vertx)
            executor.connect(FRMTestConfig)
        }
    }

    @After
    fun tearDown() {
        runBlocking {
            executor.disconnect()
        }
    }

    @Test
    fun selectForJA() = runBlocking {
        val result = lazySelect(Users)
            .filter {
                (Users.firstName equal "jack") and (Users.lastName equal "smith")
            }
            .mapOne(executor) {
                assertTrue { it.firstName == "jack" }
                assertTrue { it.lastName == "smith" }
            }
        result.fetch()
    }

    @Test
    fun selectJoin() = runBlocking {
        val delayedResult = lazySelectJoin(Users)
            .withColumns(Users.id, Users.firstName, Users.lastName, UserProfile.email)
            .join(UserProfile) {
                UserProfile.id equal Users.id
            }
            .filter { Users.firstName iLike "%ja%" }
            .map(executor) {
                val sorted = it.sortedBy { item -> item["id"] as Int }
                assertTrue { sorted[0]["id"] == 1 && sorted[0]["email"] == "jdoe@google.com" }
                assertTrue { sorted[1]["id"] == 2 && sorted[1]["email"] == "jsmith@google.com" }
                assertTrue { sorted[2]["id"] == 4 && sorted[2]["email"] == "jblack@hotmail.com" }
            }
        delayedResult.fetch()
    }

    @Test
    fun selectJoinInto() = runBlocking {
        val delayedResult = lazySelectJoin(Users)
            .withColumns(Users.id, Users.firstName, Users.lastName, UserProfile.email)
            .join(UserProfile) {
                UserProfile.id equal Users.id
            }
            .filter { Users.firstName iLike "%ja%" }
            .mapInto(executor, UserProfileJoinRecord::class) {
                val sorted = it.sortedBy { item -> item.id }
                assertTrue { sorted[0].id == 1 && sorted[0].email == "jdoe@google.com" }
                assertTrue { sorted[1].id == 2 && sorted[1].email == "jsmith@google.com" }
                assertTrue { sorted[2].id == 4 && sorted[2].email == "jblack@hotmail.com" }
            }
        val x = delayedResult.fetch()

    }

    @Test
    fun insertRecord() = runBlocking {
        val values: InsertPair<Postgres, UsersRecord, Users> =
            listOf(Users.firstName to "Jonnny", Users.lastName to "Johnson")
        val rsp = lazyInsert(Users)
            .add(
                listOf(
                    listOf(Users.firstName to "Jonnny", Users.lastName to "Johnson"),
                    listOf(Users.firstName to "Jimmy", Users.lastName to "Nashorn")
                )
            )
            .save(executor)
            .fetch()
        val newUserOne = lazySelect(Users).filter {
            (Users.firstName equal "Jonnny") and (Users.lastName equal "Johnson")
        }.map(executor) {
            it
        }
        val newUserTwo = lazySelect(Users).filter {
            (Users.firstName equal "Jimmy") and (Users.lastName equal "Nashorn")
        }.map(executor) {
            it
        }
        val userOneList = newUserOne.fetch().size
        val userTwoList = newUserTwo.fetch().size
        assertTrue { userOneList == 1 }
        assertTrue { userTwoList == 1 }
    }

    @Test
    fun testDelete() = runBlocking {
        val x = assertFailsWith(EmptyResultSet::class) {
            val delayedInsertResult = lazyInsert(Users)
                .add(
                    listOf(
                        listOf(Users.firstName to "Delete", Users.lastName to "Me")
                    )
                )
                .save(executor)
                .fetch()
            val delayedDeleteResult = lazyDelete(Users).deleteOn {
                (Users.firstName equal "Delete") and (Users.lastName equal "Me")
            }.remove(executor).fetch()
            val validateGone = lazySelect(Users).filter {
                (Users.firstName equal "Delete") and (Users.lastName equal "Me")
            }.defer(executor)
            val result = validateGone.fetch()
        }
    }
}