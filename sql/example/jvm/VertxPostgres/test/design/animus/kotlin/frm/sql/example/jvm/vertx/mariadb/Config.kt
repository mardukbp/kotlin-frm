package design.animus.kotlin.frm.sql.example.jvm.vertx.mariadb

import design.animus.kotlin.frm.sql.executors.jvm.vertx.postgres.VertxPostgresConfig

internal val FRMTestConfig = VertxPostgresConfig(
    System.getenv("pgDataBaseHost"),
    System.getenv("pgDataBase"),
    System.getenv("pgDataBaseUser"),
    System.getenv("pgDataBasePassword"),
    5432,
    "example"
)