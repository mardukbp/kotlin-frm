CREATE SCHEMA IF NOT EXISTS vertx_example AUTHORIZATION postgres;
SET SCHEMA 'vertx_example';


create table users(
    id serial primary key not null,
    first_name varchar(512) not null,
    last_name varchar(512) not null
);

 create table user_profile (
    id int primary key references users(id),
    email varchar(2048),
    bio text
 );

create table users_multi_column_primary_key(
    id serial not null,
    email varchar(2048) not null,
    first_name varchar(512) not null,
    last_name varchar(512),
    primary key (id, email, first_name, last_name)
);

create table inventory(
    id serial not null primary key,
    name varchar(512) not null,
    price decimal not null
);

create table user_orders(
    id serial primary key not null,
    user_id int not null,
    inventory_id int not null,
    order_placed timestamp not null
);

insert into users (first_name, last_name)
    values ('jane', 'doe'),
    ('jack', 'smith'),
    ('jim', 'bob'),
    ('jack', 'black'),
    ('bob', 'dean');

insert into user_profile (id, email, bio)
    values (
        (select id from users where first_name = 'jack' and last_name = 'smith'), 'jsmith@google.com', 'my bio'
    ),
    (
        (select id from users where first_name = 'jane' and last_name = 'doe'), 'jdoe@google.com', 'my bio'
    ),
    (
        (select id from users where first_name = 'jim' and last_name = 'bob'), 'jbob@google.com', 'my bio'
    ),
    (
        (select id from users where first_name = 'jack' and last_name = 'black'), 'jblack@hotmail.com', 'my bio'
    ),
    (
        (select id from users where first_name = 'bob' and last_name = 'dean'), 'bigbobbydean@lycos.com', 'my bio'
    );

insert into users_multi_column_primary_key (email, first_name, last_name)
    values ('jdoe@google.com', 'jane', 'doe'),
    ('jsmith@google.com', 'jack', 'smith'),
    ('jbob@google.com', 'jim', 'bob'),
    ('jblack@hotmail.com', 'jack', 'black'),
    ('bigbobbydean@lycos.com', 'bob', 'dean');

insert into inventory (name, price) values
    ('widget1', 127.32),
    ('widget2', 91.00),
    ('widget3', 87.00),
    ('widget4', 201.83),
    ('widget5', 670.12)
;

insert  into user_orders (user_id, inventory_id, order_placed) values
    (
        (select users.id from users where first_name = 'jane' and last_name ='doe'),
        (select inventory.id from inventory where inventory.name = 'widget1'),
        localtimestamp
    ),
    (
        (select users.id from users where first_name = 'jane' and last_name ='doe'),
        (select inventory.id from inventory where inventory.name = 'widget2'),
        localtimestamp
    ),  
    (
        (select users.id from users where first_name = 'jane' and last_name ='doe'),
        (select inventory.id from inventory where inventory.name = 'widget2'),
        localtimestamp
    ),
    (
        (select users.id from users where first_name = 'jane' and last_name ='doe'),
        (select inventory.id from inventory where inventory.name = 'widget3'),
        localtimestamp
    ),
    (
        (select users.id from users where first_name = 'jane' and last_name ='doe'),
        (select inventory.id from inventory where inventory.name = 'widget4'),
        localtimestamp
    ),
        (
            (select users.id from users where first_name = 'jack' and last_name ='black'),
            (select inventory.id from inventory where inventory.name = 'widget1'),
            localtimestamp
        ),
        (
            (select users.id from users where first_name = 'jack' and last_name ='black'),
            (select inventory.id from inventory where inventory.name = 'widget2'),
            localtimestamp
        ),  
        (
            (select users.id from users where first_name = 'jack' and last_name ='black'),
            (select inventory.id from inventory where inventory.name = 'widget2'),
            localtimestamp
        ),
        (
            (select users.id from users where first_name = 'jack' and last_name ='black'),
            (select inventory.id from inventory where inventory.name = 'widget3'),
            localtimestamp
        ),
        (
            (select users.id from users where first_name = 'jack' and last_name ='black'),
            (select inventory.id from inventory where inventory.name = 'widget4'),
            localtimestamp
        )
;