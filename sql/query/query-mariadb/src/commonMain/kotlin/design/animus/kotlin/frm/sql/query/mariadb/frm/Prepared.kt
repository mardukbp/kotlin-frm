package design.animus.kotlin.frm.sql.query.mariadb.frm

import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.mariadb.query.IMariaDB
import design.animus.kotlin.frm.sql.query.mariadb.query.IMariaDBTable

suspend fun <D : IMariaDB, R : ADatabaseRecord, T : IMariaDBTable<D, R>>
        lazyPreparedSelect(table: T) = design.animus.kotlin.frm.sql.query.common.frm.prepare.lazyPreparedSelect(
    table, CommonSQLBuilder()
)

suspend fun <D : IMariaDB, R : ADatabaseRecord, T : IMariaDBTable<D, R>>
        lazyPreparedSelectJoin(table: T) = design.animus.kotlin.frm.sql.query.common.frm.prepare.lazyPreparedSelectJoin(
    table, CommonSQLBuilder()
)

suspend fun <D : IMariaDB, R : ADatabaseRecord, T : IMariaDBTable<D, R>>
        lazyPreparedDelete(table: T) = design.animus.kotlin.frm.sql.query.common.frm.prepare.lazyPreparedDelete(
    table, CommonSQLBuilder()
)

suspend fun <D : IMariaDB, R : ADatabaseRecord, T : IMariaDBTable<D, R>>
        lazyPreparedInsert(table: T) = design.animus.kotlin.frm.sql.query.common.frm.prepare.lazyPreparedInsert(
    table, CommonSQLBuilder()
)


suspend fun <D : IMariaDB, R : ADatabaseRecord, T : IMariaDBTable<D, R>>
        lazyPreparedUpdate(table: T) = design.animus.kotlin.frm.sql.query.common.frm.prepare.lazyPreparedUpdate(
    table, CommonSQLBuilder()
)