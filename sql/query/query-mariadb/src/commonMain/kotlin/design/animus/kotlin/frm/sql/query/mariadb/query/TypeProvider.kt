package design.animus.kotlin.frm.sql.query.mariadb.query

import design.animus.kotlin.frm.sql.executors.common.exceptions.ReflectionUnknownType
import design.animus.kotlin.frm.sql.query.common.ClassName
import design.animus.kotlin.frm.sql.query.common.ISQLType
import design.animus.kotlin.frm.sql.query.common.SQLType
import design.animus.kotlin.frm.sql.query.common.SQLTypeProvider

object MariaDBTypeProvider : SQLTypeProvider(
    ClassName("design.animus.kotlin.frm.sql.query.mariadb.query", IMariaDB::class.simpleName!!),
    ClassName("design.animus.kotlin.frm.sql.query.mariadb.query", IMariaDBTable::class.simpleName!!)
) {
    override suspend fun getTypeByName(type: SQLType): ISQLType = when (type) {
        "date" -> Date
        "datetime" -> DateTime
        "new_date" -> LocalDate
        "timestamp" -> TimeStamp
        "tinyint" -> MariaDBByte
        "smallint" -> SmallInt
        "year" -> Year
        "float" -> MariaDBFloat
        "double" -> MariaDBDouble
        "int" -> MariaDBInt
        "int24" -> Int24
        "mediumint" -> MediumInt
        "bigint" -> BigInt
        "numeric" -> Numeric
        "new_decimal" -> NewDecimal
        "decimal" -> MariaDBDecimal
        "string" -> MariaDBString
        "var_string" -> VarString
        "varchar" -> VarChar
        "time" -> Time
        "text" -> Text
        "JSON" -> JSON
        "enum" -> ENUM
        "blob" -> Blob
        else -> throw ReflectionUnknownType(
            "Given type $type is unknown.",
            type
        )
    }
}