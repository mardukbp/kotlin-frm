package design.animus.kotlin.frm.sql.query.postgres.frm.prepared

import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.postgres.builder.PostgresSQLBuilder
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresDatabase
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable

suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>>
        lazyPreparedSelect(table: T) = design.animus.kotlin.frm.sql.query.common.frm.prepare.lazyPreparedSelect(
        table, PostgresSQLBuilder()
)

suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>>
        lazyPreparedSelectJoin(table: T) = design.animus.kotlin.frm.sql.query.common.frm.prepare.lazyPreparedSelectJoin(
        table, PostgresSQLBuilder()
)

suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>>
        lazyPreparedDelete(table: T) = design.animus.kotlin.frm.sql.query.common.frm.prepare.lazyPreparedDelete(
        table, PostgresSQLBuilder()
)

suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>>
        lazyPreparedInsert(table: T) = design.animus.kotlin.frm.sql.query.common.frm.prepare.lazyPreparedInsert(
        table, PostgresSQLBuilder()
)


suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>>
        lazyPreparedUpdate(table: T) = design.animus.kotlin.frm.sql.query.common.frm.prepare.lazyPreparedUpdate(
        table, PostgresSQLBuilder()
)