package design.animus.kotlin.frm.sql.query.postgres.builder

import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLQuery
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.query
import design.animus.kotlin.frm.sql.query.common.query.type.MutableQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresDatabase
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable

suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>> T.nestedQuery(
        builder: ISQLBuilder<D, R, T> = PostgresSQLBuilder(),
        block: suspend CommonSQLQuery<D, R, T>.() -> MutableQuery<D, R, T>
) = this.query(builder, block)