package design.animus.kotlin.frm.sql.query.postgres.query.aggregates

import design.animus.kotlin.frm.sql.query.common.query.AggregateField
import design.animus.kotlin.frm.sql.query.common.query.Fields
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.postgres.query.JsonAggregate
import design.animus.kotlin.frm.sql.query.postgres.query.JsonObject

typealias JsonAggPair = Pair<String, Fields<*, *, *, *>>

class JsonObjectBuilder {
    val pairs: MutableList<JsonAggPair> = mutableListOf()

    suspend infix fun <C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
            String.to(field: Fields<C, D, R, T>) =
        pairs.add(Pair(this, field))

    suspend infix fun String.to(jsonObj: JsonObject) =
        pairs.add(
            Pair(
                this, AggregateField<Any?, IDatabase, ADatabaseRecord, ITable<IDatabase, ADatabaseRecord>>(
                    jsonObj, "", "", "", ""
                )
            )
        )

    suspend fun jsonObj(builder: suspend JsonObjectBuilder.() -> Unit): JsonObject {
        val state = JsonObjectBuilder()
        state.builder()
        return JsonObject(state.pairs.toList())
    }
}

class JsonAgg {
    var builder: MutableList<JsonAggPair> = mutableListOf()
    suspend fun jsonObj(builder: suspend JsonObjectBuilder.() -> Unit) {
        val state = JsonObjectBuilder()
        state.builder()
        this.builder.addAll(state.pairs)
    }
}

suspend fun <D : IDatabase> jsonAgg(
    name: String = "",
    builder: suspend JsonAgg.() -> Unit
): AggregateField<Any?, D, ADatabaseRecord, ITable<D, ADatabaseRecord>> {
    val state = JsonAgg()
    state.builder()
    return AggregateField<Any?, D, ADatabaseRecord, ITable<D, ADatabaseRecord>>(
        JsonAggregate(state.builder.toList()), name, "", "", "", named = name
    )
}
