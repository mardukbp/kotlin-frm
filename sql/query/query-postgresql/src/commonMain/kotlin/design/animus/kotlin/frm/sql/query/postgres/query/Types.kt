package design.animus.kotlin.frm.sql.query.postgres.query

import design.animus.kotlin.frm.sql.query.common.ClassName
import design.animus.kotlin.frm.sql.query.common.ISQLType

sealed class PostgresSQLTypes : ISQLType()

object PostgresBoolean : PostgresSQLTypes() {
    override val sqlType = "boolean"
    override val jvmType = ClassName("kotlin", "Boolean")
    override val nativeType = ClassName("kotlin", "Boolean")
    override val jsType = ClassName("kotlin", "Boolean")
    override val commonType = ClassName("kotlin", "Boolean")
}

object PostgresSmallInt : PostgresSQLTypes() {
    override val sqlType = "int2"
    override val jvmType = ClassName("kotlin", "Short")
    override val nativeType = ClassName("kotlin", "Short")
    override val jsType = ClassName("kotlin", "Short")
    override val commonType = ClassName("kotlin", "Short")
}

object PostgresInt : PostgresSQLTypes() {
    override val sqlType = "int4"
    override val jvmType = ClassName("kotlin", "Int")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "Int")
}

object PostgresBigInt : PostgresSQLTypes() {
    override val sqlType = "int8"
    override val jvmType = ClassName("kotlin", "Long")
    override val nativeType = ClassName("kotlin", "Long")
    override val jsType = ClassName("kotlin", "Long")
    override val commonType = ClassName("kotlin", "Long")
}

object PostgresNumeric : PostgresSQLTypes() {
    override val sqlType = "numeric"
    override val jvmType = ClassName("java.math", "BigDecimal")
    override val nativeType = ClassName("kotlin", "Number")
    override val jsType = ClassName("kotlin", "Number")
    override val commonType = ClassName("kotlin", "Number")
}

object PostgresDecimal : PostgresSQLTypes() {
    override val sqlType = "decimal"
    override val jvmType = ClassName("java.math", "BigDecimal")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object PostgresReal : PostgresSQLTypes() {
    override val sqlType = "real"
    override val jvmType = ClassName("kotlin", "Float")
    override val nativeType = ClassName("kotlin", "Float")
    override val jsType = ClassName("kotlin", "Float")
    override val commonType = ClassName("kotlin", "Float")
}

object PostgresDouble : PostgresSQLTypes() {
    override val sqlType = "double"
    override val jvmType = ClassName("kotlin", "Double")
    override val nativeType = ClassName("kotlin", "Double")
    override val jsType = ClassName("kotlin", "Double")
    override val commonType = ClassName("kotlin", "Double")
}

object PostgresText : PostgresSQLTypes() {
    override val sqlType = "text"
    override val jvmType = ClassName("kotlin", "String")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object PostgresVarChar : PostgresSQLTypes() {
    override val sqlType = "varchar"
    override val jvmType = ClassName("kotlin", "String")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object PostgresBPChar : PostgresSQLTypes() {
    override val sqlType = "bpchar"
    override val jvmType = ClassName("kotlin", "String")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object PostgresTimeStamp : PostgresSQLTypes() {
    override val sqlType = "timestamp"
    override val jvmType = ClassName("java.time", "LocalDateTime")
    override val nativeType = ClassName("kotlin", "Long")
    override val jsType = ClassName("kotlin", "Long")
    override val commonType = ClassName("kotlin", "Long")
}

object PostgresTimeStampWithTimeZone : PostgresSQLTypes() {
    override val sqlType = "timestamp_with_timezone"
    override val jvmType = ClassName("java.time", "DateTime")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object PostgresDate : PostgresSQLTypes() {
    override val sqlType = "date"
    override val jvmType = ClassName("java.time", "LocalDate")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object PostgresTime : PostgresSQLTypes() {
    override val sqlType = "time"
    override val jvmType = ClassName("java.time", "LocalTime")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object PostgresByteA : PostgresSQLTypes() {
    override val sqlType = "bytea"
    override val jvmType = ClassName("java.time", "LocalTime")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object PostgresUUID : PostgresSQLTypes() {
    override val sqlType = "uuid"
    override val jvmType = ClassName("java.util", "UUID")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object PostgresJSON : PostgresSQLTypes() {
    override val sqlType = "json"
    override val jvmType = ClassName("kotlin", "String")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}