package design.animus.kotlin.frm.sql.query.postgres.query

import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLQuery
import design.animus.kotlin.frm.sql.query.common.builder.sqlQuery
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.type.LockedQuery
import design.animus.kotlin.frm.sql.query.common.query.type.MutableQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.postgres.builder.PostgresSQLBuilder

interface IPostgresTable<D : IPostgresDatabase, R : ADatabaseRecord> : ITable<D, R>

internal fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>> getSchemaTable(queryTable: QueryTable): TableNamePair =
    when (queryTable) {
        is Table<*, *, *> -> {
            val table = (queryTable as Table<D, R, T>).table
            Pair(
                "${table.reflectedDatabase.schema}.${table.reflectedTableName}",
                "${table.reflectedDatabase.schema}.${table.reflectedTableName}"
            )
        }
        is NamedTable<*, *, *> -> {
            val namedTable = queryTable as NamedTable<D, R, T>
            Pair(
                "${namedTable.table.reflectedDatabase.schema}.${namedTable.table.reflectedTableName} as ${namedTable.name}",
                namedTable.name
            )
        }
    }

suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>> T.query(
    block: suspend CommonSQLQuery<D, R, T>.() -> MutableQuery<D, R, T>
): LockedQuery<D, R, T> = sqlQuery(PostgresSQLBuilder(), block)

suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>> sqlQuery(
    block: suspend CommonSQLQuery<D, R, T>.() -> MutableQuery<D, R, T>
): LockedQuery<D, R, T> = sqlQuery(PostgresSQLBuilder(), block)
