package design.animus.kotlin.frm.sql.query.postgres.query

import design.animus.kotlin.frm.sql.executors.common.exceptions.ReflectionUnknownType
import design.animus.kotlin.frm.sql.query.common.ClassName
import design.animus.kotlin.frm.sql.query.common.ISQLType
import design.animus.kotlin.frm.sql.query.common.SQLType
import design.animus.kotlin.frm.sql.query.common.SQLTypeProvider

object PostgresTypeProvider : SQLTypeProvider(
    ClassName(
        "design.animus.kotlin.frm.sql.query.postgres.query",
        "IPostgresDatabase"
    ),
    ClassName("design.animus.kotlin.frm.sql.query.postgres.query", "IPostgresTable")
) {
    override suspend fun getTypeByName(type: SQLType): ISQLType = when (type) {
        "boolean", "bool" -> PostgresBoolean
        "smallint", "int2" -> PostgresSmallInt
        "integer", "int4" -> PostgresInt
        "bigint", "int8" -> PostgresBigInt
        "numeric" -> PostgresNumeric
        "real" -> PostgresReal
        "decimal" -> PostgresDecimal
        "double" -> PostgresDouble
        "text" -> PostgresText
        "varchar" -> PostgresVarChar
        "bpchar" -> PostgresBPChar
        "timestamp" -> PostgresTimeStamp
        "timestamp_with_timezone" -> PostgresTimeStampWithTimeZone
        "date" -> PostgresDate
        "time" -> PostgresTime
        "bytea" -> PostgresByteA
        "uuid" -> PostgresUUID
        "json" -> PostgresJSON
        "jsonb" -> PostgresJSON
        else -> throw ReflectionUnknownType(
            "Given type $type is unknown.",
            type
        )
    }
}
