package design.animus.kotlin.frm.sql.query.postgres.builder

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.frm.sql.executors.common.exceptions.MisMatchColumnSetForInsert
import design.animus.kotlin.frm.sql.executors.common.exceptions.NoValuesToAdd
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.SQLQuery
import design.animus.kotlin.frm.sql.query.common.builder.magicPreparedKey
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.type.NumberedCharacter
import design.animus.kotlin.frm.sql.query.common.query.type.PreparedPlacedHolder
import design.animus.kotlin.frm.sql.query.common.query.type.SimpleCharacter
import design.animus.kotlin.frm.sql.query.common.query.verbs.*
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresDatabase
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable
import design.animus.kotlin.frm.sql.query.postgres.query.JsonAggregate
import design.animus.kotlin.frm.sql.query.postgres.query.JsonObject
import mu.KotlinLogging

class PostgresSQLBuilder<D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>> : ISQLBuilder<D, R, T> {
    override val logger = KotlinLogging.logger {}
    override fun getTablePair(queryTable: QueryTable): TableNamePair =
            when (queryTable) {
                is Table<*, *, *> -> {
                    val table = queryTable.table as IPostgresTable<*, *>
                    Pair(
                            "${table.reflectedDatabase.schema}.${table.reflectedTableName}",
                            table.reflectedTableName
                    )
                }
                is NamedTable<*, *, *> -> {
                    val table = queryTable as IPostgresTable<*, *>
                    Pair(
                            "${table.reflectedDatabase.schema}.${queryTable.reflectedTable.reflectedTableName} as ${queryTable.name}",
                            queryTable.reflectedTableName
                    )
                }
            }

    override fun escapeValue(value: Any?): String = when (value) {
        Int, Long, Double, Float -> "$value"
        null -> "NULL"
        else -> "'$value'"
    }

    override suspend fun buildWhereString(conditions: List<ConditionalSteps>, table: FriendlyTableName) =
            commonBuildWhereString<D, R, T>(conditions, table)


    suspend fun createReturnStatement(schemaTable: String, returning: Option<List<Fields<Any?, D, R, T>>>) =
            Option.comprehension("") {
                val (fields) = returning
                when {
                    fields.isEmpty() -> "returning *"
                    else -> "returning ${fields.joinToString(",") { "${schemaTable}.${it.columnName}" }}"
                }
            }

    override fun buildColumnString(
            schemaTable: SchemaTableName,
            friendlyTable: FriendlyTableName,
            field: Fields<*, *, *, *>
    ): String {
        return when (field) {
            is PrimaryKey -> "${field.table}.${field.columnName}"
            is Field -> "${field.table}.${field.columnName}"
            is NamedField -> "${field.table}.${field.field.columnName} as ${field.name}"
            is PrimaryKeys -> field.columns.joinToString(",") {
                "${field.table}.${it.columnName}"
            }
            is LateField -> magicPreparedKey
            is AggregateField -> {
                when (field.aggregate) {
                    is Count, Sum, Maximum, Minimum, Average -> "${field.aggregate.functionName}($schemaTable.${field.columnName})"
                    is JsonAggregate -> {
                        val aggregate = field.aggregate as JsonAggregate
                        val jsonBuilder = aggregate.struct.joinToString(",") { jsonPair ->
                            val (key, jsonField) = jsonPair
                            val columnRepresentation =
                                    buildColumnString(jsonField.table, jsonField.table, jsonField)
                            "'$key',$columnRepresentation"
                        }
                        val named = if (field.named.isEmpty()) "" else " as ${field.named}"
                        "${aggregate.functionName}(json_build_object($jsonBuilder))$named"
                    }
                    is JsonObject -> {
                        val aggregate = field.aggregate as JsonObject
                        val jsonBuilder = aggregate.pairs.joinToString(",") { pair ->
                            val (key, jsonField) = pair
                            val columnRepresentation =
                                    buildColumnString(jsonField.table, jsonField.table, jsonField)
                            "'$key',$columnRepresentation"
                        }
                        val named = if (field.named.isEmpty()) "" else " as ${field.named}"
                        "${aggregate.functionName}($jsonBuilder)$named"
                    }
                    else -> "${field.aggregate.functionName}($schemaTable.${field.columnName})"
                }
            }
            is NestedQuery<*, *, *, *> -> {
                "(${field.sql})"
            }

        }
    }

    override fun buildGroupBy(
            schemaTable: SchemaTableName,
            friendlyTable: FriendlyTableName,
            fields: List<Fields<*, *, *, *>>
    ) = fields.joinToString(",") {
        buildColumnString(schemaTable, friendlyTable, it)
    }

    override suspend fun buildSelectQuery(query: ISelectQuery): String {
        val (schemaTable, friendlyTable) = getTablePair(query.table)
        val columns = if (query.columns.isEmpty()) "*" else query.columns.joinToString(",") {
            buildColumnString(schemaTable, friendlyTable, it)
        }
        val where = buildWhereString(query.conditions, friendlyTable)
        return if (query.groupBy.isEmpty()) {
            "${query.verb.name} $columns from $schemaTable $where"
        } else {
            val groupBy = buildGroupBy(schemaTable, friendlyTable, query.groupBy)
            "${query.verb.name} $columns from $schemaTable $where group by $groupBy"
        }.trimIndent()
    }

    override suspend fun buildUpdateQuery(query: IUpdateQuery<D, R, T>): String {
        val (schemaTable, friendlyTable) = getTablePair(query.table)
        val where = buildWhereString(query.conditions, friendlyTable)
        val changes = query.changes.joinToString(",") { change ->
            if (change.lateField != null) {
                "${change.field.columnName} = $magicPreparedKey"
            } else {
                val value = when (change.value) {
                    is Int, is Long, is Float, is Double -> "${change.value}"
                    else -> "'${change.value}'"
                }
                "${change.field.columnName} = $value"
            }
        }
        val returning = createReturnStatement(schemaTable, query.returningFields)
        return "update $schemaTable SET $changes $where $returning".trimIndent()
    }

    override suspend fun buildInsertQuery(query: IInsertQuery<D, R, T>): String {
        val (schemaTable, friendlyTable) = getTablePair(query.table)
        val columns = query.newItems.map { it.map { f -> f.first } }
        if (columns.toSet().size != 1) throw MisMatchColumnSetForInsert("Was anticipating only 1 set of columns but received ${columns.toSet().size}")
        val columnNames = columns.first().joinToString(",") { it.columnName }
        val values = when {
            query.newItems.size > 1 -> {
                query.newItems.joinToString(",") { item ->
                    val a = item.joinToString(", ") { escapeValue(it.second) }
                    "($a)"
                }
            }
            query.newItems.size == 1 -> {
                "(${query.newItems.first().joinToString(", ") { escapeValue(it.second) }})"
            }
            else -> throw NoValuesToAdd("Expected values to be added but no values were provided.")
        }
        val returning = createReturnStatement(schemaTable, query.returningFields)
        return "${query.verb.name} into $schemaTable($columnNames) values $values $returning".trimIndent()
    }

    override suspend fun buildSelectJoinQuery(query: ISelectJoinQuery): String {
        val (schemaTable, friendlyTable) = getTablePair(query.table)
        val columns = if (query.columns.isEmpty()) "*" else query.columns.joinToString(",") {
            buildColumnString(schemaTable, friendlyTable, it)
        }
        val joins = query.joinStatements.joinToString(" ") {
            val (joinSchemaTable, joinFriendlyTable) = getTablePair(it.table)
            when (it) {
                is Join -> "join $joinSchemaTable on ${buildConditionString(
                        it.condition,
                        joinFriendlyTable,
                        schemaTable
                )}"
                is InnerJoin -> "inner join $joinSchemaTable on ${buildConditionString(
                        it.condition,
                        joinFriendlyTable,
                        schemaTable
                )}"
                is RightJoin -> "right join $joinSchemaTable on ${buildConditionString(
                        it.condition,
                        joinFriendlyTable,
                        schemaTable
                )}"
                is LeftJoin -> "left join $joinSchemaTable on ${buildConditionString(
                        it.condition,
                        joinFriendlyTable,
                        schemaTable
                )}"
            }
        }
        val where = buildWhereString(query.conditions, friendlyTable)
        return if (query.groupBy.isEmpty()) {
            "SELECT $columns from $schemaTable $joins $where"
        } else {
            val groupBy = buildGroupBy(schemaTable, friendlyTable, query.groupBy)
            "SELECT $columns from $schemaTable $joins $where group by $groupBy"
        }.trimIndent()
    }

    override suspend fun buildDeleteQuery(query: IDeleteQuery<D, R, T>): String {
        val (schemaTable, friendlyTable) = getTablePair(query.table)
        val where = buildWhereString(query.conditions, friendlyTable)
        return """
            ${query.verb.name} from $schemaTable $where
        """.trimIndent()
    }

    override fun substitutePreparedVarWithExecutorVar(
            query: SQLQuery,
            variable: PreparedPlacedHolder
    ): SQLQuery {
        return when (variable) {
            is SimpleCharacter -> {
                query.replace(magicPreparedKey, variable.preparedCharacter)
            }
            is NumberedCharacter -> {
                var idx = variable.startAt
                var newQuery = query
                while (newQuery.contains(magicPreparedKey)) {
                    newQuery = newQuery.replaceFirst(
                            magicPreparedKey,
                            if (variable.escapeChar != null) """${variable.escapeChar}${variable.preparedCharacter}$idx""" else "${variable.preparedCharacter}$idx"
                    )
                    idx += 1
                }
                newQuery


            }
        }.replace("'", "")
    }
}
