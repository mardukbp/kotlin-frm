package design.animus.kotlin.frm.sql.query.postgres.query

import design.animus.kotlin.frm.sql.query.common.query.IDatabase

interface IPostgresDatabase : IDatabase {
    override val database: String
    val schema: String
}
