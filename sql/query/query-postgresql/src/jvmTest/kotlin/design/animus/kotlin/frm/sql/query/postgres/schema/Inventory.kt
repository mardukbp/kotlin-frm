package design.animus.kotlin.frm.sql.query.postgres.schema

import design.animus.kotlin.frm.sql.query.common.query.Field
import design.animus.kotlin.frm.sql.query.common.query.PrimaryKey
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecordByMap
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable

object Inventory : IPostgresTable<Example, InventoryRecord> {
    override val reflectedRecordClass = InventoryRecord::class
    override val reflectedTable = this
    override val reflectedTableName = "inventory"
    override val reflectedDatabase = Example
    override val reflectedColumns = listOf("id", "name", "price")
    val id: PrimaryKey<Long, Example, InventoryRecord, Inventory> = object :
        PrimaryKey<Long, Example, InventoryRecord, Inventory>("id", "", Example.database, reflectedTableName) {}
    val item: Field<String, Example, InventoryRecord, Inventory> =
        object : Field<String, Example, InventoryRecord, Inventory>("name", "", Example.database, reflectedTableName) {}
    val price: Field<Number, Example, InventoryRecord, Inventory> = object :
        Field<Number, Example, InventoryRecord, Inventory>("price", "", Example.database, reflectedTableName) {}

}

data class InventoryRecord(
    val map: Map<String, Any>
) : ADatabaseRecordByMap(map) {
    val id: Long by map
    val item: String by map
    val price: Number by map
}
