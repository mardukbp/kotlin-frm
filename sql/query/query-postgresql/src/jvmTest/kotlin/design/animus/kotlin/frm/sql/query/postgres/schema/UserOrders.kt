package design.animus.kotlin.frm.sql.query.postgres.schema

import design.animus.kotlin.frm.sql.query.common.query.Field
import design.animus.kotlin.frm.sql.query.common.query.PrimaryKey
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecordByMap
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable

object UserOrders : IPostgresTable<Example, UserOrdersRecord> {
    override val reflectedRecordClass = UserOrdersRecord::class
    override val reflectedTable = this
    override val reflectedTableName = "user_orders"
    override val reflectedDatabase = Example
    override val reflectedColumns = listOf("id", "user_id", "inventory_id", "order_placed")
    val id: PrimaryKey<Long, Example, InventoryRecord, Inventory> = object :
        PrimaryKey<Long, Example, InventoryRecord, Inventory>("id", "", Example.database, reflectedTableName) {}
    val inventoryId = object :
        Field<Long, Example, UserOrdersRecord, UserOrders>("inventory_id", "", Example.database, reflectedTableName) {}
    val userId = object :
        Field<Long, Example, UserOrdersRecord, UserOrders>("user_id", "", Example.database, reflectedTableName) {}
    val orderPlaced = object : Field<String, Example, UserOrdersRecord, UserOrders>(
        "order_placed",
        "",
        Example.database,
        reflectedTableName
    ) {}
}

data class UserOrdersRecord(val map: Map<String, Any?>) : ADatabaseRecordByMap(map) {
    val id: Long by map
    val inventoryId: Long by map
    val orderPlaced: String by map
}
