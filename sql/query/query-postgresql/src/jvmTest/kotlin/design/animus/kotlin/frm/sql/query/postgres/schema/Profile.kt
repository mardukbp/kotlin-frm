package design.animus.kotlin.frm.sql.query.postgres.schema

import design.animus.kotlin.frm.sql.query.common.query.Field
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecordByMap


object Profile : ITable<Example, ProfileRecord> {
    override val reflectedDatabase = Example
    override val reflectedTableName = "user_profile"
    override val reflectedRecordClass = ProfileRecord::class
    override val reflectedColumns = listOf("id", "email")
    override val reflectedTable = this
    val id: Field<Long, Example, ProfileRecord, Profile> =
        object : Field<Long, Example, ProfileRecord, Profile>("id", "bigint", Example.database, reflectedTableName) {}
    val email: Field<String, Example, ProfileRecord, Profile> = object :
        Field<String, Example, ProfileRecord, Profile>("email", "varchar", Example.database, reflectedTableName) {}
}

data class ProfileRecord(val map: Map<String, Any>) : ADatabaseRecordByMap(map) {
    val id: Long by map
    val email: String by map
}
