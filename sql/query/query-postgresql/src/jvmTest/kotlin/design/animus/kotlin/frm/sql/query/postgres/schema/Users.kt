package design.animus.kotlin.frm.sql.query.postgres.schema

import design.animus.kotlin.frm.sql.query.common.query.Field
import design.animus.kotlin.frm.sql.query.common.query.PrimaryKey
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecordByMap
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable

object Users : IPostgresTable<Example, UsersRecord> {
    override val reflectedRecordClass = UsersRecord::class
    override val reflectedTable = this
    override val reflectedTableName = "users"
    override val reflectedDatabase = Example
    override val reflectedColumns = listOf("id", "first_name", "last_name")
    val id: PrimaryKey<Long, Example, UsersRecord, Users> =
        object : PrimaryKey<Long, Example, UsersRecord, Users>("id", "bigint", Example.database, reflectedTableName) {}
    val firstName: Field<String, Example, UsersRecord, Users> = object :
        Field<String, Example, UsersRecord, Users>("first_name", "varchar(24)", Example.database, reflectedTableName) {}
    val lastName: Field<String, Example, UsersRecord, Users> = object :
        Field<String, Example, UsersRecord, Users>("last_name", "varchar(24)", Example.database, reflectedTableName) {}

}

data class UsersRecord(
    val map: Map<String, Any>
) : ADatabaseRecordByMap(map) {
    val id: Long by map
    val firstName by map
}
