package design.animus.kotlin.frm.sql.query.postgres

import design.animus.kotlin.frm.sql.query.common.builder.sqlQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.postgres.builder.PostgresSQLBuilder
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable
import design.animus.kotlin.frm.sql.query.postgres.query.aggregates.jsonAgg
import design.animus.kotlin.frm.sql.query.postgres.schema.Example
import design.animus.kotlin.frm.sql.query.postgres.schema.Inventory
import design.animus.kotlin.frm.sql.query.postgres.schema.UserOrders
import design.animus.kotlin.frm.sql.query.postgres.schema.Users
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue

class SelectAggregate {
    @Test
    fun testJsonAgg() = runBlocking {
        val query = sqlQuery<Example, ADatabaseRecord, IPostgresTable<Example, ADatabaseRecord>>(
            builder = PostgresSQLBuilder()
        ) {
            selectJ(Users.id, Users.firstName, Users.lastName, jsonAgg {
                jsonObj {
                    "orderPlaced" to UserOrders.orderPlaced
                    "inventory" to jsonObj {
                        "id" to Inventory.id
                        "name" to Inventory.item
                        "price" to Inventory.price
                    }
                }
            }) from Users join UserOrders on {
                UserOrders.userId equal Users.id
            } join Inventory on {
                Inventory.id equal UserOrders.inventoryId
            } groupBy listOf(
                Users.id, Users.firstName, Users.lastName
            )
        }
        val expected = """
            SELECT users.id,users.first_name,users.last_name,json_agg(
                json_build_object(
                    'orderPlaced', user_orders.order_placed,
                    'inventory', json_build_object(
                        'id', inventory.id,
                        'name', inventory.name,
                        'price', inventory.price
                    )
                )
            ) from example.users 
            join example.user_orders on user_orders.user_id = users.id
            join example.inventory on inventory.id = user_orders.inventory_id 
            group by users.id,users.first_name,users.last_name
        """.lines()
            .joinToString(" ")
            .replace(Regex("""\s+"""), " ")
            .trim()
            .replace("( ", "(")
            .replace(" )", ")")
            .replace(", ", ",")
        val generated = query.buildSQLString()
        println("Generated")
        println(generated)
        println("Expected")
        println(expected)
        assertTrue { expected == generated }
    }
}
