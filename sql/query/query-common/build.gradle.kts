import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget
import org.jetbrains.kotlin.konan.target.HostManager
import design.animus.kotlin.frm.Versions.Dependencies
plugins {
    kotlin("multiplatform")
    id("org.jetbrains.dokka")
    `maven-publish`
    id("jacoco")
}

kotlin {
    val nativeMain by sourceSets.creating {
        dependencies {
            api("org.jetbrains.kotlin:kotlin-stdlib-common")
        }
    }
    jvm()
    js {
        compilations.named("main") {
            kotlinOptions {
                metaInfo = true
                sourceMap = true
                verbose = true
                moduleKind = "umd"
            }
        }
    }
    val linuxX64 by sourceSets.creating {
        dependsOn(nativeMain)
    }
    targets.withType<org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget> {
        compilations["main"].defaultSourceSet.dependsOn(nativeMain)
        when {
            HostManager.hostIsLinux -> compilations["main"].defaultSourceSet.dependsOn(linuxX64)
            else -> error("Unsupported host platform")
        }
    }
    when {
        HostManager.hostIsLinux -> {
            targetFromPreset(presets["linuxX64"], "linuxX64")
        }
        else -> error("Unsupported host platform")
    } as KotlinNativeTarget
    sourceSets {
        commonMain {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-common:${Dependencies.coroutine}")
                implementation("io.github.microutils:kotlin-logging:${Dependencies.kotlinLogging}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:${Dependencies.serialization}")
                implementation(kotlin("stdlib-common"))
                implementation("design.animus.kotlin.mp:datatypes:${Dependencies.MPDataTypes}")
            }
        }
        commonTest {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-common:${Dependencies.coroutine}")
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        named("jvmMain") {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("reflect"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Dependencies.serialization}")
                implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
            }
        }
        named("jvmTest") {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }
        named("jsMain") {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-js:${Dependencies.coroutine}")
                implementation("io.github.microutils:kotlin-logging-js:${Dependencies.kotlinLogging}")
                implementation(kotlin("stdlib-js"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-js:${Dependencies.serialization}")
                implementation("design.animus.kotlin.mp:datatypes-js:${Dependencies.MPDataTypes}")
            }
        }
        named("jsTest") {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
        named("linuxX64Main") {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-native:${Dependencies.coroutine}")
                implementation("io.github.microutils:kotlin-logging-linuxx64:${Dependencies.kotlinLogging}")

                implementation("design.animus.kotlin.mp:datatypes-linuxx64:${Dependencies.MPDataTypes}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-native:${Dependencies.serialization}")
            }
        }
    }
}

tasks {
    val jvmTest by getting(org.jetbrains.kotlin.gradle.targets.jvm.tasks.KotlinJvmTest::class) {
        this.useJUnit()
        this.reports.junitXml.isEnabled = true
    }
}
