package design.animus.kotlin.frm.sql.query.common

fun String.flattenString() = this
        .lines()
        .joinToString("")
        .replace(Regex("""\s+"""), " ")
        .replace(Regex("""\(\s+"""), "(")
        .replace(Regex("""\s+\)"""), ")")
        .replace(Regex(""",\s"""), ",")
        .trim()
        .trimIndent()