package design.animus.kotlin.frm.sql.query.common.select

import design.animus.kotlin.frm.sql.query.common.builder.nestedQuery
import design.animus.kotlin.frm.sql.query.common.builder.preparedQuery
import design.animus.kotlin.frm.sql.query.common.builder.query
import design.animus.kotlin.frm.sql.query.common.flattenString
import design.animus.kotlin.frm.sql.query.common.query.type.SimpleCharacter
import design.animus.kotlin.frm.sql.query.common.schema.Profile
import design.animus.kotlin.frm.sql.query.common.schema.Users
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue

class NestedQueryTest {
    @Test
    fun testNestedQueryInWhere() = runBlocking {
        val query = Profile.query {
            select() from Profile where {
                val a = Profile.id contains Users.nestedQuery {
                    select(Users.id) from Users where {
                        Users.firstName iLike "%ja%"
                    }
                }
            }
        }
        val sql = query.buildSQLString()
        val expected = """
            SELECT * from user_profile
            	where user_profile.id in (
            		SELECT users.id from users
            			where users.first_name ilike '%ja%'
            	)
        """.flattenString()
        println(sql)
        println(expected)
        assertTrue { sql == expected }
    }

    @Test
    fun testNestedQueryInColumn() = runBlocking {
        val query = Profile.query {
            select(Profile.id, Profile.email,
                    Users.nestedQuery { select(Users.firstName) from Users where { Users.id equal Profile.id } }.toField(),
                    Users.nestedQuery { select(Users.lastName) from Users where { Users.id equal Profile.id } }.toField()
            ) from Profile

        }
        val expected = """
            SELECT user_profile.id,user_profile.email,
            	(SELECT users.first_name from users where users.id = user_profile.id),
            	(SELECT users.last_name from users where users.id = user_profile.id)
            from user_profile
        """.flattenString()
        val sql = query.buildSQLString()
        println(sql)
        println(expected)
        assertTrue { sql == expected }
    }

    @Test
    fun testNestedPreparedQueryInWhere() = runBlocking {

        val query = Profile.preparedQuery {
            select() from Profile where {
                Profile.id contains Users.nestedQuery {
                    select(Users.id) from Users where {
                        Users.firstName iLike Users.lazyFirstName
                    }
                }
            }
        }
        val sql = query.buildSQLString(SimpleCharacter("?"))
        val expected = """
            SELECT * from user_profile
            	where user_profile.id in (
            		SELECT users.id from users
            			where users.first_name ilike ?
            	)
        """.flattenString()
        println(sql)
        println(expected)
        assertTrue { sql == expected }
    }
}