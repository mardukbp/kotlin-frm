package design.animus.kotlin.frm.sql.query.common.select

import design.animus.kotlin.frm.sql.query.common.builder.sqlQuery
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.schema.ExampleIDatabase
import design.animus.kotlin.frm.sql.query.common.schema.Users
import design.animus.kotlin.frm.sql.query.common.schema.UsersRecord
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue

class SelectAggregateTest {
    @Test
    fun testCount() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            select(count(Users.id), Users.firstName) from Users groupBy listOf(Users.firstName)
        }
        val expected = """
            SELECT count(users.id),users.first_name from users group by users.first_name
        """.trimIndent()
        val generated = query.buildSQLString()
        assertTrue { expected == generated }
    }

    @Test
    fun testSum() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            select(sum(Users.id), Users.firstName) from Users groupBy listOf(Users.firstName)
        }
        val expected = """
            SELECT sum(users.id),users.first_name from users group by users.first_name
        """.trimIndent()
        val generated = query.buildSQLString()
        println(generated)
        println(expected)
        assertTrue { expected == generated }
    }

    @Test
    fun testMax() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            select(max(Users.id), Users.firstName) from Users groupBy listOf(Users.firstName)
        }
        val expected = """
            SELECT max(users.id),users.first_name from users group by users.first_name
        """.trimIndent()
        val generated = query.buildSQLString()
        println(expected)
        println(generated)
        assertTrue { expected == generated }
    }

    @Test
    fun testMin() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            select(min(Users.id), Users.firstName) from Users groupBy listOf(Users.firstName)
        }
        val expected = """
            SELECT min(users.id),users.first_name from users group by users.first_name
        """.trimIndent()
        val generated = query.buildSQLString()
        println(generated)
        println(expected)
        assertTrue { expected == generated }
    }

    @Test
    fun testAvg() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            select(avg(Users.id), Users.firstName) from Users groupBy listOf(Users.firstName)
        }
        val expected = """
            SELECT avg(users.id),users.first_name from users group by users.first_name
        """.trimIndent()
        val generated = query.buildSQLString()
        println(generated)
        println(expected)
        assertTrue { expected == generated }
    }
}
