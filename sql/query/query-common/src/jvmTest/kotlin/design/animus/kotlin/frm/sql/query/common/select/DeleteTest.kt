package design.animus.kotlin.frm.sql.query.common.select

import design.animus.kotlin.frm.sql.query.common.builder.sqlQuery
import design.animus.kotlin.frm.sql.query.common.schema.ExampleIDatabase
import design.animus.kotlin.frm.sql.query.common.schema.Users
import design.animus.kotlin.frm.sql.query.common.schema.UsersRecord
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue

class DeleteTest {
    @Test
    fun testDelete() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            delete(Users) where { Users.firstName iLike "%ja%" }
        }
        val expected = "DELETE from users where users.first_name ilike '%ja%'"
        val generated = query.buildSQLString()
        println(generated)
        println(expected)
        assertTrue { expected == generated }
    }
}