package design.animus.kotlin.frm.sql.query.common.schema

import design.animus.kotlin.frm.sql.query.common.query.IDatabase

object ExampleIDatabase : IDatabase {
    override val database = "example"
}
