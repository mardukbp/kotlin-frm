package design.animus.kotlin.frm.sql.query.common.select

import design.animus.kotlin.frm.sql.query.common.builder.query
import design.animus.kotlin.frm.sql.query.common.query.AS
import design.animus.kotlin.frm.sql.query.common.schema.Profile
import design.animus.kotlin.frm.sql.query.common.schema.Users
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue


class SelectJoinTest {
    @Test
    fun joinTest() = runBlocking {
        val query = Users.query {
            selectJ(Users.id, Users.firstName, Profile.email) from Users join Profile on {
                (Profile.id equal Users.id)
            } where {
                (Users.firstName iLike "%ja%")
            }
        }
        val expected = """
                        SELECT users.id,users.first_name,user_profile.email
                            from users 
                            join user_profile on user_profile.id = users.id 
                            where users.first_name ilike '%ja%'
                    """.lines().joinToString("").replace(Regex("""\s+"""), " ").trim()
        println(query.buildSQLString())
        println(expected)
        val sql = query.buildSQLString()
        assertTrue { sql == expected }
    }

    @Test
    fun joinNamedTest() = runBlocking {
        val query = Users.query {
            selectJ(Users.id, Users.firstName, Profile.email) from Users join (Profile AS "profiles") on {
                (Users.id equal Profile.id)
            } where {
                (Users.firstName iLike "%ja%")
            }
        }
        val expected = """
                        SELECT users.id,users.first_name,user_profile.email
                            from users 
                            join user_profile as profiles on users.id = user_profile.id 
                            where users.first_name ilike '%ja%'
                    """.lines().joinToString("").replace(Regex("""\s+"""), " ").trim()
        val sql = query.buildSQLString()
        println(sql)
        println(expected)
        assertTrue { sql == expected }
    }

    @Test
    fun innerJoinTest() = runBlocking {
        val query = Users.query {
            selectJ(Users.id, Users.firstName, Profile.email) from Users innerJoin Profile on {
                (Users.id equal Profile.id)
            } where {
                (Users.firstName iLike "%ja%")
            }
        }
        val expected = """
                        SELECT users.id,users.first_name,user_profile.email
                            from users 
                            inner join user_profile on users.id = user_profile.id 
                            where users.first_name ilike '%ja%'
                    """.lines().joinToString("").replace(Regex("""\s+"""), " ").trim()
        val sql = query.buildSQLString()
        assertTrue { sql == expected }
    }

    @Test
    fun innerJoinNamedTest() = runBlocking {
        val query = Users.query {
            selectJ(Users.id, Users.firstName, Profile.email) from Users innerJoin (Profile AS "profiles") on {
                (Users.id equal Profile.id)
            } where {
                (Users.firstName iLike "%ja%")
            }
        }
        val expected = """
                        SELECT users.id,users.first_name,user_profile.email
                            from users 
                            inner join user_profile as profiles  on users.id = user_profile.id  
                            where users.first_name ilike '%ja%'
                    """.lines().joinToString("").replace(Regex("""\s+"""), " ").trim()
        val sql = query.buildSQLString()
        assertTrue { sql == expected }
    }

    @Test
    fun leftJoinTest() = runBlocking {
        val query = Users.query {
            selectJ(Users.id, Users.firstName, Profile.email) from Users leftJoin Profile on {
                (Users.id equal Profile.id)
            } where {
                (Users.firstName iLike "%ja%")
            }
        }
        val expected = """
                        SELECT users.id,users.first_name,user_profile.email
                            from users 
                            left join user_profile on users.id = user_profile.id 
                            where users.first_name ilike '%ja%'
                    """.lines().joinToString("").replace(Regex("""\s+"""), " ").trim()
        val sql = query.buildSQLString()
        assertTrue { sql == expected }
    }

    @Test
    fun leftJoinNamedTest() = runBlocking {
        val query = Users.query {
            selectJ(Users.id, Users.firstName, Profile.email) from Users leftJoin (Profile AS "profiles") on {
                (Users.id equal Profile.id)
            } where {
                (Users.firstName iLike "%ja%")
            }
        }
        val sql = query.buildSQLString()
        val expected = """
                        SELECT users.id,users.first_name,user_profile.email
                            from users 
                            left join user_profile as profiles  on  users.id = user_profile.id 
                            where users.first_name ilike '%ja%'
                    """.lines().joinToString("").replace(Regex("""\s+"""), " ").trim()
        assertTrue { sql == expected }
    }


}
