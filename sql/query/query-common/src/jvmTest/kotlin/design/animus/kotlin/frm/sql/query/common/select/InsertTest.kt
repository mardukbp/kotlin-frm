package design.animus.kotlin.frm.sql.query.common.select

import design.animus.kotlin.frm.sql.query.common.builder.sqlQuery
import design.animus.kotlin.frm.sql.query.common.schema.ExampleIDatabase
import design.animus.kotlin.frm.sql.query.common.schema.Users
import design.animus.kotlin.frm.sql.query.common.schema.UsersRecord
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue

class InsertTest {
    @Test
    fun insertQuery() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            insert(Users) columns listOf(Users.firstName, Users.lastName) values listOf(
                listOf("John", "Doe"),
                listOf("Jane", "Doe")
            )
        }
        println(query)
        val sql = query.buildSQLString()
        val expected = "INSERT into users (users.first_name,users.last_name) values ('John','Doe'),('Jane','Doe')"
        println(sql)
        println(expected)
        assertTrue { sql == expected }
    }

    @Test
    fun insertReturningQuery() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            insert(Users) columns listOf(Users.firstName, Users.lastName) values listOf(
                listOf("John", "Doe"),
                listOf("Jane", "Doe")
            ) returning listOf(Users.id)
        }
        println(query)
        val sql = query.buildSQLString()
        val expected = "INSERT into users (users.first_name,users.last_name) values ('John','Doe'),('Jane','Doe') returning users.id"
        println(sql)
        println(expected)
        assertTrue { sql == expected }
    }
}