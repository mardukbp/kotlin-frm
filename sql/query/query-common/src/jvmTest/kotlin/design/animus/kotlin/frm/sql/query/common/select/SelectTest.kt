package design.animus.kotlin.frm.sql.query.common.select

import design.animus.kotlin.frm.sql.query.common.builder.query
import design.animus.kotlin.frm.sql.query.common.builder.sqlQuery
import design.animus.kotlin.frm.sql.query.common.query.AS
import design.animus.kotlin.frm.sql.query.common.schema.ExampleIDatabase
import design.animus.kotlin.frm.sql.query.common.schema.Users
import design.animus.kotlin.frm.sql.query.common.schema.UsersRecord
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertTrue

class SelectTest {
    @Test
    fun testQuerySelectAll() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> { select() from Users }.buildSQLString()
        val expected = "SELECT * from users"
        println(query)
        println(expected)
        assertTrue { query == expected }
    }

    @Test
    fun testQuerySelectOneField() = runBlocking {
        val query =
            sqlQuery<ExampleIDatabase, UsersRecord, Users> { select(Users.firstName) from Users }.buildSQLString()
        val expected = "SELECT users.first_name from users"
        println(query)
        println(expected)
        assertTrue { query == expected }
    }

    @Test
    fun testQuerySelectOneNamedField() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            select(Users.firstName AS "first_name") from Users
        }.buildSQLString()
        val expected = "SELECT users.first_name as first_name from users"
        println(expected)
        println(query)
        assertTrue { query == expected }
    }

    @Test
    fun testQuerySelectTwoFields() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            select(
                Users.firstName,
                Users.lastName
            ) from Users
        }.buildSQLString()
        val expected = "SELECT users.first_name,users.last_name from users"
        println(query)
        println(expected)
        assertTrue { query == expected }
    }

    @Test
    fun testQueryWithOneCondition() {
        runBlocking {
            val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
                select() from Users where {
                    (Users.id equal 1)
                }
            }.buildSQLString()
            val expected = "SELECT * from users where users.id = 1"
            println(expected)
            println(query)
            assertTrue {
                query == expected
            }
        }
    }

    @Test
    fun testQueryWithTwoConditions() {
        runBlocking {
            val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
                select() from Users where {
                    (Users.id equal 1) and (Users.firstName equal "John")
                }
            }
            val generated = query.buildSQLString()
            println(generated)
            val expected = "SELECT * from users where users.id = '1' AND users.first_name = 'John'"
            println(expected)
            assertTrue { generated == expected }
        }
    }

    @Test
    fun testQueryWithThreeConditions() {
        runBlocking {
            val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
                select() from Users where {
                    (Users.id equal 1) and (Users.firstName equal "John") or (Users.lastName equal "Doe")
                }
            }
            val generated = query.buildSQLString()
            val expected =
                "SELECT * from users where users.id = '1' AND users.first_name = 'John' OR users.last_name = 'Doe'"
            println(generated)
            println(expected)
            assertTrue { generated == expected }
        }
    }

    @Test
    fun testQueryAllWithNamedTable() = runBlocking {

        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            select() from (Users AS "users_table")
        }.buildSQLString()
        val expected = "SELECT * from users as users_table"
        println(query)
        println(expected)
        assertTrue { query == expected }
    }

    @Test
    fun testQueryAllWithNamedTableAndOneCondition() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            select() from (Users AS "users_table") where {
                Users.id equal 1L
            }
        }
        val sql = query.buildSQLString()
        assertTrue {
            sql == "SELECT * from users as users_table where users_table.id = 1"
        }
    }
}