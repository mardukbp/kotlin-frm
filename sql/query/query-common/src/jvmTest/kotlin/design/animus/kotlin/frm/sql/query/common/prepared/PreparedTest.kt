package design.animus.kotlin.frm.sql.query.common.prepared

import design.animus.kotlin.frm.sql.query.common.builder.preparedQuery
import design.animus.kotlin.frm.sql.query.common.builder.sqlPreparedQuery
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.type.SimpleCharacter
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.common.schema.ExampleIDatabase
import design.animus.kotlin.frm.sql.query.common.schema.Profile
import design.animus.kotlin.frm.sql.query.common.schema.Users
import design.animus.kotlin.frm.sql.query.common.schema.UsersRecord
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue

class PreparedTest {
    @Test
    fun testPreparedSelect() = runBlocking {
        val query = sqlPreparedQuery<ExampleIDatabase, UsersRecord, Users> {
            select() from Users where { Users.firstName equal Users.lazyFirstName }

        }
        val expected = "SELECT * from users where users.first_name = ?"
        val generated = query.buildSQLString(SimpleCharacter("?"))
        println(generated)
        println(expected)
        assertTrue { generated == expected }
    }

    @Test
    fun testPreparedSelectJoin() = runBlocking {
        val query = sqlPreparedQuery<ExampleIDatabase, ADatabaseRecord, ITable<ExampleIDatabase, ADatabaseRecord>> {
            selectJ() from Users join Profile on {
                Profile.id equal Profile.lazyId
            } where { Users.firstName equal Users.lazyFirstName }

        }
        val expected = "SELECT * from users join user_profile on user_profile.id = ? where users.first_name = ?"
        val generated = query.buildSQLString(SimpleCharacter("?"))
        println(generated)
        println(expected)
        assertTrue { generated == expected }
    }

    @Test
    fun testPreparedDelete() = runBlocking {
        val query = sqlPreparedQuery<ExampleIDatabase, UsersRecord, Users> {
            delete(Users) where {
                (Users.firstName equal Users.lazyFirstName) and (Users.lastName equal Users.lazyLastName)
            }

        }
        val expected = "DELETE from users where users.first_name = ? AND users.last_name = ?"
        val generated = query.buildSQLString(SimpleCharacter("?"))
        println(generated)
        println(expected)
        assertTrue { generated == expected }
    }

    @Test
    fun testPreparedUpdate() = runBlocking {
        val query = Users.preparedQuery {
            update(Users) set {
                Users.firstName TO Users.lazyFirstName
                Users.lastName TO Users.lazyLastName
            } where {
                Users.id equal Users.lazyId
            }
        }
        val expected = "update users SET users.first_name = ?,users.last_name = ? where users.id = ?"
        val generated = query.buildSQLString(SimpleCharacter("?"))
        println(generated)
        println(expected)
    }
}