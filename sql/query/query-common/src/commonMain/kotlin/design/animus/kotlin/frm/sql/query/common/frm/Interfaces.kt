package design.animus.kotlin.frm.sql.query.common.frm

import mu.KLogger

interface IFRM<QueryType> {
    val logger: KLogger
    val query: QueryType
}