package design.animus.kotlin.frm.sql.query.common.frm.prepare

import design.animus.kotlin.frm.sql.executors.common.*
import design.animus.kotlin.frm.sql.query.common.SQLConditionalBuilder
import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.frm.IFRM
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.type.PreparedSelectJoinQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlinx.coroutines.Dispatchers
import mu.KotlinLogging
import kotlin.coroutines.CoroutineContext

class PreparedSelectJoinFRM<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
    val table: T,
    sqlBuilder: ISQLBuilder<D, R, T>
) : IFRM<PreparedSelectJoinQuery<D, R, T>> {
    override val logger = KotlinLogging.logger {}
    override var query: PreparedSelectJoinQuery<D, R, T> =
        PreparedSelectJoinQuery(sqlBuilder, Table(table))


    suspend fun filter(block: suspend SQLConditionalBuilder<D,R,T>.() -> Unit): PreparedSelectJoinFRM<D, R, T> {
        val table = this
        val state = SQLConditionalBuilder<D,R,T>()
        state.block()
        query = query.copy(conditions = state.conditions)
        return this
    }

    @Suppress("UNCHECKED_CAST")
    suspend fun withColumns(vararg inColumns: Fields<*, D, *, *>): PreparedSelectJoinFRM<D, R, T> {
        query = query.copy(columns = inColumns.toList() as List<Fields<*, D, R, T>>)
        return this
    }

    @Suppress("UNCHECKED_CAST")
    suspend fun groupBy(vararg inColumns: Fields<*, D, *, *>): PreparedSelectJoinFRM<D, R, T> {
        query = query.copy(groupBy = inColumns.toList() as List<Fields<*, D, R, T>>)
        return this
    }

    suspend fun <C : IExecutorConfig, RSLT, RSP> mapOne(
        executor: IAsyncExecutor<C, RSLT>,
        vararg values: Any,
        ctx: CoroutineContext = Dispatchers.Default,
        block: suspend (R) -> RSP
    ): AsyncDBResponse<D, R, T, C, RSLT, RSP> {
        val arguments = query.bind(*values)
        return executor.preparedFetchAsync<D, R, T, R, Any, RSP>(query, SingleOfRecord, arguments.toList(), ctx, block)
    }

    suspend fun <C : IExecutorConfig, RSLT, RSP> map(
        executor: IAsyncExecutor<C, RSLT>,
        vararg values: Any,
        ctx:CoroutineContext=Dispatchers.Default,
        block: suspend (List<R>) -> RSP
    ): AsyncDBResponse<D, R, T, C, RSLT, RSP> {
        val arguments = query.bind(*values)
        return executor.preparedFetchAsync<D, R, T, List<R>, Any, RSP>(query, ListOfRecord, arguments.toList(), ctx,block)
    }

    suspend fun <JR : ADatabaseRecord, JT : ITable<D, JR>> join(
        table: JT, conditionBuilder: suspend SQLConditionalBuilder<D,JR,JT>.() -> Unit
    ): PreparedSelectJoinFRM<D, R, T> {
        val state = SQLConditionalBuilder<D,JR,JT>()
        state.conditionBuilder()
        val newJoinStatements = query.joinStatements.toMutableList()
        newJoinStatements.add(Join(Table(table), state.conditions))
        query = query.copy(joinStatements = newJoinStatements.toList())
        return this
    }
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
        lazyPreparedSelectJoin(
    table: T,
    sqlBuilder: ISQLBuilder<D, R, T> = CommonSQLBuilder<D, R, T>()
): PreparedSelectJoinFRM<D, R, T> =
    PreparedSelectJoinFRM<D, R, T>(table, sqlBuilder)