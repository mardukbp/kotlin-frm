package design.animus.kotlin.frm.sql.query.common.frm.prepare

import design.animus.kotlin.frm.sql.executors.common.AsyncDBResponse
import design.animus.kotlin.frm.sql.executors.common.IAsyncExecutor
import design.animus.kotlin.frm.sql.executors.common.IExecutorConfig
import design.animus.kotlin.frm.sql.executors.common.Save
import design.animus.kotlin.frm.sql.query.common.SQLConditionalBuilder
import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.UpdateSet
import design.animus.kotlin.frm.sql.query.common.frm.IFRM
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.SQLVerbs
import design.animus.kotlin.frm.sql.query.common.query.Table
import design.animus.kotlin.frm.sql.query.common.query.type.PreparedUpdateQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlinx.coroutines.Dispatchers
import mu.KotlinLogging
import kotlin.coroutines.CoroutineContext

class PreparedUpdateFRM<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
    val table: T,
    val sqlBuilder: ISQLBuilder<D, R, T>
) : IFRM<PreparedUpdateQuery<D, R, T>> {
    override val logger = KotlinLogging.logger {}
    override var query = PreparedUpdateQuery<D, R, T>(sqlBuilder, listOf(), Table(table), listOf(), SQLVerbs.UPDATE)

    suspend fun mutateTo(block: UpdateSet<D, R, T>.() -> Unit)
            : PreparedUpdateFRM<D, R, T> {
        val state = UpdateSet<D, R, T>()
        state.block()
        query = query.copy(changes = state.changes)
        return this
    }

    suspend fun changeOn(block: suspend  SQLConditionalBuilder<D,R,T>.() -> Unit)
            : PreparedUpdateFRM<D, R, T> {
        val state = SQLConditionalBuilder<D,R,T>()
        state.block()
        query = query.copy(conditions = state.conditions)
        return this
    }

    suspend fun <C : IExecutorConfig, RSLT> save(executor: IAsyncExecutor<C, RSLT>, vararg arguments: Any?, ctx:CoroutineContext= Dispatchers.Default)
            : AsyncDBResponse<D, R, T, C, RSLT, Boolean> {
        val values = query.bind(*arguments)
        println(values)
        return executor.preparedFetchAsync<D, R, T, Boolean, Any, Boolean>(query, Save, values, ctx) { true }
    }
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
        lazyPreparedUpdate(
    table: T,
    sqlBuilder: ISQLBuilder<D, R, T> = CommonSQLBuilder<D, R, T>()
): PreparedUpdateFRM<D, R, T> = PreparedUpdateFRM<D, R, T>(table, sqlBuilder)