package design.animus.kotlin.frm.sql.executors.common.exceptions

import design.animus.kotlin.frm.sql.query.common.SQLType
import design.animus.kotlin.frm.sql.query.common.query.ConditionalSteps
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.type.BoundArguments
import design.animus.kotlin.frm.sql.query.common.query.type.PreparedQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

interface IExecutorThrowable
class NotConnected(message: String) : IExecutorThrowable, Exception(message)
class EmptyResultSet(message: String) : IExecutorThrowable, Exception(message)
class TooManyResults(message: String) : IExecutorThrowable, Exception(message)
class UnexpectedException(message: String, error: Throwable) : IExecutorThrowable, Exception(message)
class MisMatchColumnSetForInsert(message: String) : IExecutorThrowable, Exception(message)
class NoValuesToAdd(message: String) : IExecutorThrowable, Exception(message)
class LazyConditionsNotEqualToArguments(
        message: String,
        lazyConditions: List<ConditionalSteps>,
        arguments: List<Any?>
) : IExecutorThrowable, Exception(message)

class LazyConditionTypeMismatch(
        message: String,
        val position: Int,
        val expectedType: String,
        val argumentType: String
) : IExecutorThrowable, Exception(message)

class LazyConditionsNotCalculated(
        message: String,
        val query: PreparedQuery<IDatabase, ADatabaseRecord, ITable<IDatabase, ADatabaseRecord>, BoundArguments<IDatabase, ADatabaseRecord, ITable<IDatabase, ADatabaseRecord>>>
) : IExecutorThrowable, Exception(message)

class UnsupportedQueryOperation(message: String) : IExecutorThrowable, Exception(message)
class ReflectionUnknownType(message: String, val givenType: SQLType) : Exception(message)