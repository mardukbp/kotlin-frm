package design.animus.kotlin.frm.sql.query.common.builder


import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.type.*
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

@Suppress("UNCHECKED_CAST")
class CommonSQLQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> {
    lateinit var mutableQuery: MutableQuery<D, R, T>


    //Select
    suspend fun select() = SelectMutableQuery<D, R, T>(listOf(), conditions = listOf())

    suspend fun select(vararg fields: Fields<*, D, R, T>) =
            SelectMutableQuery<D, R, T>(fields.toList(), conditions = listOf())

    suspend fun selectJ(vararg fields: Fields<*, D, *, *>) =
            SelectJoinMutableQuery<D, R, T>(
                    fields.toList() as List<Fields<*, D, ADatabaseRecord, ITable<D, ADatabaseRecord>>>,
                    conditions = listOf()
            )
    // End Select

    //Update
    suspend fun update(inTable: T) = UpdateMutableQuery<D, R, T>(listOf()).apply {
        table = Table(inTable)
    }

    suspend fun update(inTable: NamedTable<D, R, T>) = UpdateMutableQuery<D, R, T>(listOf()).apply {
        table = inTable
    }
    // End Update

    // Begin Insert
    suspend fun insert(inTable: T) = InsertMutableQuery<D, R, T>().apply {
        table = Table(inTable)
    }

    suspend fun insert(inTable: NamedTable<D, R, T>) = InsertMutableQuery<D, R, T>().apply {
        table = inTable
    }
    // End Insert

    //Begin Delete
    suspend fun delete(inTable: T) = DeleteMutableQuery<D, R, T>(listOf()).apply {
        table = Table(inTable)
    }

    suspend fun delete(inTable: NamedTable<D, R, T>) = DeleteMutableQuery<D, R, T>(listOf()).apply {
        table = inTable
    }
    // End Delete
}

// Non Prepared
suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> sqlQuery(
        builder: ISQLBuilder<D, R, T> = CommonSQLBuilder(),
        block: suspend CommonSQLQuery<D, R, T>.() -> MutableQuery<D, R, T>
): LockedQuery<D, R, T> {
    val state = CommonSQLQuery<D, R, T>()
    val generatedQuery = state.block()
    return generatedQuery.lockQuery(builder)
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> T.query(
        builder: ISQLBuilder<D, R, T> = CommonSQLBuilder(),
        block: suspend CommonSQLQuery<D, R, T>.() -> MutableQuery<D, R, T>
): LockedQuery<D, R, T> {
    val state = CommonSQLQuery<D, R, T>()
    val generatedQuery = state.block()
    return generatedQuery.lockQuery(builder)
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> T.nestedQuery(
        builder: ISQLBuilder<D, R, T> = CommonSQLBuilder(),
        block: suspend CommonSQLQuery<D, R, T>.() -> MutableQuery<D, R, T>
) = this.query(builder, block)
// End Non Prepared

// Prepared
suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> sqlPreparedQuery(
        builder: ISQLBuilder<D, R, T> = CommonSQLBuilder(),
        preparedPlacedHolder: PreparedPlacedHolder = SimpleCharacter("?"),
        block: suspend CommonSQLQuery<D, R, T>.() -> MutableQuery<D, R, T>
): PreparedQuery<D, R, T, *> {
    val state = CommonSQLQuery<D, R, T>()
    val generatedQuery = state.block()
    val lockedQuery = generatedQuery.lockQuery(builder)
    return PreparedQuery.fromLockedQuery(lockedQuery)
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> T.preparedQuery(
        builder: ISQLBuilder<D, R, T> = CommonSQLBuilder(),
        block: suspend CommonSQLQuery<D, R, T>.() -> MutableQuery<D, R, T>
): PreparedQuery<D, R, T, *> {
    val state = CommonSQLQuery<D, R, T>()
    val generatedQuery = state.block()
    val lockedQuery = generatedQuery.lockQuery(builder)
    return PreparedQuery.fromLockedQuery(lockedQuery)
}
// End Prepared

