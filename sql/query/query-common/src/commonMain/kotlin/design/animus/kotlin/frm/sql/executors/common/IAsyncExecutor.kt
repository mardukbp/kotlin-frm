package design.animus.kotlin.frm.sql.executors.common

import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.type.*
import design.animus.kotlin.frm.sql.query.common.query.verbs.IDeleteQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import mu.KLogger
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass

interface IExecutorConfig {
    val databaseHost: String
    val database: String
    val databaseUser: String
    val databasePassword: String
    val databasePort: Int
}

interface IAsyncExecutor<C : IExecutorConfig, RSLT> {
    val logger: KLogger
    var connected: Boolean
    val preparedPlaceHolder: PreparedPlacedHolder
    suspend fun connect(config: C): Boolean
    suspend fun disconnect(): Boolean

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> getColumnNames(query: IBaseQuery<D, R, T>) = when (query) {
        is SelectQuery -> query.columns.map { it.columnName }
        is PreparedSelectQuery -> query.columns.map { it.columnName }
        is SelectJoinQuery -> query.columns.map { it.columnName }
        is PreparedSelectJoinQuery -> query.columns.map { it.columnName }
        is InsertQuery -> query.returningFields.fold(
                { listOf<String>() },
                { rsp -> rsp.map { it.columnName } }
        )
        is PreparedInsertQuery -> query.returning.fold(
                { listOf<String>() },
                { rsp -> rsp.map { it.columnName } }
        )
        is UpdateQuery -> query.returningFields.fold(
                { listOf<String>() },
                { rsp -> rsp.map { it.columnName } }
        )
        is PreparedUpdateQuery -> query.returning.fold(
                { listOf<String>() },
                { rsp -> rsp.map { it.columnName } }
        )
        is DeleteQuery -> listOf()
        else -> listOf()
    }

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN, TargetClass : Any, RSP>
            fetchAsync(
            query: IQuery<D, R, T>,
            responseType: ExpectedResponses,
            ctx: CoroutineContext,
            block: suspend (IN) -> RSP
    ): AsyncDBResponse<D, R, T, C, RSLT, RSP>

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN, TargetClass : Any, RSP>
            preparedFetchAsync(
            preparedQuery: IPreparedQuery<D, R, T>,
            responseType: ExpectedResponses,
            values: BoundArguments<D, R, T>,
            ctx: CoroutineContext,
            block: suspend (IN) -> RSP
    ): AsyncDBResponse<D, R, T, C, RSLT, RSP>

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
            save(query: ISaveQuery<D, R, T>)
            : AsyncDBResponse<D, R, T, C, RSLT, Boolean>

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
            delete(query: IDeleteQuery<D, R, T>)
            : AsyncDBResponse<D, R, T, C, RSLT, Boolean>

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN>
            unpackDBFuture(result: RSLT, query: IBaseQuery<D, R, T>): IN

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN>
            unpackDBFutures(result: RSLT, query: IBaseQuery<D, R, T>)
            : IN

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN>
            unpackDBFutureAsMap(result: RSLT, query: IBaseQuery<D, R, T>)
            : IN

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, IN>
            unpackDBFutureAsMultiMap(result: RSLT, query: IBaseQuery<D, R, T>)
            : IN

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, TargetClass : Any, IN, RSP>
            unpackDBFutureAsCustomClass(result: RSLT, targetClass: KClass<TargetClass>, query: IBaseQuery<D, R, T>)
            : IN

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, TargetClass : Any, IN, RSP>
            unpackDBFutureAsListOfCustomClass(
            result: RSLT,
            targetClass: KClass<TargetClass>,
            query: IBaseQuery<D, R, T>
    ): IN

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, TargetClass : Any, IN, RSP>
            unpackDBSave(result: RSLT)
            : IN

    suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, TargetClass : Any, IN, RSP>
            unpackDBDelete(result: RSLT, query: IDeleteQuery<D, R, T>)
            : IN
}
