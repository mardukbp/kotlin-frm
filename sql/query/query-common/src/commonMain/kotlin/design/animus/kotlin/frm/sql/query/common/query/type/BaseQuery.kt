package design.animus.kotlin.frm.sql.query.common.query.type

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.SQLQuery
import design.animus.kotlin.frm.sql.query.common.frm.LazyInsertPair
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.verbs.UpdateChange
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

interface IBaseQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> {
    val sqlBuilder: ISQLBuilder<D, R, T>
    val verb: SQLVerbs
    val table: QueryTable

}

interface IQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : IBaseQuery<D, R, T> {
    suspend fun buildSQLString(): SQLQuery
}

interface ISaveQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : IQuery<D, R, T>

interface IPreparedQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : IBaseQuery<D, R, T> {
    suspend fun buildSQLString(variable: PreparedPlacedHolder): SQLQuery
}

interface IPreparedSelectQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : IPreparedQuery<D, R, T> {
    val columns: List<Fields<*, *, *, *>>
    val conditions: List<ConditionalSteps>
    val groupBy: List<Fields<*, *, *, *>>
}

interface IPreparedSelectJoinQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> :
        IPreparedSelectQuery<D, R, T> {
    val joinStatements: List<JoinType>
}

interface IPreparedSaveQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : IPreparedQuery<D, R, T>

interface IPreparedDeleteQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : IPreparedQuery<D, R, T> {
    val conditions: List<ConditionalSteps>

}

interface IPreparedInsertQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : IPreparedSaveQuery<D, R, T> {
    val newItems: List<LazyInsertPair<D, R, T>>
    val returning: Option<List<Fields<Any?, D, R, T>>>
}

interface IPreparedUpdateQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : IPreparedQuery<D, R, T> {
    val conditions: List<ConditionalSteps>
    val changes: List<UpdateChange<*, D, R, T>>
    val returning: Option<List<Fields<Any?, D, R, T>>>
}