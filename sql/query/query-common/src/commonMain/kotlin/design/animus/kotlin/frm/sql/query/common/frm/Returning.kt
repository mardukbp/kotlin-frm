package design.animus.kotlin.frm.sql.query.common.frm

import design.animus.kotlin.frm.sql.executors.common.*
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.type.LockedQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass

class ReturnMultipleCustom<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val query: LockedQuery<D, R, T>
) {
    suspend fun <C : IExecutorConfig, RSLT, RSP>
            saveAndReturn(asyncExecutor: IAsyncExecutor<C, RSLT>,
                          ctx: CoroutineContext = Dispatchers.Default,
                          block: suspend (List<Map<String, Any?>>) -> RSP): AsyncDBResponse<D, R, T, C, RSLT, RSP> =
            asyncExecutor.fetchAsync<D, R, T, List<Map<String, Any>>, Any, RSP>(query, SaveAndReturnMultiple, ctx, block)

    suspend fun <C : IExecutorConfig, RSLT, RSP, Target : KClass<Any>>
            saveAndReturn(asyncExecutor: IAsyncExecutor<C, RSLT>,
                          targetClass: Target,
                          ctx: CoroutineContext = Dispatchers.Default,
                          block: suspend (List<Target>) -> RSP): AsyncDBResponse<D, R, T, C, RSLT, RSP> =
            asyncExecutor.fetchAsync<D, R, T, List<Target>, Target, RSP>(query, SaveAndReturnWithCustomRecordMultiple(targetClass), ctx, block)

    suspend fun <C : IExecutorConfig, RSLT>
            defer(asyncExecutor: IAsyncExecutor<C, RSLT>,
                  ctx: CoroutineContext = Dispatchers.Default): AsyncDBResponse<D, R, T, C, RSLT, List<Map<String, Any?>>> =
            asyncExecutor.fetchAsync<D, R, T, List<Map<String, Any?>>, Any, List<Map<String, Any?>>>(query, SaveAndReturnMultiple, ctx) { it }

    suspend fun <C : IExecutorConfig, RSLT, Target : KClass<Any>>
            defer(asyncExecutor: IAsyncExecutor<C, RSLT>,
                  targetClass: Target,
                  ctx: CoroutineContext = Dispatchers.Default): AsyncDBResponse<D, R, T, C, RSLT, List<Target>> =
            asyncExecutor.fetchAsync<D, R, T, List<Target>, Target, List<Target>>(query, SaveAndReturnWithCustomRecordMultiple(targetClass), ctx) { it }
}

class ReturnSingleCustom<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val query: LockedQuery<D, R, T>
) {
    suspend fun <C : IExecutorConfig, RSLT, RSP>
            saveAndReturn(asyncExecutor: IAsyncExecutor<C, RSLT>,
                          ctx: CoroutineContext = Dispatchers.Default,
                          block: suspend (Map<String, Any?>) -> RSP): AsyncDBResponse<D, R, T, C, RSLT, RSP> =
            asyncExecutor.fetchAsync<D, R, T, Map<String, Any>, Any, RSP>(query, SaveAndReturn, ctx, block)

    suspend fun <C : IExecutorConfig, RSLT, RSP, Target : KClass<Any>>
            saveAndReturn(asyncExecutor: IAsyncExecutor<C, RSLT>,
                          targetClass: Target,
                          ctx: CoroutineContext = Dispatchers.Default,
                          block: suspend (Target) -> RSP): AsyncDBResponse<D, R, T, C, RSLT, RSP> =
            asyncExecutor.fetchAsync<D, R, T, Target, Target, RSP>(query, SaveAndReturnWithCustomRecord(targetClass), ctx, block)

    suspend fun <C : IExecutorConfig, RSLT, Target : KClass<Any>>
            defer(asyncExecutor: IAsyncExecutor<C, RSLT>,
                  ctx: CoroutineContext = Dispatchers.Default): AsyncDBResponse<D, R, T, C, RSLT, Map<String, Any?>> =
            asyncExecutor.fetchAsync<D, R, T, Map<String, Any?>, Any, Map<String, Any?>>(query, SaveAndReturn, ctx) { it }

    suspend fun <C : IExecutorConfig, RSLT, Target : KClass<Any>>
            defer(asyncExecutor: IAsyncExecutor<C, RSLT>,
                  targetClass: Target,
                  ctx: CoroutineContext = Dispatchers.Default): AsyncDBResponse<D, R, T, C, RSLT, Target> =
            asyncExecutor.fetchAsync<D, R, T, Target, Target, Target>(query, SaveAndReturnWithCustomRecord(targetClass), ctx) { it }
}

class ReturnMultipleRecord<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val query: LockedQuery<D, R, T>
) {
    suspend fun <C : IExecutorConfig, RSLT, RSP>
            saveAndReturn(asyncExecutor: IAsyncExecutor<C, RSLT>,
                          ctx: CoroutineContext = Dispatchers.Default,
                          block: suspend (List<R>) -> RSP): AsyncDBResponse<D, R, T, C, RSLT, RSP> =
            asyncExecutor.fetchAsync<D, R, T, List<R>, Any, RSP>(query, SaveAndReturnListOfRecord, ctx, block)

    suspend fun <C : IExecutorConfig, RSLT>
            defer(asyncExecutor: IAsyncExecutor<C, RSLT>,
                  ctx: CoroutineContext = Dispatchers.Default): AsyncDBResponse<D, R, T, C, RSLT, List<R>> =
            asyncExecutor.fetchAsync<D, R, T, List<R>, Any, List<R>>(query, SaveAndReturnListOfRecord, ctx) { it }
}


class ReturnSingleRecord<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val query: LockedQuery<D, R, T>
) {
    suspend fun <C : IExecutorConfig, RSLT, RSP>
            saveAndReturn(asyncExecutor: IAsyncExecutor<C, RSLT>,
                          ctx: CoroutineContext = Dispatchers.Default,
                          block: suspend (R) -> RSP): AsyncDBResponse<D, R, T, C, RSLT, RSP> =
            asyncExecutor.fetchAsync<D, R, T, R, Any, RSP>(query, SaveAndReturnRecord, ctx, block)

    suspend fun <C : IExecutorConfig, RSLT, Target : KClass<Any>>
            defer(asyncExecutor: IAsyncExecutor<C, RSLT>,
                  ctx: CoroutineContext = Dispatchers.Default): AsyncDBResponse<D, R, T, C, RSLT, List<R>> =
            asyncExecutor.fetchAsync<D, R, T, List<R>, Any, List<R>>(query, SaveAndReturnRecord, ctx) { it }
}