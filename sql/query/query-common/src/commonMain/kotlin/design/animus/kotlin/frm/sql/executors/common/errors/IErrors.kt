package design.animus.kotlin.frm.sql.executors.common.errors

interface IQueryError

interface IConditionalError : IQueryError