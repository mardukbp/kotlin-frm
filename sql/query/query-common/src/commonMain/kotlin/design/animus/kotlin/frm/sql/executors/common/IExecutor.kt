package design.animus.kotlin.frm.sql.executors.common

import design.animus.kotlin.frm.sql.query.common.query.type.PreparedPlacedHolder
import mu.KLogger

interface IExecutor<C : IExecutorConfig, RSLT> {
    val logger: KLogger
    var connected: Boolean
    val preparedPlaceHolder: PreparedPlacedHolder
}