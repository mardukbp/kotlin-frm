package design.animus.kotlin.frm.sql.query.common.frm

import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.SQLVerbs
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord


suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> T.lazy(verb: SQLVerbs) = Unit