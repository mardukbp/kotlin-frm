package design.animus.kotlin.frm.sql.query.common.builder

import design.animus.kotlin.frm.sql.query.common.query.Field
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.LateField
import design.animus.kotlin.frm.sql.query.common.query.verbs.UpdateChange
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

class UpdateSet<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> {
    val changes: MutableList<UpdateChange<*, D, R, T>> = mutableListOf()

    infix fun <C : Any?, F : Field<C, D, R, T>> F.TO(value: C): UpdateChange<C, D, R, T> {
        val change = UpdateChange<C, D, R, T>(this, value)
        changes.add(change)
        return change
    }

    infix fun <C : Any?, F : Field<C, D, R, T>> F.TO(value: LateField<C, D, R, T>?): UpdateChange<C, D, R, T> {
        val change = UpdateChange<C, D, R, T>(
            this as Field<C, D, R, T>,
            null,
            lateField = value
        )
        changes.add(change)
        return change
    }
}
