package design.animus.kotlin.frm.sql.query.common.frm

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.frm.sql.executors.common.*
import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.type.InsertMutableQuery
import design.animus.kotlin.frm.sql.query.common.query.type.InsertQuery
import design.animus.kotlin.frm.sql.query.common.query.type.LockedQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlinx.coroutines.Dispatchers
import mu.KLogger
import mu.KotlinLogging
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass

typealias InsertPair<D, R, T> = List<Pair<Fields<*, D, R, T>, Any?>>
typealias LazyInsertPair<D, R, T> = List<Pair<Fields<Any?, D, R, T>, LateField<Any?, D, R, T>>>

@Suppress("UNCHECKED_CAST")
class InsertFRM<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val table: T,
        val sqlBuilder: ISQLBuilder<D, R, T>
) : IFRM<InsertMutableQuery<D, R, T>> {
    override val logger = KotlinLogging.logger {}
    override var query = InsertMutableQuery<D, R, T>()
    suspend fun addOne(item: InsertPair<D, R, T>): InsertAddSingle<D, R, T> {
        val newQuery = query.copy(
                columns = item.map { it.first },
                columnValues = listOf(item.map { it.second })
        ).apply { table = Table(this@InsertFRM.table) }
        return InsertAddSingle(sqlBuilder, newQuery, table, logger)
    }

    suspend fun add(values: List<InsertPair<D, R, T>>): InsertAddMultiple<D, R, T> {
        val columns = values.map { it.map { col -> col.first } }.toSet()
        if (columns.size > 1) throw UncommonInsertColumns("Found more than one type of columns, while only one was anticipated.")
        val newQuery = query.copy(
                columns = columns.first(),
                columnValues = values.map { set -> set.map { item -> item.second } }
        ).apply { table = Table(this@InsertFRM.table) }
        return InsertAddMultiple(sqlBuilder, newQuery, table, logger)
    }
}

class InsertAddMultiple<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val sqlBuilder: ISQLBuilder<D, R, T>,
        val query: InsertMutableQuery<D, R, T>,
        val table: T,
        val logger: KLogger = KotlinLogging.logger { }) {
    suspend fun <C : Any?> returning(vararg fields: Fields<C, D, R, T>): ReturnMultipleCustom<D, R, T> {
        val castFields = fields.toList() as List<Fields<Any?, D, R, T>>
        val newQuery = query.copy(
                returningFields = Option.Some(castFields)
        ).apply { table = Table(this@InsertAddMultiple.table) }
        return ReturnMultipleCustom(newQuery.lockQuery(sqlBuilder))
    }

    suspend fun returning(): ReturnMultipleRecord<D, R, T> {
        val newQuery = query.copy(returningFields = Option.Some(listOf())).apply { table = Table(this@InsertAddMultiple.table) }
        return ReturnMultipleRecord(newQuery.lockQuery(sqlBuilder))
    }

    suspend fun <C : IExecutorConfig, RSLT>
            save(asyncExecutor: IAsyncExecutor<C, RSLT>) =
            asyncExecutor.save(
                    query.lockQuery(sqlBuilder) as InsertQuery<D, R, T>
            )
}

class InsertAddSingle<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val sqlBuilder: ISQLBuilder<D, R, T>,
        val query: InsertMutableQuery<D, R, T>,
        val table: T,
        val logger: KLogger = KotlinLogging.logger { }) {
    suspend fun <C : Any?> returning(vararg fields: Fields<C, D, R, T>): ReturnSingleCustom<D, R, T> {
        val castFields = fields.toList() as List<Fields<Any?, D, R, T>>
        val newQuery = query.copy(
                returningFields = Option.Some(castFields)
        ).apply { table = Table(this@InsertAddSingle.table) }
        return ReturnSingleCustom(newQuery.lockQuery(sqlBuilder))
    }

    suspend fun returning(): ReturnSingleRecord<D, R, T> {
        val newQuery = query.copy(returningFields = Option.Some(listOf())).apply { table = Table(this@InsertAddSingle.table) }
        return ReturnSingleRecord(newQuery.lockQuery(sqlBuilder))
    }

    suspend fun <C : IExecutorConfig, RSLT>
            save(asyncExecutor: IAsyncExecutor<C, RSLT>) =
            asyncExecutor.save(
                    query.lockQuery(sqlBuilder) as InsertQuery<D, R, T>
            )

}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
        lazyInsert(
        table: T,
        sqlBuilder: ISQLBuilder<D, R, T> = CommonSQLBuilder<D, R, T>()
): InsertFRM<D, R, T> = InsertFRM<D, R, T>(table, sqlBuilder)