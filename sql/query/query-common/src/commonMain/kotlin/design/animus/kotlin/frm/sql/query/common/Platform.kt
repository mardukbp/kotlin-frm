package design.animus.kotlin.frm.sql.query.common

sealed class Platform
object JavaScript : Platform()
object JVM : Platform()
object Common : Platform()
object Native : Platform()
