package design.animus.kotlin.frm.sql.query.common.query.verbs

import design.animus.kotlin.frm.sql.query.common.query.ConditionalSteps
import design.animus.kotlin.frm.sql.query.common.query.Fields
import design.animus.kotlin.frm.sql.query.common.query.QueryTable
import design.animus.kotlin.frm.sql.query.common.query.SQLVerbs

interface ISelectQuery {
    val verb: SQLVerbs
    val columns: List<Fields<*, *, *, *>>
    val conditions: List<ConditionalSteps>
    val table: QueryTable
    val groupBy: List<Fields<*, *, *, *>>
}
