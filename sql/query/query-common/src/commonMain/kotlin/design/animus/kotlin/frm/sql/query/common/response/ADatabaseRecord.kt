package design.animus.kotlin.frm.sql.query.common.response

import kotlinx.serialization.KSerializer

typealias Column = String

sealed class ADatabaseRecord

abstract class ADatabaseRecordByMap(map: Map<String, Any?>) : ADatabaseRecord()

abstract class AJoinRecord(map: Map<String, Any?>) : ADatabaseRecord()

abstract class ADatabaseRecordByMapWithSerializers(map: Map<String, Any?>, serializers: Map<Column, KSerializer<*>>) :
    ADatabaseRecord()
