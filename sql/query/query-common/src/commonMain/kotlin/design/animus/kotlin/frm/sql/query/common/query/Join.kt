package design.animus.kotlin.frm.sql.query.common.query

sealed class JoinType(
    open val table: QueryTable,
    open val condition: List<ConditionalSteps>
)

data class Join(
    override val table: QueryTable,
    override val condition: List<ConditionalSteps>
) : JoinType(table, condition)

data class InnerJoin(
    override val table: QueryTable,
    override val condition: List<ConditionalSteps>
) : JoinType(table, condition)

data class RightJoin(
    override val table: QueryTable,
    override val condition: List<ConditionalSteps>
) : JoinType(table, condition)

data class LeftJoin(
    override val table: QueryTable,
    override val condition: List<ConditionalSteps>
) : JoinType(table, condition)
