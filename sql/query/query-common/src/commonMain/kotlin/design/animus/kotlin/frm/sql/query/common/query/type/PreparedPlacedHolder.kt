package design.animus.kotlin.frm.sql.query.common.query.type

sealed class PreparedPlacedHolder {
    abstract val preparedCharacter: PreparedVariable
}

data class SimpleCharacter(
    override val preparedCharacter: PreparedVariable
) : PreparedPlacedHolder()

data class NumberedCharacter(
    val startAt: Int,
    override val preparedCharacter: PreparedVariable,
    val escapeChar: String? = null
) : PreparedPlacedHolder()