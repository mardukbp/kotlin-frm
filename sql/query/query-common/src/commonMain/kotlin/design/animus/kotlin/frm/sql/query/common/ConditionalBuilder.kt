package design.animus.kotlin.frm.sql.query.common

import design.animus.functional.datatypes.either.Either
import design.animus.kotlin.frm.sql.executors.common.errors.IConditionalError
import design.animus.kotlin.frm.sql.executors.common.errors.SQMultipleColumnSpecified
import design.animus.kotlin.frm.sql.executors.common.errors.SQNoReturnSpecified
import design.animus.kotlin.frm.sql.executors.common.errors.SubQueryErrors
import design.animus.kotlin.frm.sql.executors.common.logger
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.type.*
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
        validateNestedQuery(query: LockedQuery<D, R, T>)
        : Either<SubQueryErrors, Fields<out Any?, D, out ADatabaseRecord, *>> {
    val possibleColumns = when (query) {
        is SelectQuery -> Either.right(query.columns)
        is UpdateQuery -> query.returningFields.fold(
                { Either.right(listOf<Fields<Any?, D, R, T>>()) },
                { rsp -> Either.right(rsp) }

        )
        is InsertQuery -> query.returningFields.fold(
                { Either.right(listOf<Fields<Any?, D, R, T>>()) },
                { rsp -> Either.right(rsp) }

        )
        is SelectJoinQuery -> Either.right(query.columns)
        is DeleteQuery -> Either.left("Delete does not return a field type")
    }
    return Either.comprehension {
        val (columns) = possibleColumns
        if (columns.size > 1) Either.left(SQMultipleColumnSpecified)
        if (columns.isEmpty()) Either.left(SQNoReturnSpecified)
        columns.first()
    }
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> validateNestedPreparedQuery(query: PreparedQuery<D, R, T, *>): Fields<out Any?, D, out ADatabaseRecord, *> {
    val columns = when (query) {
        is PreparedSelectQuery -> query.columns
        is PreparedUpdateQuery -> query.returning.fold(
                { listOf<Fields<Any?, D, R, T>>() },
                { rsp -> rsp }

        )
        is PreparedInsertQuery -> query.returning.fold(
                { listOf<Fields<Any?, D, R, T>>() },
                { rsp -> rsp }

        )
        is PreparedSelectJoinQuery -> query.columns
        is PreparedDeleteQuery -> throw Exception("Delete does not return a field type")
    }
    if (columns.size > 1) throw Exception("Can only support one return column in nested queries")
    if (columns.isEmpty()) throw Exception("Can not process nested query with no return.")
    return columns.first()
}

class SQLConditionalBuilder<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> {
    val conditions = mutableListOf<ConditionalSteps>()

    private fun <C : Any?> Fields<C, D, R, T>.conditionBuilder(
            value: C,
            operator: SQLConditionalOperator
    ): Condition<*, D, R, T> {
        val con = Condition<C, D, R, T>(
                this,
                operator,
                value
        ) as Condition<*, D, R, T>
        conditions.add(con)
        return con
    }

    private fun <C : Any?>
            Fields<C, D, *, *>.conditionBuilder(value: Fields<C, D, *, *>, verb: SQLConditionalOperator)
            : FieldComparison<C, D> {
        val con = FieldComparison(
                fieldOne = this,
                fieldTwo = value,
                operator = verb
        )
        conditions.add(con)
        return con
    }

    // Value Comparison
    suspend infix fun <C : Any?> Fields<C, D, R, T>.equal(value: C) =
            conditionBuilder(value, SQLConditionalOperator.Equal)

    suspend infix fun <C : Any?> Fields<C, D, R, T>.gt(value: C) =
            conditionBuilder(value, SQLConditionalOperator.GreaterThan)

    suspend infix fun <C : Any?> Fields<C, D, R, T>.gte(value: C) =
            conditionBuilder(value, SQLConditionalOperator.GreaterThanOrEqual)

    suspend infix fun <C : Any?> Fields<C, D, R, T>.lte(value: C) =
            conditionBuilder(value, SQLConditionalOperator.LessThanOrEqual)

    suspend infix fun <C : Any?> Fields<C, D, R, T>.like(value: C) =
            conditionBuilder(value, SQLConditionalOperator.Like)

    suspend infix fun <C : Any?> Fields<C, D, R, T>.iLike(value: C) =
            conditionBuilder(value, SQLConditionalOperator.CaseInsensitiveLike)

    suspend infix fun <C : Any?> Fields<C, D, R, T>.notEqual(value: C) =
            conditionBuilder(value, SQLConditionalOperator.NotEqual)
    // End Value Comparison

    // Field Comparison
    suspend infix fun <C : Any?>
            Fields<C, D, *, *>.equal(value: Fields<C, D, *, *>) = conditionBuilder(value, SQLConditionalOperator.Equal)

    suspend infix fun <C : Any?>
            Field<C, D, R, T>.gt(value: Fields<C, D, *, *>) =
            conditionBuilder(value, SQLConditionalOperator.GreaterThan)

    suspend infix fun <C : Any?>
            Field<C, D, R, T>.gte(value: Fields<C, D, *, *>) =
            conditionBuilder(value, SQLConditionalOperator.GreaterThanOrEqual)

    suspend infix fun <C : Any?>
            Field<C, D, R, T>.lte(value: Fields<C, D, *, *>) =
            conditionBuilder(value, SQLConditionalOperator.LessThanOrEqual)

    suspend infix fun <C : Any?>
            Fields<C, D, R, T>.like(value: Fields<C, D, *, *>) = conditionBuilder(value, SQLConditionalOperator.Like)

    suspend infix fun <C : Any?>
            Fields<C, D, *, *>.iLike(value: Fields<C, D, *, *>) =
            conditionBuilder(value, SQLConditionalOperator.CaseInsensitiveLike)

    suspend infix fun <C : Any?>
            Field<C, D, R, T>.notEqual(value: Fields<C, D, *, *>) =
            conditionBuilder(value, SQLConditionalOperator.NotEqual)
    // End Field Comparison

    // Nested Comparisons
    suspend infix fun <C : Any?, NR : ADatabaseRecord, NT : ITable<D, NR>>
            Fields<C, D, *, *>.contains(query: LockedQuery<D, NR, NT>): FieldComparison<C, D> {
        val possibleColumn = validateNestedQuery(query)
        val possibleRsp = Either.comprehensionAsync<SubQueryErrors, FieldComparison<C, D>> {
            val (col) = possibleColumn
            if (col.columnType != this@contains.columnType) errorOf("Differing column type cannot make comparison")
            conditionBuilder(
                    NestedQuery(query, query.buildSQLString(), col.columnName, col.columnType, col.schema, col.table),
                    SQLConditionalOperator.In
            )
        }
        println(possibleRsp)
        return possibleRsp.fold(
                { err ->
                    logger.error { err.message }
                    throw Exception(err.message)
                },
                {
                    it
                }
        )
    }
    // End Nested Comparisons

    private fun <C : Any?> handleConditionOperator(
            value: Condition<C, D, R, T>,
            operator: ConditionalSteps
    ): Condition<*, D, R, T> {
        val index = conditions.size - 1
        val lastCondition = conditions[index]
        conditions.removeAt(index)
        conditions.add(operator)
        conditions.add(lastCondition)
        return value
    }

    private fun <C : Any?> handleConditionOperator(
            value: FieldComparison<C, D>,
            operator: ConditionalSteps
    ): FieldComparison<C, D> {
        val index = conditions.size - 1
        val lastCondition = conditions[index]
        conditions.removeAt(index)
        conditions.add(operator)
        conditions.add(lastCondition)
        return value
    }

    suspend infix fun <C : Any?> Condition<*, D, R, T>.and(value: Condition<C, D, R, T>) =
            handleConditionOperator(value, AND)

    suspend infix fun FieldComparison<*, D>.and(value: FieldComparison<*, D>) =
            handleConditionOperator(value, AND)

    suspend infix fun <C : Any?> FieldComparison<C, D>.and(value: Condition<C, D, R, T>) =
            handleConditionOperator(value, AND)

    suspend infix fun <C : Any?> Condition<*, D, R, T>.or(value: Condition<C, D, R, T>) =
            handleConditionOperator(value, OR)

    suspend infix fun <C : Any?> Condition<*, D, R, T>.not(value: Condition<C, D, R, T>) =
            handleConditionOperator(value, NOT)

}
