package design.animus.kotlin.frm.sql.executors.common.errors

sealed class SubQueryErrors(val message:String)

object SQMultipleColumnSpecified : SubQueryErrors("Can only support one return column in nested queries")
object SQNoReturnSpecified : SubQueryErrors("Can not process nested query with no return.")