package design.animus.kotlin.frm.sql.query.common.frm.prepare

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.frm.sql.executors.common.AsyncDBResponse
import design.animus.kotlin.frm.sql.executors.common.IAsyncExecutor
import design.animus.kotlin.frm.sql.executors.common.IExecutorConfig
import design.animus.kotlin.frm.sql.executors.common.Save
import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.frm.IFRM
import design.animus.kotlin.frm.sql.query.common.query.Fields
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.Table
import design.animus.kotlin.frm.sql.query.common.query.type.PreparedInsertQuery
import design.animus.kotlin.frm.sql.query.common.query.verbs.InsertBuilder
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlinx.coroutines.Dispatchers
import mu.KotlinLogging
import kotlin.coroutines.CoroutineContext

class PreparedInsertFRM<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
    val table: T,
    val sqlBuilder: ISQLBuilder<D, R, T>
) : IFRM<PreparedInsertQuery<D, R, T>> {
    override val logger = KotlinLogging.logger {}
    override var query: PreparedInsertQuery<D, R, T> =
        PreparedInsertQuery(listOf(), sqlBuilder, Table(table))

    suspend fun add(newItems: suspend InsertBuilder<D, R, T>.() -> Unit): PreparedInsertFRM<D, R, T> {
        val state = InsertBuilder<D, R, T>()
        state.newItems()
        val originalNewItems = query.newItems.toMutableList()
        originalNewItems.add(state.preparedNewItems.toList())
        query = query.copy(newItems = originalNewItems.toList())
        return this
    }

    suspend fun <C : Any?> returning(vararg fields: Fields<Any?, D, R, T>): PreparedInsertFRM<D, R, T> {
        query = query.copy(returning = Option.Some(fields.toList()) as Option<List<Fields<Any?, D, R, T>>>)
        return this
    }

    suspend fun <C : IExecutorConfig, RSLT> save(
        executor: IAsyncExecutor<C, RSLT>,
        vararg arguments: Any?,
        ctx: CoroutineContext = Dispatchers.Default
    )
            : AsyncDBResponse<D, R, T, C, RSLT, Boolean> {
        val values = query.bind(*arguments)
        return executor.preparedFetchAsync<D, R, T, Boolean, Any, Boolean>(query, Save, values, ctx) { true }
    }

    suspend fun <C:IExecutorConfig, RSLT> saveAndReturn(
        executor:IAsyncExecutor<C,RSLT>,
        vararg arguments:Any?,
        ctx:CoroutineContext = Dispatchers.Default
    ): AsyncDBResponse<D, R, T, C, RSLT, Boolean> {
        val values = query.bind(*arguments)
        return executor.preparedFetchAsync<D, R, T, Boolean, Any, Boolean>(query, Save, values, ctx) { true }
    }


}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
        lazyPreparedInsert(
    table: T,
    sqlBuilder: ISQLBuilder<D, R, T> = CommonSQLBuilder<D, R, T>()
): PreparedInsertFRM<D, R, T> =
    PreparedInsertFRM<D, R, T>(table, sqlBuilder)