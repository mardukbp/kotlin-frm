package design.animus.kotlin.frm.sql.query.common.query

enum class SQLConditionalOperator(val operator: String) {
    Equal("="),
    NotEqual("!="),
    GreaterThan(">"),
    GreaterThanOrEqual(">="),
    LessThan("<"),
    In("in"),
    LessThanOrEqual("<="),
    Like("like"),
    CaseInsensitiveLike("ilike")
}
