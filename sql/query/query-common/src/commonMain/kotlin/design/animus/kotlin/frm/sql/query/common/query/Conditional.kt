package design.animus.kotlin.frm.sql.query.common.query

import design.animus.kotlin.frm.sql.query.common.builder.magicPreparedKey
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

typealias WhereString = String

interface CommonCondition<C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> {
    val column: Fields<C, D, R, T>
    val operator: SQLConditionalOperator
    val expectedValue: C
}

interface CommonConditionChain

sealed class ConditionalSteps
data class Condition<C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val column: Fields<C, D, R, T>,
        override val operator: SQLConditionalOperator,
        override val expectedValue: C
) : CommonCondition<C, D, R, T>, ConditionalSteps() {
    override fun toString() =
            "Condition:[${this.column.columnName} ${this.operator.operator} '${this.expectedValue}']"

}

data class FieldComparison<C : Any?, D : IDatabase>(
        val fieldOne: Fields<C, D, *, *>,
        val fieldTwo: Fields<C, D, *, *>,
        val operator: SQLConditionalOperator
) : ConditionalSteps()

object AND : CommonConditionChain, ConditionalSteps() {
    override fun toString() = "AND"
}

object OR : CommonConditionChain, ConditionalSteps() {
    override fun toString() = "OR"
}

object NOT : CommonConditionChain, ConditionalSteps() {
    override fun toString() = "NOT"
}

fun buildConditionString(conditions: List<ConditionalSteps>, table: FriendlyTableName, tableTwo: String? = null) =
        conditions.joinToString(" ") { condition ->
            when (condition) {
                is Condition<*, *, *, *> -> "$table.${condition.column.columnName} ${condition.operator.operator} '${condition.expectedValue}'"
                AND -> "AND"
                OR -> "OR"
                NOT -> "NOT"
                is FieldComparison<*, *> -> {
                    when (condition.fieldTwo) {
                        is LateField<*, *, *, *> -> {
                            "${condition.fieldOne.table}.${condition.fieldOne.columnName} ${condition.operator.operator} $magicPreparedKey"
                        }
                        is NestedQuery<*, *, *, *> -> "$table.${condition.fieldOne.columnName} ${condition.operator.operator} (${condition.fieldTwo.sql})"
                        else -> {
                            "${condition.fieldOne.table}.${condition.fieldOne.columnName} ${condition.operator.operator} ${condition.fieldTwo.table}.${condition.fieldTwo.columnName}"
                        }
                    }

                }
            }
        }


/**
 * @author Animus Null
 * @since 1.0
 * @throws IllegalArgumentException On an unsupported query, i.e. one that does not use conditions.
 * @return A string representing the where condition
 */
suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> commonBuildWhereString(
        conditions: List<ConditionalSteps>,
        table: FriendlyTableName
): WhereString = when {
    conditions.isEmpty() -> ""
    conditions.size == 1 -> {
        conditions.forEach { println(it) }
        when (val condition = conditions.first()) {
            is Condition<*, *, *, *> -> {
                val value = condition.expectedValue
                val v = if (value is Int || value is Double || value is Float || value is Long)
                    "$value" else "'$value'"
                "where $table.${condition.column.columnName} ${condition.operator.operator} $v"
            }
            is FieldComparison<*, *> -> {
                when (condition.fieldTwo) {
                    is LateField -> "where $table.${condition.fieldOne.columnName} ${condition.operator.operator} $magicPreparedKey"
                    is NestedQuery<*, *, *, *> -> "where $table.${condition.fieldOne.columnName} ${condition.operator.operator} (${condition.fieldTwo.sql})"
                    else -> "where $table.${condition.fieldOne.columnName} ${condition.operator.operator} ${condition.fieldTwo.table}.${condition.fieldTwo.columnName}"
                }
            }
            else -> throw IllegalArgumentException("If one condition it must be a Condition and not Operator")
        }

    }
    else -> {
        "where ${buildConditionString(conditions, table)}"
    }
}