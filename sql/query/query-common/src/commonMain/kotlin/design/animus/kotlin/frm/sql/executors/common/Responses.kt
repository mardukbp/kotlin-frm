package design.animus.kotlin.frm.sql.executors.common

import design.animus.kotlin.frm.sql.executors.common.exceptions.UnsupportedQueryOperation
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.type.IBaseQuery
import design.animus.kotlin.frm.sql.query.common.query.verbs.IDeleteQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import mu.KotlinLogging
import kotlin.reflect.KClass

internal val logger = KotlinLogging.logger {}

sealed class AsyncDBResponse<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, C : IExecutorConfig, RSLT, RSP> :
        IAsyncDBResponse<D, R, T, C, RSLT, RSP>

data class GenericWrapper<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, C : IExecutorConfig, RSLT, IN, TargetClass : Any, RSP>(
        override val futureResponse: RSLT,
        override val asyncExecutor: IAsyncExecutor<C, RSLT>,
        val responseType: ExpectedResponses,
        override val query: IBaseQuery<D, R, T>,
        val callback: suspend (IN) -> RSP
) : AsyncDBResponse<D, R, T, C, RSLT, RSP>() {
    @Suppress("UNCHECKED_CAST")
    override suspend fun fetch(): RSP {
        val rsp = when {
            responseType is SingleOfRecord || responseType is DirectOfRecord || responseType is SaveAndReturnRecord -> {
                asyncExecutor.unpackDBFuture<D, R, T, IN>(futureResponse, query)
            }
            responseType is ListOfRecord || responseType is DirectListOfRecord || responseType is SaveAndReturnListOfRecord -> {
                asyncExecutor.unpackDBFutures<D, R, T, IN>(futureResponse, query)
            }
            responseType is HashMap || responseType is SaveAndReturn -> {
                asyncExecutor.unpackDBFutureAsMap<D, R, T, IN>(futureResponse, query)
            }
            responseType is ListOfHashMap || responseType is SaveAndReturnMultiple -> {
                asyncExecutor.unpackDBFutureAsMultiMap<D, R, T, IN>(futureResponse, query)
            }
            responseType is Delete && query is IDeleteQuery<D, R, T> -> {
                asyncExecutor.unpackDBDelete<D, R, T, Any, IN, RSP>(futureResponse, query)
            }
            responseType is Save -> {
                asyncExecutor.unpackDBSave<D, R, T, Any, IN, RSP>(futureResponse)
            }
            responseType is ICustomRecord<*> -> {
                asyncExecutor.unpackDBFutureAsCustomClass<D, R, T, TargetClass, IN, RSP>(
                        futureResponse,
                        responseType.targetClass as KClass<TargetClass>, query
                )
            }
            responseType is IListOfCustomRecord<*> -> {
                asyncExecutor.unpackDBFutureAsListOfCustomClass<D, R, T, TargetClass, IN, RSP>(
                        futureResponse,
                        responseType.targetClass as KClass<TargetClass>, query
                )
            }
            else -> {
                throw UnsupportedQueryOperation("Got a response type of : $responseType which is not supported in generic.")
            }
        }
        return callback(rsp)
    }
}

interface IAsyncDBResponse<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, C : IExecutorConfig, RSLT, RSP> {
    val futureResponse: RSLT
    val asyncExecutor: IAsyncExecutor<C, RSLT>
    val query: IBaseQuery<D, R, T>
    suspend fun fetch(): RSP
}
