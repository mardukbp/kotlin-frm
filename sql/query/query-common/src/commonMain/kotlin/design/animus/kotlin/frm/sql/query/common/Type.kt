package design.animus.kotlin.frm.sql.query.common

data class ClassName(
    val packageName: String,
    val className: String
)


typealias SQLType = String

abstract class ISQLType {
    abstract val sqlType: SQLType
    abstract val commonType: ClassName
    abstract val jvmType: ClassName
    abstract val nativeType: ClassName
    abstract val jsType: ClassName
}

typealias CustomTypes = List<CustomType>

data class CustomType(
    val sqlType: SQLType,
    val packageName: String,
    val className: String
)