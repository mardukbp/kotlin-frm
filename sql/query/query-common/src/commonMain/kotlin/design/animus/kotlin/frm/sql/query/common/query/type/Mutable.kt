@file:Suppress("UNCHECKED_CAST")

package design.animus.kotlin.frm.sql.query.common.query.type

import design.animus.functional.datatypes.either.Either
import design.animus.functional.datatypes.either.foldValid
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.frm.sql.executors.common.errors.IQueryError
import design.animus.kotlin.frm.sql.executors.common.exceptions.MisMatchColumnSetForInsert
import design.animus.kotlin.frm.sql.query.common.SQLConditionalBuilder
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.UpdateSet
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.verbs.IMutableInsertQuery
import design.animus.kotlin.frm.sql.query.common.query.verbs.IMutableUpdateQuery
import design.animus.kotlin.frm.sql.query.common.query.verbs.ISelectQuery
import design.animus.kotlin.frm.sql.query.common.query.verbs.UpdateChange
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

sealed class MutableQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        open val verb: SQLVerbs,
        open val conditions: List<ConditionalSteps>
) {
    lateinit var table: QueryTable

    @Suppress("UNCHECKED_CAST")
    suspend fun lockQuery(sqlBuilder: ISQLBuilder<D, R, T>): LockedQuery<D, R, T> = when (this) {
        is SelectMutableQuery -> SelectQuery(
                sqlBuilder,
                this.verb,
                this.columns,
                this.conditions,
                this.table,
                this.groupBy
        )
        is UpdateMutableQuery -> UpdateQuery(
                sqlBuilder, this.verb, this.conditions, this.table, this.changes.toList(), this.returning
        )
        is InsertMutableQuery -> {
            this.columnValues.forEach { valueSet ->
                if (valueSet.size != this.columns.size) throw MisMatchColumnSetForInsert(
                        "Value set $valueSet had ${valueSet.size} new values but was expecting ${this.columns.size}"
                )
            }
            val newItems = this.columnValues.map { valueSet ->
                valueSet.mapIndexed { index, value ->
                    Pair(this.columns[index], value)
                }
            }
            InsertQuery(
                    sqlBuilder, this.verb, this.table, newItems, this.returningFields
            )
        }
        is SelectJoinMutableQuery -> SelectJoinQuery<D, R, T>(
                sqlBuilder, this.verb, this.columns as List<Fields<*, D, *, *>>,
                this.conditions, this.table, this.joinStatements, this.groupBy
        )
        is DeleteMutableQuery -> DeleteQuery<D, R, T>(sqlBuilder, this.table, this.conditions)
    }

    suspend fun buildSQLString(sqlBuilder: ISQLBuilder<D, R, T>) =
            when (val lockedQuery = this.lockQuery(sqlBuilder)) {
                is SelectQuery -> sqlBuilder.buildSelectQuery(lockedQuery).trim()
                is UpdateQuery -> sqlBuilder.buildUpdateQuery(lockedQuery).trim()
                is InsertQuery -> sqlBuilder.buildInsertQuery(lockedQuery).trim()
                is SelectJoinQuery -> sqlBuilder.buildSelectJoinQuery(lockedQuery).trim()
                is DeleteQuery -> sqlBuilder.buildDeleteQuery(lockedQuery).trim()
            }
}


data class SelectMutableQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val columns: List<Fields<*, D, R, T>>,
        override val conditions: List<ConditionalSteps>,
        override val groupBy: List<Fields<*, *, *, *>> = listOf(),
        override val verb: SQLVerbs = SQLVerbs.SELECT
) : ISelectQuery, MutableQuery<D, R, T>(verb, conditions) {
    suspend infix fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> join(inTable: T) =
            SelectJoinMutableQuery<D, R, T>(
                    this.columns, this.conditions
            ).apply {
                table = this@SelectMutableQuery.table
            }.run {
                joinStatements.add(Join(Table(inTable), listOf()))
                this
            }


    suspend infix fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> join(inTable: NamedTable<D, R, T>) =
            SelectJoinMutableQuery<D, R, T>(
                    this.columns, this.conditions
            ).apply {
                table = this@SelectMutableQuery.table
            }.run {
                joinStatements.add(Join(inTable, listOf()))
                this
            }


    infix fun from(inTable: T): SelectMutableQuery<D, R, T> {
        this.table = Table<D, R, T>(inTable)
        return this
    }

    infix fun from(inTable: NamedTable<D, R, T>): SelectMutableQuery<D, R, T> {
        this.table = inTable
        return this
    }

    suspend infix fun where(block: suspend SQLConditionalBuilder<D, R, T>.() -> Unit): SelectMutableQuery<D, R, T> {
        val inTable = this.table
        val state = SQLConditionalBuilder<D, R, T>()
        state.block()
        return this@SelectMutableQuery.copy(conditions = state.conditions).apply { table = inTable }

    }

    suspend infix fun <D : IDatabase>
            groupBy(fields: List<Fields<*, D, *, *>>) = this
            .copy(groupBy = fields)
            .apply { table = this@SelectMutableQuery.table }
}

data class UpdateMutableQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val conditions: List<ConditionalSteps>,
        override val changes: MutableList<UpdateChange<*, D, R, T>> = mutableListOf(),
        override val verb: SQLVerbs = SQLVerbs.UPDATE,
        override var returning: Option<List<Fields<Any?, D, R, T>>> = Option.None()
) : IMutableUpdateQuery<D, R, T>, MutableQuery<D, R, T>(verb, conditions) {
    suspend infix fun set(block: UpdateSet<D, R, T>.() -> Unit): UpdateMutableQuery<D, R, T> {
        val state = UpdateSet<D, R, T>()
        state.block()
        this.changes.addAll(state.changes)
        return this
    }

    suspend infix fun where(block: suspend SQLConditionalBuilder<D, R, T>.() -> Unit): UpdateMutableQuery<D, R, T> {
        val inTable = this.table
        val state = SQLConditionalBuilder<D, R, T>()
        state.block()
        return this.copy(conditions = state.conditions).apply { table = inTable }
    }

    suspend infix fun returning(inFields: List<Fields<Any?, D, R, T>>): UpdateMutableQuery<D, R, T> {
        return this.copy(returning = Option.Some(inFields)).apply { table = this@UpdateMutableQuery.table }
    }
}

data class InsertMutableQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val verb: SQLVerbs = SQLVerbs.INSERT,
        var columns: List<Fields<*, D, R, T>> = listOf(),
        var columnValues: List<List<Any?>> = listOf(),
        var returningFields: Option<List<Fields<Any?, D, R, T>>> = Option.None()
) : IMutableInsertQuery, MutableQuery<D, R, T>(verb, listOf()) {

    suspend infix fun columns(inColumns: List<Field<*, D, R, T>>) = this.apply {
        columns = inColumns
    }

    suspend infix fun values(changes: List<List<Any>>) = this.apply {
        columnValues = changes
    }

    suspend infix fun <C : Any?> returning(fields: List<Fields<C, D, R, T>>) = this.apply {
        returningFields = Option.Some(fields) as Option<List<Fields<Any?, D, R, T>>>
    }

    suspend fun returning() = this.apply {
        returningFields = Option.Some(listOf())
    }
}

data class SelectJoinMutableQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val columns: List<Fields<*, *, *, *>>,
        override val conditions: List<ConditionalSteps>,
        val joinStatements: MutableList<JoinType> = mutableListOf<JoinType>(),
        override val groupBy: List<Fields<*, *, *, *>> = listOf(),
        override val verb: SQLVerbs = SQLVerbs.SELECTJOIN
) : ISelectQuery, MutableQuery<D, R, T>(verb, conditions) {
    suspend infix fun on(changes: suspend SQLConditionalBuilder<D, R, T>.() -> Unit): SelectJoinMutableQuery<D, R, T> {
        val state = SQLConditionalBuilder<D, R, T>()
        state.changes()
        val statement = when (val last = joinStatements.last()) {
            is Join -> Join(last.table, state.conditions)
            is InnerJoin -> InnerJoin(last.table, state.conditions)
            is RightJoin -> RightJoin(last.table, state.conditions)
            is LeftJoin -> LeftJoin(last.table, state.conditions)
        }
        joinStatements.removeAt(joinStatements.size - 1)
        joinStatements.add(statement)
        return this
    }

    suspend infix fun where(block: suspend SQLConditionalBuilder<D, R, T>.() -> Unit): SelectJoinMutableQuery<D, R, T> {
        val inTable = this.table
        val state = SQLConditionalBuilder<D, R, T>()
        state.block()
        return this.copy(conditions = state.conditions).apply {
            table = inTable
        }
    }

    // Join Helper Functions
    private suspend fun addJoinElement(type: JoinType): SelectJoinMutableQuery<D, R, T> {
        joinStatements.add(type)
        return this
    }
    // End Join Helper Functions

    // Join
    suspend infix fun <ND : IDatabase, NR : ADatabaseRecord, NT : ITable<ND, NR>> join(inTable: NamedTable<ND, NR, NT>) =
            addJoinElement(Join(inTable, listOf()))

    suspend infix fun <ND : IDatabase, NR : ADatabaseRecord, NT : ITable<ND, NR>> join(inTable: NT) =
            addJoinElement(Join(Table(inTable), listOf()))
    // End Join

    // Left Join
    suspend infix fun <ND : IDatabase, NR : ADatabaseRecord, NT : ITable<ND, NR>> leftJoin(inTable: NamedTable<ND, NR, NT>) =
            addJoinElement(LeftJoin(inTable, listOf()))

    suspend infix fun <ND : IDatabase, NR : ADatabaseRecord, NT : ITable<ND, NR>> leftJoin(inTable: NT) =
            addJoinElement(LeftJoin(Table(inTable), listOf()))
    // End Left Join

    // right Join
    suspend infix fun <ND : IDatabase, NR : ADatabaseRecord, NT : ITable<ND, NR>> rightJoin(inTable: NamedTable<ND, NR, NT>) =
            addJoinElement(RightJoin(inTable, listOf()))

    suspend infix fun <ND : IDatabase, NR : ADatabaseRecord, NT : ITable<ND, NR>> rightJoin(inTable: NT) =
            addJoinElement(RightJoin(Table(inTable), listOf()))
    // End right Join

    // inner Join
    suspend infix fun <ND : IDatabase, NR : ADatabaseRecord, NT : ITable<ND, NR>> innerJoin(inTable: NamedTable<ND, NR, NT>) =
            addJoinElement(InnerJoin(inTable, listOf()))

    suspend infix fun <ND : IDatabase, NR : ADatabaseRecord, NT : ITable<ND, NR>> innerJoin(inTable: NT) =
            addJoinElement(InnerJoin(Table(inTable), listOf()))
    // End inner Join

    suspend infix fun <ND : IDatabase, NR : ADatabaseRecord, NT : ITable<ND, NR>> from(inTable: NT): SelectJoinMutableQuery<D, R, T> {
        this.table = Table(inTable)
        return this
    }

    suspend infix fun <ND : IDatabase, NR : ADatabaseRecord, NT : ITable<ND, NR>> from(inTable: NamedTable<ND, NR, NT>): SelectJoinMutableQuery<D, R, T> {
        this.table = inTable
        return this
    }

    suspend infix fun <D : IDatabase>
            groupBy(fields: List<Fields<*, D, *, *>>) = this
            .copy(groupBy = fields)
            .apply { table = this@SelectJoinMutableQuery.table }

}

data class DeleteMutableQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val conditions: List<ConditionalSteps>,
        override val verb: SQLVerbs = SQLVerbs.DELETE
) : MutableQuery<D, R, T>(verb, conditions) {
    suspend infix fun where(block: suspend SQLConditionalBuilder<D, R, T>.() -> Unit): DeleteMutableQuery<D, R, T> {
        val inTable = this.table
        val state = SQLConditionalBuilder<D, R, T>()
        state.block()
        return this.copy(conditions = state.conditions).apply { table = inTable }
    }
}