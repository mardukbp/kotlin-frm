package design.animus.kotlin.frm.sql.query.common.query

interface IDatabase {
    val database: String
}
