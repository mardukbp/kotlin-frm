@file:Suppress("UNCHECKED_CAST")

package design.animus.kotlin.frm.sql.query.common.frm

import design.animus.kotlin.frm.sql.executors.common.*
import design.animus.kotlin.frm.sql.query.common.SQLConditionalBuilder
import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.type.SelectJoinMutableQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlinx.coroutines.Dispatchers
import mu.KotlinLogging
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass


class SelectJoinFRM<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
    val table: T,
    val sqlBuilder: ISQLBuilder<D, R, T>
) : IFRM<SelectJoinMutableQuery<D, R, T>> {
    override val logger = KotlinLogging.logger {}
    override var query: SelectJoinMutableQuery<D, R, T> =
        SelectJoinMutableQuery<D, R, T>(listOf(), listOf()).apply {
            table = Table(this@SelectJoinFRM.table)
        }

    @Suppress("UNCHECKED_CAST")
    suspend fun withColumns(vararg inColumns: Fields<*, D, *, *>): SelectJoinFRM<D, R, T> {
        query = query.copy(columns = inColumns.toList() as List<Fields<*, D, R, T>>)
            .apply { table = Table(this@SelectJoinFRM.table) }
        return this
    }

    @Suppress("UNCHECKED_CAST")
    suspend fun groupBy(vararg inColumns: Fields<*, D, *, *>): SelectJoinFRM<D, R, T> {
        query = query.copy(groupBy = inColumns.toList() as List<Fields<*, D, R, T>>)
            .apply { table = Table(this@SelectJoinFRM.table) }
        return this
    }

    suspend fun filter(block: suspend SQLConditionalBuilder<D,R,T>.() -> Unit): SelectJoinFRM<D, R, T> {
        val state = SQLConditionalBuilder<D,R,T>()
        state.block()
        query = query.copy(conditions = state.conditions).apply { table = Table(this@SelectJoinFRM.table) }
        return this
    }

    suspend fun <JR : ADatabaseRecord, JT : ITable<D, JR>> join(
        table: JT, conditionBuilder: suspend SQLConditionalBuilder<D,R,T>.() -> Unit
    ): SelectJoinFRM<D, R, T> {
        val state = SQLConditionalBuilder<D,R,T>()
        state.conditionBuilder()
        query.joinStatements.add(Join(Table(table), state.conditions))
        return this
    }

    suspend fun <C : IExecutorConfig, RSLT, RSP> mapOne(
        executor: IAsyncExecutor<C, RSLT>,
        ctx: CoroutineContext = Dispatchers.Default,
        block: suspend (Map<String, Any?>) -> RSP
    ): AsyncDBResponse<D, R, T, C, RSLT, RSP> =
        executor.fetchAsync<D, R, T, Map<String, Any?>, Any, RSP>(query.lockQuery(sqlBuilder), HashMap, ctx, block)


    suspend fun <C : IExecutorConfig, RSLT, RSP> map(
        executor: IAsyncExecutor<C, RSLT>,
        ctx: CoroutineContext = Dispatchers.Default,
        block: suspend (List<Map<String, Any?>>) -> RSP
    ): AsyncDBResponse<D, R, T, C, RSLT, RSP> =
        executor.fetchAsync<D, R, T, List<Map<String, Any?>>, Any, RSP>(
            query.lockQuery(sqlBuilder),
            ListOfHashMap,
            ctx,
            block
        )

    suspend fun <C : IExecutorConfig, RSLT, TargetClass : Any, RSP> mapOneInto(
        executor: IAsyncExecutor<C, RSLT>,
        targetClass: KClass<TargetClass>,
        ctx: CoroutineContext = Dispatchers.Default,
        block: suspend (TargetClass) -> RSP
    ) {
        executor.fetchAsync<D, R, T, TargetClass, TargetClass, RSP>(
            query.lockQuery(sqlBuilder),
            ListOfCustomRecord(targetClass),
            ctx,
            block
        )
    }

    suspend fun <C : IExecutorConfig, RSLT, TargetClass : Any, RSP> mapInto(
        executor: IAsyncExecutor<C, RSLT>,
        targetClass: KClass<TargetClass>,
        ctx: CoroutineContext = Dispatchers.Default,
        block: suspend (List<TargetClass>) -> RSP
    ) =
        executor.fetchAsync<D, R, T, List<TargetClass>, TargetClass, RSP>(
            query.lockQuery(sqlBuilder),
            ListOfCustomRecord(targetClass),
            ctx,
            block
        )

}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
        lazySelectJoin(
    table: T,
    sqlBuilder: ISQLBuilder<D, R, T> = CommonSQLBuilder<D, R, T>()
): SelectJoinFRM<D, R, T> = SelectJoinFRM<D, R, T>(table, sqlBuilder)