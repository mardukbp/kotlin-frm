package design.animus.kotlin.frm.sql.query.common.frm.prepare

import design.animus.kotlin.frm.sql.executors.common.*
import design.animus.kotlin.frm.sql.query.common.frm.IFRM
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.type.IPreparedQuery
import design.animus.kotlin.frm.sql.query.common.query.type.PreparedInsertQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlinx.coroutines.Dispatchers
import mu.KotlinLogging
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass

class PreparedInsertReturning<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override var query: PreparedInsertQuery<D, R, T>
) : IFRM<PreparedInsertQuery<D, R, T>> {
    override val logger = KotlinLogging.logger {}

    suspend fun <C : IExecutorConfig, RSLT, RSP>
            save(asyncExecutor: IAsyncExecutor<C, RSLT>,
                 vararg values: Any?,
                 ctx: CoroutineContext = Dispatchers.Default,
                 block: suspend (R) -> RSP) {
        val arguments = query.bind(*values)
        asyncExecutor.preparedFetchAsync<D, R, T, R, Any, RSP>(query as IPreparedQuery<D, R, T>, SingleOfRecord, arguments, ctx, block)
    }

}


class PreparedInsertReturningCustom<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override var query: PreparedInsertQuery<D, R, T>
) : IFRM<PreparedInsertQuery<D, R, T>> {
    override val logger = KotlinLogging.logger {}

    suspend fun <C : IExecutorConfig, RSLT, RSP>
            save(asyncExecutor: IAsyncExecutor<C, RSLT>,
                 vararg values: Any?,
                 ctx: CoroutineContext = Dispatchers.Default,
                 block: suspend (Map<String, Any?>) -> RSP): AsyncDBResponse<D, R, T, C, RSLT, RSP> {
        val arguments = query.bind(*values)
        return asyncExecutor.preparedFetchAsync<D, R, T, Map<String, Any?>, Any, RSP>(query as IPreparedQuery<D, R, T>, HashMap, arguments, ctx, block)
    }


    suspend fun <C : IExecutorConfig, RSLT, TargetClass : Any, RSP>
            save(asyncExecutor: IAsyncExecutor<C, RSLT>,
                 vararg values: Any?,
                 ctx: CoroutineContext = Dispatchers.Default,
                 into: KClass<T>,
                 block: suspend (TargetClass) -> RSP): AsyncDBResponse<D, R, T, C, RSLT, RSP> {
        val arguments = query.bind(*values)
        return asyncExecutor.preparedFetchAsync<D, R, T, TargetClass, TargetClass, RSP>(query as IPreparedQuery<D, R, T>, CustomRecord(into), arguments, ctx, block)
    }
}