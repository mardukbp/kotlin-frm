package design.animus.kotlin.frm.sql.query.common.query.verbs

import design.animus.kotlin.frm.sql.query.common.query.ConditionalSteps
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.type.IQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

interface IDeleteQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : IQuery<D, R, T> {
    val conditions: List<ConditionalSteps>
}