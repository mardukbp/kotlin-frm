package design.animus.kotlin.frm.sql.query.common.query.verbs

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.type.ISaveQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

typealias UpdateSetString = String

data class UpdateChange<C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val field: Field<C, D, R, T>,
        val value: C?,
        val lateField: LateField<C, D, R, T>? = null
) {
    override fun toString(): String =
            "Set[${this.field.columnName} = ${this.value}]"
}

interface IMutableUpdateQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> {
    val conditions: List<ConditionalSteps>
    val changes: MutableList<UpdateChange<*, D, R, T>>
    val table: QueryTable
    var returning: Option<List<Fields<Any?, D, R, T>>>
}

interface IUpdateQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : ISaveQuery<D, R, T> {
    val conditions: List<ConditionalSteps>
    val changes: List<UpdateChange<*, D, R, T>>
    val returningFields: Option<List<Fields<Any?, D, R, T>>>
}
