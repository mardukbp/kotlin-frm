package design.animus.kotlin.frm.sql.query.common

import design.animus.kotlin.frm.sql.executors.common.*
import design.animus.kotlin.frm.sql.query.common.frm.IFRM
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.type.IQuery
import design.animus.kotlin.frm.sql.query.common.query.type.LockedQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlinx.coroutines.Dispatchers
import mu.KLogger
import mu.KotlinLogging
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass

interface IReturning<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, Q : LockedQuery<D, R, T>> : IFRM<Q> {
    override val logger : KLogger
        fun get() {
            KotlinLogging.logger {}
        }
    override var query: Q
    suspend fun <C : IExecutorConfig, RSLT, RSP>
            save(asyncExecutor: IAsyncExecutor<C, RSLT>, ctx: CoroutineContext = Dispatchers.Default,
                 block: suspend (R) -> RSP) =
            asyncExecutor.fetchAsync<D, R, T, R, Any, RSP>(query as IQuery<D, R, T>, SingleOfRecord, ctx, block)
}


interface IReturningCustom<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, Q : LockedQuery<D, R, T>> : IFRM<Q> {
    override val logger : KLogger
    fun get() {
        KotlinLogging.logger {}
    }
    override var query: Q

    suspend fun <C : IExecutorConfig, RSLT, RSP>
            save(asyncExecutor: IAsyncExecutor<C, RSLT>, ctx: CoroutineContext = Dispatchers.Default,
                 block: suspend (Map<String, Any?>) -> RSP) =
            asyncExecutor.fetchAsync<D, R, T, Map<String, Any?>, Any, RSP>(query as IQuery<D, R, T>, HashMap, ctx, block)

    suspend fun <C : IExecutorConfig, RSLT, TargetClass : Any, RSP>
            save(asyncExecutor: IAsyncExecutor<C, RSLT>, ctx: CoroutineContext = Dispatchers.Default,
                 into: KClass<T>,
                 block: suspend (TargetClass) -> RSP) =
            asyncExecutor.fetchAsync<D, R, T, TargetClass, TargetClass, RSP>(query as IQuery<D, R, T>, CustomRecord(into), ctx, block)
}