package design.animus.kotlin.frm.sql.query.common.frm

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.frm.sql.executors.common.IAsyncExecutor
import design.animus.kotlin.frm.sql.executors.common.IExecutorConfig
import design.animus.kotlin.frm.sql.query.common.SQLConditionalBuilder
import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.UpdateSet
import design.animus.kotlin.frm.sql.query.common.query.Fields
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.Table
import design.animus.kotlin.frm.sql.query.common.query.type.UpdateMutableQuery
import design.animus.kotlin.frm.sql.query.common.query.type.UpdateQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import mu.KotlinLogging


@Suppress("UNCHECKED_CAST")
class UpdateFRM<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val table: T,
        val sqlBuilder: ISQLBuilder<D, R, T>
) : IFRM<UpdateMutableQuery<D, R, T>> {
    override val logger = KotlinLogging.logger {}
    override var query = UpdateMutableQuery<D, R, T>(listOf())

    suspend fun mutateTo(block: suspend UpdateSet<D, R, T>.() -> Unit)
            : UpdateFRM<D, R, T> {
        val state = UpdateSet<D, R, T>()
        state.block()
        query = query.copy(changes = state.changes).apply { table = Table(this@UpdateFRM.table) }
        return this
    }

    suspend fun changeOn(block: suspend SQLConditionalBuilder<D,R,T>.() -> Unit)
            : UpdateFRM<D, R, T> {
        val state = SQLConditionalBuilder<D,R,T>()
        state.block()
        query = query.copy(conditions = state.conditions).apply { table = Table(this@UpdateFRM.table) }
        return this
    }

    suspend fun <C : Any?> returning(vararg fields: Fields<C, D, R, T>): ReturnMultipleCustom<D, R, T> {
        val castList = fields.toList() as List<Fields<Any?,D,R,T>>
        query = query.copy(returning = Option.Some(castList)).apply { table = Table(this@UpdateFRM.table) }
        return ReturnMultipleCustom(query.lockQuery(sqlBuilder) as UpdateQuery<D, R, T>)
    }

    suspend fun returning(): ReturnMultipleRecord<D, R, T> {

        query = query.copy(
                returning = Option.Some(listOf())
        ).apply { table = Table(this@UpdateFRM.table) }
        return ReturnMultipleRecord(query.lockQuery(sqlBuilder) as UpdateQuery<D, R, T>)
    }

    suspend fun <C : IExecutorConfig, RSLT>
            save(asyncExecutor: IAsyncExecutor<C, RSLT>) =
            asyncExecutor.save(
                    query.lockQuery(sqlBuilder) as UpdateQuery<D, R, T>
            )
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
        lazyUpdate(
        table: T,
        sqlBuilder: ISQLBuilder<D, R, T> = CommonSQLBuilder<D, R, T>()
): UpdateFRM<D, R, T> = UpdateFRM<D, R, T>(table, sqlBuilder)