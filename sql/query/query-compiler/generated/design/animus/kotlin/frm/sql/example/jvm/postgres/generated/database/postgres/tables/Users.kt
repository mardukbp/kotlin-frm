package design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables

import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.Postgres
import design.animus.kotlin.frm.sql.query.common.query.Field
import design.animus.kotlin.frm.sql.query.common.query.LateField
import design.animus.kotlin.frm.sql.query.common.query.PrimaryKey
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecordByMap
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable
import kotlin.Any
import kotlin.Int
import kotlin.String
import kotlin.collections.List
import kotlin.collections.Map
import kotlin.reflect.KClass

object Users : IPostgresTable<Postgres, UsersRecord> {
    val id: PrimaryKey<Int, Postgres, UsersRecord, Users> = UsersId

    val firstName: Field<String, Postgres, UsersRecord, Users> = UsersFirstName

    val lastName: Field<String, Postgres, UsersRecord, Users> = UsersLastName

    val lazyId: LateField<Int, Postgres, UsersRecord, Users> = UsersLazyId

    val lazyFirstName: LateField<String, Postgres, UsersRecord, Users> = UsersLazyFirstName

    val lazyLastName: LateField<String, Postgres, UsersRecord, Users> = UsersLazyLastName

    override val reflectedTableName: String = "users"

    override val reflectedDatabase: Postgres = Postgres

    override val reflectedColumns: List<String> = listOf("id", "firstName", "lastName")

    override val reflectedRecordClass: KClass<UsersRecord> = UsersRecord::class

    override val reflectedTable: IPostgresTable<Postgres, UsersRecord> = this
}

class UsersRecord(
    private val map: Map<String, Any>
) : ADatabaseRecordByMap(map) {
    val id: Int by map

    val firstName: String by map

    val lastName: String by map
}

object UsersId : PrimaryKey<Int, Postgres, UsersRecord, Users>(
    "id", "integer", "jasync_example",
    "users"
)

object UsersFirstName : Field<String, Postgres, UsersRecord, Users>(
    "first_name",
    "character varying", "jasync_example", "users"
)

object UsersLastName : Field<String, Postgres, UsersRecord, Users>(
    "last_name", "character varying",
    "jasync_example", "users"
)

object UsersLazyId : LateField<Int, Postgres, UsersRecord, Users>(
    "Int", "id", "integer",
    "jasync_example", "users"
)

object UsersLazyFirstName : LateField<String, Postgres, UsersRecord, Users>(
    "String", "first_name",
    "character varying", "jasync_example", "users"
)

object UsersLazyLastName : LateField<String, Postgres, UsersRecord, Users>(
    "String", "last_name",
    "character varying", "jasync_example", "users"
)
