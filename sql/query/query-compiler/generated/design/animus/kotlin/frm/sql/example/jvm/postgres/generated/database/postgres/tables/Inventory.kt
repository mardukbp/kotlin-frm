package design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables

import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.Postgres
import design.animus.kotlin.frm.sql.query.common.query.Field
import design.animus.kotlin.frm.sql.query.common.query.LateField
import design.animus.kotlin.frm.sql.query.common.query.PrimaryKey
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecordByMap
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable
import java.math.BigDecimal
import kotlin.Any
import kotlin.Int
import kotlin.String
import kotlin.collections.List
import kotlin.collections.Map
import kotlin.reflect.KClass

object Inventory : IPostgresTable<Postgres, InventoryRecord> {
    val id: PrimaryKey<Int, Postgres, InventoryRecord, Inventory> = InventoryId

    val name: Field<String, Postgres, InventoryRecord, Inventory> = InventoryName

    val price: Field<BigDecimal, Postgres, InventoryRecord, Inventory> = InventoryPrice

    val lazyId: LateField<Int, Postgres, InventoryRecord, Inventory> = InventoryLazyId

    val lazyName: LateField<String, Postgres, InventoryRecord, Inventory> = InventoryLazyName

    val lazyPrice: LateField<BigDecimal, Postgres, InventoryRecord, Inventory> = InventoryLazyPrice

    override val reflectedTableName: String = "inventory"

    override val reflectedDatabase: Postgres = Postgres

    override val reflectedColumns: List<String> = listOf("id", "name", "price")

    override val reflectedRecordClass: KClass<InventoryRecord> = InventoryRecord::class

    override val reflectedTable: IPostgresTable<Postgres, InventoryRecord> = this
}

class InventoryRecord(
    private val map: Map<String, Any>
) : ADatabaseRecordByMap(map) {
    val id: Int by map

    val name: String by map

    val price: BigDecimal by map
}

object InventoryId : PrimaryKey<Int, Postgres, InventoryRecord, Inventory>(
    "id", "integer",
    "jasync_example", "inventory"
)

object InventoryName : Field<String, Postgres, InventoryRecord, Inventory>(
    "name",
    "character varying", "jasync_example", "inventory"
)

object InventoryPrice : Field<BigDecimal, Postgres, InventoryRecord, Inventory>(
    "price", "numeric",
    "jasync_example", "inventory"
)

object InventoryLazyId : LateField<Int, Postgres, InventoryRecord, Inventory>(
    "Int", "id",
    "integer", "jasync_example", "inventory"
)

object InventoryLazyName : LateField<String, Postgres, InventoryRecord, Inventory>(
    "String", "name",
    "character varying", "jasync_example", "inventory"
)

object InventoryLazyPrice : LateField<BigDecimal, Postgres, InventoryRecord,
        Inventory>("BigDecimal", "price", "numeric", "jasync_example", "inventory")
