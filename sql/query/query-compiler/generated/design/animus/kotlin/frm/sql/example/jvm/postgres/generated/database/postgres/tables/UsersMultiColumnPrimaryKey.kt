package design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables

import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.Postgres
import design.animus.kotlin.frm.sql.query.common.query.LateField
import design.animus.kotlin.frm.sql.query.common.query.PrimaryKey
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecordByMap
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable
import kotlin.Any
import kotlin.Int
import kotlin.String
import kotlin.collections.List
import kotlin.collections.Map
import kotlin.reflect.KClass

object UsersMultiColumnPrimaryKey : IPostgresTable<Postgres, UsersMultiColumnPrimaryKeyRecord> {
    val id: PrimaryKey<Int, Postgres, UsersMultiColumnPrimaryKeyRecord, UsersMultiColumnPrimaryKey> =
        UsersMultiColumnPrimaryKeyId

    val email: PrimaryKey<String, Postgres, UsersMultiColumnPrimaryKeyRecord,
            UsersMultiColumnPrimaryKey> = UsersMultiColumnPrimaryKeyEmail

    val firstName: PrimaryKey<String, Postgres, UsersMultiColumnPrimaryKeyRecord,
            UsersMultiColumnPrimaryKey> = UsersMultiColumnPrimaryKeyFirstName

    val lastName: PrimaryKey<String, Postgres, UsersMultiColumnPrimaryKeyRecord,
            UsersMultiColumnPrimaryKey> = UsersMultiColumnPrimaryKeyLastName

    val lazyId: LateField<Int, Postgres, UsersMultiColumnPrimaryKeyRecord, UsersMultiColumnPrimaryKey> =
        UsersMultiColumnPrimaryKeyLazyId

    val lazyEmail: LateField<String, Postgres, UsersMultiColumnPrimaryKeyRecord,
            UsersMultiColumnPrimaryKey> = UsersMultiColumnPrimaryKeyLazyEmail

    val lazyFirstName: LateField<String, Postgres, UsersMultiColumnPrimaryKeyRecord,
            UsersMultiColumnPrimaryKey> = UsersMultiColumnPrimaryKeyLazyFirstName

    val lazyLastName: LateField<String, Postgres, UsersMultiColumnPrimaryKeyRecord,
            UsersMultiColumnPrimaryKey> = UsersMultiColumnPrimaryKeyLazyLastName

    override val reflectedTableName: String = "users_multi_column_primary_key"

    override val reflectedDatabase: Postgres = Postgres

    override val reflectedColumns: List<String> = listOf("id", "email", "firstName", "lastName")

    override val reflectedRecordClass: KClass<UsersMultiColumnPrimaryKeyRecord> =
        UsersMultiColumnPrimaryKeyRecord::class

    override val reflectedTable: IPostgresTable<Postgres, UsersMultiColumnPrimaryKeyRecord> = this
}

class UsersMultiColumnPrimaryKeyRecord(
    private val map: Map<String, Any>
) : ADatabaseRecordByMap(map) {
    val id: Int by map

    val email: String by map

    val firstName: String by map

    val lastName: String by map
}

object UsersMultiColumnPrimaryKeyId : PrimaryKey<Int, Postgres, UsersMultiColumnPrimaryKeyRecord,
        UsersMultiColumnPrimaryKey>("id", "integer", "jasync_example", "users_multi_column_primary_key")

object UsersMultiColumnPrimaryKeyEmail : PrimaryKey<String, Postgres,
        UsersMultiColumnPrimaryKeyRecord, UsersMultiColumnPrimaryKey>(
    "email", "character varying",
    "jasync_example", "users_multi_column_primary_key"
)

object UsersMultiColumnPrimaryKeyFirstName : PrimaryKey<String, Postgres,
        UsersMultiColumnPrimaryKeyRecord, UsersMultiColumnPrimaryKey>(
    "first_name", "character varying",
    "jasync_example", "users_multi_column_primary_key"
)

object UsersMultiColumnPrimaryKeyLastName : PrimaryKey<String, Postgres,
        UsersMultiColumnPrimaryKeyRecord, UsersMultiColumnPrimaryKey>(
    "last_name", "character varying",
    "jasync_example", "users_multi_column_primary_key"
)

object UsersMultiColumnPrimaryKeyLazyId : LateField<Int, Postgres, UsersMultiColumnPrimaryKeyRecord,
        UsersMultiColumnPrimaryKey>(
    "Int", "id", "integer", "jasync_example",
    "users_multi_column_primary_key"
)

object UsersMultiColumnPrimaryKeyLazyEmail : LateField<String, Postgres,
        UsersMultiColumnPrimaryKeyRecord, UsersMultiColumnPrimaryKey>(
    "String", "email",
    "character varying", "jasync_example", "users_multi_column_primary_key"
)

object UsersMultiColumnPrimaryKeyLazyFirstName : LateField<String, Postgres,
        UsersMultiColumnPrimaryKeyRecord, UsersMultiColumnPrimaryKey>(
    "String", "first_name",
    "character varying", "jasync_example", "users_multi_column_primary_key"
)

object UsersMultiColumnPrimaryKeyLazyLastName : LateField<String, Postgres,
        UsersMultiColumnPrimaryKeyRecord, UsersMultiColumnPrimaryKey>(
    "String", "last_name",
    "character varying", "jasync_example", "users_multi_column_primary_key"
)
