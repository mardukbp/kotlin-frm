package design.animus.kotlin.frm.sql.query.compiler.records

sealed class DBType

object Postgres : DBType()
object Common : DBType()