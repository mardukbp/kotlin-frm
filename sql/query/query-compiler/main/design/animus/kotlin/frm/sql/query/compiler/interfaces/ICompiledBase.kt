package design.animus.kotlin.frm.sql.query.compiler.interfaces

import design.animus.kotlin.frm.sql.query.common.builder.SQLQuery

typealias QueryVersion = Number

interface ICompiledBase<R> {
    val sqlQuery: SQLQuery
    val version: QueryVersion
}