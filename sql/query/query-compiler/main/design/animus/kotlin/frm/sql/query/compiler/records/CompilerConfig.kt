package design.animus.kotlin.frm.sql.query.compiler.records

import design.animus.kotlin.frm.sql.query.compiler.interfaces.QueryVersion
import java.io.File

data class CompilerConfig(
    val namespace: String,
    val name: String,
    val version: QueryVersion,
    val fileDestination: File,
    val dbType: DBType
)
