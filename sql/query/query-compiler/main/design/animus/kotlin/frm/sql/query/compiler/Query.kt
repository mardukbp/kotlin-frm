@file:Suppress("UNCHECKED_CAST")

package design.animus.kotlin.frm.sql.query.compiler

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.kotlin.frm.sql.query.common.builder.SQLQuery
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.type.*
import design.animus.kotlin.frm.sql.query.common.query.verbs.ISelectQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.compiler.interfaces.ICompiledQuery
import design.animus.kotlin.frm.sql.query.compiler.interfaces.QueryVersion
import design.animus.kotlin.frm.sql.query.compiler.records.CompilerConfig
import design.animus.kotlin.frm.sql.query.postgres.query.JsonAggregate
import design.animus.kotlin.frm.sql.query.postgres.query.JsonObject
import design.animus.kotlin.frm.sql.query.postgres.query.aggregates.JsonAggPair
import design.animus.kotlin.frm.sql.schema.common.renameClass
import design.animus.kotlin.frm.sql.schema.common.renameProperty
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable
import kotlin.coroutines.CoroutineContext

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> compileQuery(
    query: LockedQuery<D, R, T>,
    config: CompilerConfig
) {
    val queryType = buildQueryObject(config, query)
    val file = FileSpec.builder(config.namespace, config.name)
    val record = when (query) {
        is SelectQuery, is SelectJoinQuery -> {
            val (recordType, extraType) = buildRecordObject<D, R, T>(config, query as ISelectQuery)
            file.addType(recordType)
            extraType.forEach { file.addType(it) }
        }
        is UpdateQuery -> TODO()
        is InsertQuery -> TODO()

        is DeleteQuery -> TODO()
    }
    file.addType(queryType)
    file
        .build()
        .writeTo(config.fileDestination)
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> buildQueryObject(
    config: CompilerConfig,
    query: IQuery<D, R, T>
): TypeSpec {
    val recordName = "${config.name}Record"
    return TypeSpec.objectBuilder(ClassName(config.namespace, config.name))
        .addSuperinterface(ICompiledQuery::class.asTypeName().parameterizedBy(ClassName(config.namespace, recordName)))
        .addProperty(
            PropertySpec.builder("sqlQuery", SQLQuery::class.asTypeName())
                .addModifiers(KModifier.OVERRIDE)
                .initializer("%S", query.buildSQLString())
                .build()
        )
        .addProperty(
            PropertySpec.builder("version", QueryVersion::class.asTypeName())
                .addModifiers(KModifier.OVERRIDE)
                .initializer("%L", config.version)
                .build()
        )
        .build()
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> buildJsonRecord(
    config: CompilerConfig,
    classTitle: String,
    pairs: List<JsonAggPair>
): Pair<TypeSpec, List<TypeSpec>> {
    val props = pairs.map { aggField ->
        buildFieldProperty<Any?, D, R, T>(
            config,
            aggField.second as Fields<Any?, D, R, T>,
            aggField.first
        )
    }
    val type = TypeSpec.classBuilder(classTitle)
        .addModifiers(KModifier.DATA)
        .addAnnotation(Serializable::class)
        .primaryConstructor(
            FunSpec.constructorBuilder()
                .addParameters(
                    props.map {
                        val prop = it.first
                        ParameterSpec.builder(prop.name, prop.type)
                            .build()
                    }
                )
                .build()
        )
        .addProperties(props.map {
            val prop = it.first
            PropertySpec.builder(prop.name, prop.type)
                .initializer(prop.name)
                .build()
        })
        .build()
    return Pair(type, props.map { it.second }.flatten())
}

suspend fun <C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> buildFieldProperty(
    config: CompilerConfig,
    field: Fields<C, D, R, T>,
    name: String? = null
)
        : Pair<PropertySpec, List<TypeSpec>> {
    val secondaryTypes = mutableListOf<TypeSpec>()
    val props = when (field) {
        is PrimaryKeys, is NamedField, is Field -> PropertySpec.builder(
            renameProperty(field.columnName),
            String::class
        )
            .build()
        is AggregateField -> {
            when (field.aggregate) {
                Sum, Count, Average, Minimum, Maximum -> PropertySpec.builder(
                    renameProperty(field.columnName),
                    Number::class.asTypeName()
                )
                    .build()
                is JsonAggregate -> {
                    val jsonAgg = field.aggregate as JsonAggregate
                    val classTitle = "Json${renameClass(
                        field.named
                    )}Record"
                    val className = ClassName(config.namespace, classTitle)
                    val (recordType, extraTypes) = buildJsonRecord<D, R, T>(config, classTitle, jsonAgg.struct)
                    secondaryTypes.add(recordType)
                    secondaryTypes += extraTypes

                    PropertySpec.builder(
                        renameProperty(field.named),
                        List::class.asTypeName().parameterizedBy(className)
                    )
                        .build()

                }
                is JsonObject -> {
                    val jsonObj = field.aggregate as JsonObject
                    val classTitle = "Json${renameClass(
                        name?.capitalize() ?: field.named
                    )}Record"
                    val (mainType, extraTypes) = buildJsonRecord<D, R, T>(config, classTitle, jsonObj.pairs)
                    secondaryTypes.add(mainType)
                    secondaryTypes += extraTypes
                    PropertySpec.builder(name ?: field.named, ClassName(config.namespace, classTitle))
                        .build()
                }
                else -> {
                    throw Exception("Unknown aggregate type")
                }
            }

        }
        is LateField -> throw Exception("Late fields are only supported on prepared queries.")
    }
    return Pair(props, secondaryTypes.toList())
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> buildRecordObject(
    config: CompilerConfig,
    query: ISelectQuery,
    ctx: CoroutineContext = Dispatchers.Default
) = withContext(ctx) {
    val props = query.columns.map { col ->
        buildFieldProperty<Any?, D, R, T>(config, col as Fields<Any?, D, R, T>)
    }
    val recordType = TypeSpec.classBuilder("${config.name}Record")
        .addModifiers(KModifier.DATA)
        .addProperties(props.map {
            val prop = it.first
            PropertySpec.builder(prop.name, prop.type)
                .initializer(prop.name)
                .build()
        })
        .primaryConstructor(
            FunSpec.constructorBuilder()
                .addParameters(
                    props.map {
                        val prop = it.first
                        ParameterSpec.builder(prop.name, prop.type)
                            .build()
                    }
                )
                .build()
        )
        .build()
    Pair(recordType, props.map { it.second }.flatten())

}

