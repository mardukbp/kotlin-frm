package design.animus.kotlin.frm.sql.query.compiler

import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.Inventory
import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.UserOrders
import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.Users
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.type.IPreparedQuery
import design.animus.kotlin.frm.sql.query.common.query.verbs.ISelectQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.compiler.records.CompilerConfig
import design.animus.kotlin.frm.sql.query.compiler.records.Postgres
import design.animus.kotlin.frm.sql.query.postgres.builder.PostgresSQLBuilder
import design.animus.kotlin.frm.sql.query.postgres.frm.lazySelectJoin
import design.animus.kotlin.frm.sql.query.postgres.query.aggregates.jsonAgg
import java.io.File

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> compilePreparedQuery(query: IPreparedQuery<D, R, T>) {

}


suspend fun main() {
    val testQuery = lazySelectJoin(Users)
        .withColumns(Users.id, Users.firstName, Users.lastName,
            jsonAgg(name = "orders") {
                jsonObj {
                    "orderPlaced" to UserOrders.orderPlaced
                    "inventory" to jsonObj {
                        "id" to Inventory.id
                        "name" to Inventory.name
                        "price" to Inventory.price
                    }
                }
            }
        )
        .join(UserOrders) { UserOrders.userId equal Users.id }
        .join(Inventory) { Inventory.id equal UserOrders.inventoryId }
        .groupBy(Users.id, Users.firstName, Users.lastName).query.lockQuery(PostgresSQLBuilder())
    val config = CompilerConfig(
        "design.animus.kotlin.frm.sql.query.compiler.test",
        "GetUserOrders",
        1,
        File("/home/sobrien/dev/kotlin-frm/sql/query/query-compiler/generated"),
        Postgres
    )
    (testQuery as ISelectQuery).columns.forEach { field ->
        when (field) {

        }
    }
    compileQuery(testQuery, config)

}