## Summary

Kotlin FRM is a [multi-platform](https://kotlinlang.org/docs/reference/multiplatform.html),
multi data-set type safe tool set. It will work with multiple data sources, SQL only for now.
Reflecting on the schema, and generating out types via a gradle plugin. THen provide
generic methods that consume those types.

* [Apache License](./LICENSE.md)

#### Modules

* [SQL](./sql/intro.md)
