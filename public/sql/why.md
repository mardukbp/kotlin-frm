**Core Features**

* Schema First, generates types off your schema via a build (*gradle plugin*)
* Multi platform support for js, jvm, native
    * Support for multiple Executor i.e. JDBC and JaSync
* DSL that maps closely to SQL. 
* Functional builder methods that map closely to collection methods.
* Support for advanced aggregation methods on different databases
    * *json_agg*
* Functional Wrappers
    * Executors that return types instead of exceptions
    * [Arrow](https://arrow-kt.io) Wrapper
* Can operate as a type safe string builder
* Support for kotlin features
    * Suspend non-blocking connections
    * Multi column primary keys (*Pair, Triple*)   
* Generation of common SQL operations
    * Get by ID
    * update by id
    * etc.
* SQL for now more data bases coming soon. 
* Compiled queries
    * Don't waste cycles using the DSL, have the query as a string ready for the connection layer.
* Designed with Flyway in mind for ease of migrations.``