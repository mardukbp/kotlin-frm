Selecting start with a `lazySelect` function. With a single parameter of the `table`.

```kotlin
lazySelect(Users)
    .filter { Users.id equal 1 }
```

```sql
select * from users where users.id = 1
```

###### Specifying Columns

**Note: If you use specify columns it will return a Map**
**But you can do a `mapInto` specifying the target data class.**

```kotlin
lazySelect(Users)
    .withColumns(Users.id, Users.firstName, Users.lastName, UserProfile.email)
```

```sql
select users.id, users.first_name, users.last_name, users.email
```

The function takes in a list of fields, or aggregate fields. You can perform similar operations in the
`withColumns` block as a standard select. Items like `count`, `as`, `json_agg`, etc. will work.

```kotlin
lazySelect(Users)
    .withColumns(count(Users.id) AS "totalUsers"))
```

```sql
select count(users.id) as totalUsers
```

###### Joining Tables

`join` is a function that takes a lambda to build out the conditional list.

```kotlin
lazySelect(Users)
    .withColumns(Users.id, Users.firstName, Users.lastName, UserProfile.email)
    .join(UsersProfile) {
        UsersProfile.userId equal Users.id
    }   
```

```sql
select users.id, users.first_name, users.last_name, user_profile.email
    from users
    join user_profile on users.id = users.id
```

###### Filtering Results

```kotlin
lazySelect(Users)
    .withColumns(count(Users.id) AS "totalUsers"))
    .filter {
        Users.firstName iLike "%ja%"
    }   
```

```sql
select  count(users.id) as totalUsers
    where users.first_name ilike '%ja%'
```
###### Group By

`groupBy` takes in a `vararg` of `Fields`.

```kotlin
.groupBy(Users.id, Users.firstName, Users.lastName) 
```

#### Full Query

```kotlin
val query = lazySelect(Users)
        .withColumns(Users.id, Users.firstName, Users.lastName,
                jsonAgg {
                    jsonObj {
                        "orderPlaced" to UserOrders.orderPlaced
                        "inventory" to jsonObj {
                            "id" to Inventory.id
                            "name" to Inventory.name
                            "price" to Inventory.price
                        }
                    }
                } AS "orders"
        )
        .join(UserOrders) { UserOrders.userId equal Users.id }
        .join(Inventory) { Inventory.id equal UserOrders.inventoryId }
        .groupBy(Users.id, Users.firstName, Users.lastName)
```
