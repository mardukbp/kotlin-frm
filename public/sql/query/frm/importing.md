Sometimes Intellij has trouble finding the extension methods. 
It is recommended to do a wild card import, then use code formatting
`optimize imports` to whittle down what is used.

**Common**

```kotlin
import design.animus.kotlin.frm.sql.query.common.frm.*
```

**Postgres**

```kotlin
import design.animus.kotlin.frm.sql.query.postgres.frm.*
```

**Maria DB**

```kotlin
import design.animus.kotlin.frm.sql.query.mariadb.frm.*
```


