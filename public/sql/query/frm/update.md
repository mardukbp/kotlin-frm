
#### Specifying Update Constraint

Specifying the criteria for updating utilizes the [`ConditionBuilder DSL`](./sql/query/sql_dsl/condition_builder.md)

```kotlin
lazyUpdate(Users)
    .changeOn {
        (Users.firstName equal "bob") and
        (Users.lastName equal "dean")
    }
```

```sql
update example.users
    where users.first_name = 'bob' AND users.last_name = 'dean'
```
#### Changes to Apply

```kotlin
.mutateTo {
    Users.firstName TO "Sausage"
    Users.lastName TO "Man"
}
```

```sql
set first_name = 'Sausage', last_name='Man'
```

#### Full Query

```kotlin
val delayedResult =
        lazyUpdate(Users)
                .changeOn {
                    (Users.firstName equal "bob") and
                    (Users.lastName equal "dean")
                }
                .mutateTo {
                    Users.firstName TO "Sausage"
                    Users.lastName TO "Man"
                }
                .save(executor)
```

```sql
update example.users SET first_name = 'Sausage',last_name = 'Man'
    where users.first_name = 'bob' AND users.last_name = 'dean'
```