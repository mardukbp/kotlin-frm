**Warning**

This is a costly function, when using `json_agg` it is recommended to compile the query.

#### Example

**FRM**

```kotlin
  val delayedQuery = Users
                .withColumns(Users.id, Users.firstName, Users.lastName,
                        jsonAgg {
                            jsonObj {
                                "orderPlaced" to UserOrders.orderPlaced
                                "inventory" to jsonObj {
                                    "id" to Inventory.id
                                    "name" to Inventory.name
                                    "price" to Inventory.price
                                }
                            }
                        } AS "orders"
                )
                .join(UserOrders) { UserOrders.userId equal Users.id }
                .join(Inventory) { Inventory.id equal UserOrders.inventoryId }
                .groupBy(Users.id, Users.firstName, Users.lastName)
                .map(executor) {
                    it.forEach { row ->
                        row.forEach { (key, value) ->
                            if (key == "orders") {
                                println(value)
                                println(json.parse(JsonOrder.serializer().list, value as String))
                            } else {
                                println("Key:$key \n\t$value")
                            }

                        }
                    }
                }
```

**DSL**

```kotlin
selectJ(Users.id, Users.firstName, Users.lastName, jsonAgg {
                jsonObj {
                    "orderPlaced" to UserOrders.orderPlaced
                    "inventory" to jsonObj {
                        "id" to Inventory.id
                        "name" to Inventory.item
                        "price" to Inventory.price
                    }
                }
            } AS "orders" ) from Users join UserOrders on {
                UserOrders.userId equal Users.id
            } join Inventory on {
                Inventory.id equal UserOrders.inventoryId
            } groupBy listOf(
                    Users.id, Users.firstName, Users.lastName
            )
```

#### Function

`jsonAgg` is a function launching a DSL to build out the aggregate function. It takes
one parameter as a lambda. Where the lambda is used to build a json object ([*see documentation*](./json_obj.md)).
The items can be nested, allowing for nested objects. From a type stand point all keys
will be a `String`, and values are expected to be an `Any?`

Values are assigned to a key by the infix function `to`. This will return a json array containing
the constructed objects.


