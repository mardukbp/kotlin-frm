`jsonObj` is used to build out a json object from a series of fields. It can be used
when wanting to combine several tables into one table structure.

```kotlin
jsonObj {
    "orderPlaced" to UserOrders.orderPlaced
    "inventory" to jsonObj {
        "id" to Inventory.id
        "name" to Inventory.item
        "price" to Inventory.price
    }
}
```