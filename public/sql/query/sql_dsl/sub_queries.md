Sub queries allow you to nest queries, or make an additional 
query in either the column or where section.

**Sample**

```kotlin
val query = Profile.query {
    select() from Profile where {
        Profile.id contains Users.nestedQuery {
            select(Users.id) from Users where {
                Users.firstName iLike "%ja%"
            }
        }
    }
}
```

```sql
SELECT * from user_profile
    where user_profile.id in (
        SELECT users.id from users
            where users.first_name ilike '%ja%'
    )
```

**Error Checking**