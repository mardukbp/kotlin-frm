#### Select

**Picking Columns**

There are several methods to define the columns queried from the
database. Of note is due to conflict with JVM compilation, the
join query is denoted with a `selectJ`. 

Following are the kotlin function calls, and the generated sql.

*Note: If you pass in a custom column selection, or join. The geenrated
records will not work. You need to do a `map into` call.* 

```kotlin
select()
```
    
```sql
select *
```
 
```kotlin
select(vararg fields: Fields<*, D, R, T>)
select(Users.id, Users.firstName, User.lastName)
```

```sql
select users.id, users.first_name users.last_name
```

```kotlin
suspend fun selectJ(vararg fields: Fields<*, D, *, *>)               
select(Users.id, Users.firstName, User.lastName, UserProfile.email)
```                                             

```sql
select users.id, users.first_name users.last_name, user_profile.email
```                
You can also name columns. There is an infix function `AS`. The left side
is the `target field` and the right is the `name` which is a string.

```kotlin
select(Users.id, Users.firstName AS "alias", User.lastName, UserProfile.email)
```
```sql
select users.id, users.first_name as alias users.last_name, user_profile.email   
```

**Aggregates**

There are database specific aggregates, and common aggregates you can 
apply. We only show `count` below. But the principle applies to other
functions as well.

We also support more advanced methods, but they are specific to the
database method. As an example `jsonAgg` is specific to Postgres. 

```kotlin
count(field: Fields<C, D, R, T>)
avg(field: Fields<C, D, R, T>)
sum(field: Fields<C, D, R, T>) 
min(field: Fields<C, D, R, T>)
max(field: Fields<C, D, R, T>) 
```                           

```kotlin
select(count(Users.id)) from Users
```                               

```sql
select count(users.id) from users
```

**Defining the table**

From is an infix function.

```kotlin
select() from Users
```

```sql
select * from users
```                

You can also name tables

```kotlin
select() from (Users AS "People")
```

```sql
select * from users as people
````

**Joins**

Joins follow a similar pattern irrespective of the join type. 

```kotlin
select() from Users join UsersProfile on { UsersProfile.userId equal Users.id}
```
```sql
select * from users
    join user_profile on users_profile.user_id = users.id
```

This also supports named tables as well.

```kotlin
select() from Users join (UsersProfile as "profiles") on { UsersProfile.userId equal Users.id}
```

```sql
select * from users
    join user_profile as profiles 
        on users_profile.user_id = users.id
```

**Where**

[See the condition builder](./condition_builder.md)
