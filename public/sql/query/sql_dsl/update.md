```kotlin
update(Users) set {
    Users.firstName TO "Bob"
    Users.lastName TO "Smith"
} where {
    (Users.id equal 1)
}
```

```sql
update users SET users.first_name = 'Bob',users.last_name = 'Smith' where users.id = 1
```
#### Setting the Table

Update takes in a table either named or regular.

```kotlin
update(Users)
```

```kotlin
update(Users AS "people")
```

#### Setting the Changes

Changes are applied via a DSL builder. 

The field is mapped to the new value via an infix function `TO`. 
*These will need to match on type. I.E. if a field is `Long` the new value must be `Long`*

```kotlin
set {
    Users.firstName TO "Bob"
    Users.lastName TO "Smith"
}
```

#### Criteria to update on

[This follows the condition builder for other parts. See that documentation](./sql/query/sql_dsl/condition_builder.md)

```kotlin
where {
    (Users.id equal 1)
}
```