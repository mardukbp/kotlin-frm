Defining conditions whether it be in a `where` statement for select,
`update on`, on `join on`. Are all completed via a similar DSL builder.
These are type safe where in if a field is Long it will only compile on
a Long. 

Filtering on a single field does not require enclosure in `()`. 

```kotlin
where {  
    Users.id equal 1
}
```

If you are to use multiple conditions they need to be enclosed in a `()`.                                                                
                                                                
```kotlin
where {
    (Users.firstName equal "Jack") AND (users.lastName equal "Black") 
}
```

**Conditional Checks**

```kotlin
equal
gt
gte
lte
like
iLike
notEqual
contains
```

The conditional checks have two implementations. One is based on the generic column
type. Where it matches the field type. The other matches a field with the same type.

As an example if you have two fields with of `bigint` / `Long` that will work.
But if one field is `varchar` and the other is `int` that will not work.

**Conditional Combinators**

The available combination patterns:

```
AND
OR
NOT
```

These can be combined with the above conditional checks to define a conditional
pattern.
