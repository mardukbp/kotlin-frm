#### Specifying Table

**Regular Table**

```kotlin
delete(Users)
```

```sql
delete from users
```

```kotlin
delete(Users AS "people")
```

```sql
delete from users as people
```

#### Specifying Conditions

```kotlin
where {
    (Users.firstName iLike "%ja%") and (Users.lastName iLike "%doe%")
}
```

```sql
where users.first_name ilike '%ja%' and users.last_name ilike '%doe%'
```

#### Full Query

```kotlin
  val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            delete(Users) where { Users.firstName iLike "%ja%" }
    }
```

```sql
delete from users where users.first_name ilike '%ja%' and users.last_name ilike '%doe%'
```