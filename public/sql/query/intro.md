There are two modes to query the database. We leave it to you
to choose which one to use.

**Important**

This is not aware of a connection, or resource. This simply
builds a syntax correct query. This can then be passed to an executor.

**DSL Query**

```kotlin    
val query = sqlQuery<ExampleDatabase, ADatabaseRecord, ITable<ExampleDatabase, ADatabaseRecord>> {
    selectJ(Users.id, Users.firstName, Profile.email) from Users innerJoin (Profile AS "profiles") on {
        (Users.id equal Profile.id)
    } where {
        (Users.firstName iLike "%ja%")
    }
}
```

**FRM Query**

```kotlin
val result = Users
    .filter {
        (Users.id equal 1L)
    }
    .mapOne(asyncExecutor) {
        "Hello ${it.firstName} ${it.lastName}"
    }
```
#### Query Stages

A query has two stages `Mutable` and `Locked`. 

During the initial building phase while a query is being defined it sits in a `Mutable` state.
This allows for the different fields to be adjusted. `MutableQuery` is a sealed class with a
number of sub classes. Those sub classes have a number of methods to help with the query
definition. 

Once you have finished building the query. It moves to a `locked query`. The primary difference
is that the `locked` type has no helper methods. You can change it via a `.copy`, but that
is not recommended.

The locked query is meant to be passed into the executor.
 
