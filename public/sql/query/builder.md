A builder takes the constructed query and builds it into an appropriate string.
It can be thought of as a `string builder` or `template engine`. The final built query
takes in a class for the sql builder. 

```kotlin
sealed class LockedQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        open val sqlBuilder: ISQLBuilder<D, R, T>,
...
)
```

#### Builder Interface

```kotlin
interface ISQLBuilder<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> {
    fun getTablePair(queryTable: QueryTable): TableNamePair
    fun buildColumnString(schemaTable: SchemaTableName, friendlyTable: FriendlyTableName, field: Fields<*, *, *, *>)
            : String

    fun buildGroupBy(schemaTable: SchemaTableName, friendlyTable: FriendlyTableName, fields: List<Fields<*, *, *, *>>): String
    suspend fun buildWhereString(conditions: List<ConditionalSteps>, table: FriendlyTableName): WhereString
    suspend fun buildSelectQuery(query: ISelectQuery): String
    suspend fun buildUpdateQuery(query: IUpdateQuery<D, R, T>): String
    suspend fun buildInsertQuery(query: IInsertQuery<D, R, T>): String
    suspend fun buildSelectJoinQuery(query: ISelectJoinQuery): String
}
```

The interface has methods for building each respective query type. The queries
are also a `sealed class` allowing for pattern matching on the passed query.

#### Why do we need a builder?

While [SQL is an ANSI standard](https://en.wikipedia.org/wiki/SQL). There are slight
variations between databases. Postgres may want the schema when first bringing a table
in scope, something else may want a database. This allows us the flexibility to customize
how the query strings are built. You can also subclass, or implement your own builder.