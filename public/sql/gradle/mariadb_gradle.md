The following snippet shows how to configure schema reflection for Maria DB.
This is utilizing the `Gradle KTS` standard. It is targeting the `JVM` platform.
As noted if doing multi platform ensure you have the `multiplatform` plugin with appropriate
source sets. 

*It is recommended to use this with FlyWay.*

```kotlin
plugins {
    id("design.animus.kotlin.frm.sql.schema.mariadb") version KotlinFRMVersion
}

val dbHost = System.getenv("myDataBaseHost") ?: "localhost"
val db = "example_jasync"
val dbPort = (System.getenv("myDataBasePort") ?: "3306").toInt()
val dbUser = System.getenv("myDataBaseUser") ?: "mariadb"
val dbPassword = System.getenv("myDataBasePassword") ?: "mariadb"

MariaDBGeneratorConfig {
    dataBases = listOf(db)
    dataBaseHost = dbHost
    dataBaseUser = dbUser
    dataBasePassword = dbPassword
    dataBasePort = dbPort
    namespace = "design.animus.kotlin.frm.sql.example.jvm.postgres.generated"
    outputDir = "$projectDir/generated"
    platforms = setOf(design.animus.kotlin.frm.sql.schema.common.JVM)
    excludeTables = setOf("flyway_schema_history")
}
```