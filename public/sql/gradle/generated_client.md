With the platforms being a sealed class. It is recommended to do this in the `kts` script.

This gradle file places the reflected schema into a directory of `generated`. Seperating your business logic
from the generated types of the database. 

**Dependencies**

There are two types as noted below the query builder and executor. Pull in those dependencies as you see fit.
This allows you to change the core executor from say `JaSync` to `JDBC` or something else entirely. The same
goes with the `query` builder. Pull in the modules you need to build your queries.

*Query*

Here we are using both `query-common` and `query-postgres`. 

*Executor*

Here we are using `exuector:jasync-postgres`.


#### JVM Client

```kotlin
plugins {
    kotlin("jvm")
    id("kotlinx-serialization")
    id("design.animus.kotlin.frm.sql.schema.postgresql") version "0.0.1"
    id("org.flywaydb.flyway") version "6.0.7"
}

kotlin {
    target {

    }
    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
                implementation(project(":sql:executors:jvm:jasync-postgres:"))
                implementation("io.github.microutils:kotlin-logging:1.7.6")
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.2")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.13.0")
                implementation("org.postgresql:postgresql:42.2.8")
                implementation(project(":sql:query:query-common:"))
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
                implementation(project(":sql:executors:jvm:jasync-postgres:"))
                implementation("com.github.jasync-sql:jasync-postgresql:1.0.11")

            }
        }
    }
}

val dbHost = System.getenv("pgDataBaseHost") ?: "localhost"
val db = System.getenv("pgDataBase") ?: "postgres"
val dbPort = (System.getenv("pgDataBasePort") ?: "5432").toInt()
val dbUser = System.getenv("pgDataBaseUser") ?: "postgres"
val dbPassword = System.getenv("pgDataBasePassword") ?: "postgres"

flyway {
    url = "jdbc:postgresql://$dbHost/$db?user=$dbUser&password=$dbPassword"
    schemas = arrayOf("example")
    locations = arrayOf("filesystem:$projectDir/main/resources")
}

databaseGeneratorConfig {
    dataBases = listOf(db)
    dataBaseHost = dbHost
    dataBaseUser = dbUser
    dataBasePassword = dbPassword
    dataBasePort = dbPort
    namespace = "design.animus.kotlin.frm.sql.example.jvm.postgres.generated"
    outputDir = "$projectDir/generated"
    dataBaseSchema = "example"
    platforms = setOf(design.animus.kotlin.frm.sql.schema.common.JVM)
    excludeTables = setOf("flyway_schema_history")
}
```