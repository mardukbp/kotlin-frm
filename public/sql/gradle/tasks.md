#### Tasks

**checkConnection**

This will utilize the configuration settings, and verify it can make
a connection to the database.

**buildSchema**

This will build the code from the reflected schema.