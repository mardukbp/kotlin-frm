The following snippet shows how to configure schema reflection for Postgres.
This is utilizing the `Gradle KTS` standard. It is targeting the `JVM` platform.
As noted if doing multi platform ensure you have the `multiplatform` plugin with appropriate
source sets. 

```kotlin
plugins {
    id("design.animus.kotlin.frm.sql.schema.postgresql") version KotlinFRMVersion
}

val dbHost = System.getenv("pgDataBaseHost") ?: "localhost"
val db = System.getenv("pgDataBase") ?: "postgres"
val dbPort = (System.getenv("pgDataBasePort") ?: "5432").toInt()
val dbUser = System.getenv("pgDataBaseUser") ?: "postgres"
val dbPassword = System.getenv("pgDataBasePassword") ?: "postgres"

PostgresGeneratorConfig {
    dataBases = listOf(db)
    dataBaseHost = dbHost
    dataBaseUser = dbUser
    dataBasePassword = dbPassword
    dataBasePort = dbPort
    namespace = "design.animus.kotlin.frm.sql.example.jvm.postgres.generated"
    outputDir = "$projectDir/generated"
    dataBaseSchema = "jasync_example"
    platforms = setOf(design.animus.kotlin.frm.sql.schema.common.JVM)
    excludeTables = setOf("flyway_schema_history")
}
```