During reflection the following table determines the types.

| PostgreSQL type         	| Java type     	| Javascript 	| Native  	| Common  	|
|-------------------------	|---------------	|------------	|---------	|---------	|
| boolean                 	| Boolean       	| Boolean    	| Boolean 	| Boolean 	|
| smallint                	| Short         	| Short      	| Short   	|  Short  	|
| integer (or serial)     	| Int           	| Int        	| Int     	| Int     	|
| bigint (or bigserial)   	| Long          	| Long       	| Long    	| Long    	|
| numeric                 	| BigDecimal    	| Number     	| Number  	| Number  	|
| real                    	| Float         	| Float      	| Float   	| Float   	|
| decimal                 	| BigDecimal    	| String     	| String  	| String  	|
| double                  	| Double        	| Double     	| Double  	| Double  	|
| text                    	| String        	| String     	| String  	| String  	|
| varchar                 	| String        	| String     	| String  	| String  	|
| bpchar                  	| String        	| String     	| String  	| String  	|
| timestamp               	| LocalDateTime 	| Long       	| Long    	| Long    	|
| timestamp_with_timezone 	| DateTime      	| Long       	| Long    	| Long    	|
| date                    	| LocalDate     	| String     	| String  	| String  	|
| time                    	| LocalTime     	| String     	| String  	| String  	|
| uuid                    	| UUID          	| String     	| String  	| String  	|
| json                    	| String        	| String     	| String  	| String  	|
| jsonb                   	| String        	| String     	| String  	| String  	|

#### Json

JSON support is still being fleshed out but it a focus.