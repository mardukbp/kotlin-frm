The hierarchy of schema generation is broken into several parts.

* Database
* Table
* Record
* Field

#### Database

The core interface for a database is as follows.

```kotlin
interface IDatabase {
    val database: String
}
```

Some databases extend on this, *Postgres* adds a schema field.. But all tables are 
categorized, and fall under a data base. Which will provide scope to your queries.

##### Tables

We reserve a number of properties to provide information on the table, and how it
maps to the rest of the domain. These are all prefixed with `reflected` to avoid
collisions with column names.

```kotlin
interface ITable<D : IDatabase, R : ADatabaseRecord> {
    val reflectedTableName: String
    val reflectedDatabase: D
    val reflectedColumns: List<String>
    val reflectedRecordClass: KClass<R>
    val reflectedTable: ITable<D, R>
}
```

* **reflectedTableName** A string representation of the table in the database, case is not adjusted.
* **reflectedDatabase** A reference to the database object. 
* **reflectedColumns** A list of strings representing the columns. This is renamed to camel-case and used with the `zip`
    function to delegate by map.
* **reflectedRecordClass** A pointer to the record class that we cast data into.
* **reflectedTable** A reference to self.

*Why store KCLass, and references to self?*

In multi-platform support for reflection on some platforms is lacking. To get around this we include more information on
the query chain. This being the pre-cursor parent dependencies, and what the records will be cast to.