Instead of defining, or building classes. Then integrating
with the database. The data classes, objects, and other items
are generated based off a reflected schema. A gradle plugin
will connect to your database, reflect the schema then generate
the code.

There is a core configuration shared between the database providers.
But there will also be per database configuration options. 

It is recommended to utilize [flyway](sql/gradle/flyway.md) with this process.

