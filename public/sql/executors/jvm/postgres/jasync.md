Based on [`JaSync`](https://github.com/jasync-sql/jasync-sql), a postgres pool. That executes
on netty. Providing a non blocking data base access layer.

#### Executor Method Name Space

```kotlin
design.animus.kotlin.frm.sql.query.postgres.frm
```

#### Configuration

**The Configuration Data Class**

```kotlin
data class JaSyncPostgresConfig(
        override val databaseHost: String,
        override val database: String,
        override val databaseUser: String,
        override val databasePassword: String,
        override val databasePort: Int,
        val schema: String
) : IExecutorConfig
```

**Example Pulling from Env Vars**

```kotlin
internal val FRMTestConfig = JaSyncPostgresConfig(
        System.getenv("pgDataBaseHost"),
        System.getenv("pgDataBase"),
        System.getenv("pgDataBaseUser"),
        System.getenv("pgDataBasePassword"),
        5432,
        "example"
)
```

#### Establishing a Connection

```kotlin
val executor = JaSyncPostgresExecutor() 
executor.connect(FRMTestConfig) // Connect with config, returns Unit
```

The pool is self maintained. Each query you execute will ensure
the connection pool is active. If not a `NotConnected` exception will be thrown.

##### Using the Executor

To utilize the executor it is passed in as the last item in the chain. Returning an `AsyncDBResponse` which requires 
a fetch to get the final data.

```kotlin
val delayedResult = Users
        .filter {
            (Users.firstName equal "jack") and (Users.lastName equal "smith")
        } // Up to this point a SelectQuery was Built
        .mapOne(executor) { // A connection is pulled from the JaSync pool
            "Hello ${it.firstName} ${it.lastName}" // This is returned 
        }
// other things happen here
val welcomeMessage : String = delayedResult.fetch()
```

##### Crafting a Join Record

To build a custom target join record you provide a data class. Delegated by map. Which
subclasses `AJoinRecord`. This will then call the constructor. Zipping the resulting
row set into a map. With the columns defined on the field.

*Note: If a column is `first_name` we rename to `firstName`*

```kotlin
data class UserProfileJoinRecord(val map: Map<String, Any?>) : AJoinRecord(map) {
    val id: Int by map
    val firstName: String by map
    val lastName: String by map
    val email: String by map
}
```

It would be called with `mapInto` like:

```kotlin
.mapInto(executor, UserProfileJoinRecord::class) 
```
