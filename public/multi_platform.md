#### Explaining Multi Platform

What do we mean multi platform? This library has the goal of.

* Automatically generating types to represent your database across multiple platforms.
* Build a type safe query
* Provide platform and technology agnostic executors that take in those queries.

**The Problem**

The biggest problem with multi platform is the column data type. We don't fully have multi platform
data types. An example of this would be `UUID` or `Date`, these two types require specific
implementations for each platform.

**Solution**

By generating the fields as separate `objects/types` for each column. It allows us to generate
code automatically for each platform. See the [type table for Postgres](./sql/schema/postgres/types.md)
The columns are most important on conditional parameters in a query. Especially when you start
considering advanced conditional checks, like a date range or something similar.

By separating the query into a generic representation of the columns, table, and conditions in a query.
Then passing it into an executor which adheres to an interface. Allows us to implement across multiple
platforms.

**Longer Term**

In an upcoming release we will support compiling queries. **This is still pending development,
but will give an idea where we are going.** This will compile a query for multiple platforms,
but also generate the custom data classes. 

This means if you are doing a join, no custom data classes. They are built for you. Or if you're
doing `json_agg` Then we will build out the `Serializable` data classes for you.