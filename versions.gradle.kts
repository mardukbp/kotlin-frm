extra["baseGroup"] = "design.animus.kotlin.frm"
extra["buildNumber"] = System.getenv("CI_COMMIT_SHA") ?: ""
extra["artifactUser"] = System.getenv("ArtifactUser") ?: "user"
extra["artifactPassword"] = System.getenv("ArtifactPassword") ?: "password"
extra["baseVersion"] = "0.2.0-SNAPSHOT"
val baseVersion = extra["baseVersion"] as String
extra["projectVersion"] = if (baseVersion.endsWith("SNAPSHOT")) {
    if (extra["buildNumber"].toString().isNotEmpty()) {
        baseVersion.replace("-SNAPSHOT", "-${extra["buildNumber"]}-SNAPSHOT")
    } else {
        baseVersion
    }
} else {
    baseVersion
}
extra["artifactUrl"] = if (extra["baseVersion"].toString().contains("SNAPSHOT")) {
    System.getenv("ArtifactSnapShotURL") ?: "https://127.0.0.1"
} else {
    System.getenv("ArtifactURL") ?: "https://127.0.0.1"
}
extra["projectId"] = "15031851"