#### Summary

Kotlin Functional Relational Mapper (*FRM*)

* Schema first, generates type classes through a gradle plugin.
=======
##### Summary

* *License*: Apache
* [Full Documentation]()

Kotlin Functional Relational Mapper (*FRM*) is a type safe builder against your data base.

It borrows heavily from the concept of [Type Providers from F#](https://docs.microsoft.com/en-us/dotnet/fsharp/tutorials/type-providers/)
This can be summarized as connect to a data source, or read a file. Then generate types off that
reflected schema. 


**Core Features**

* Schema First, generates types off your schema via a build (*gradle plugin*)
* Multi platform support for js, jvm, native
    * Support for multiple Executor i.e. JDBC and JaSync
* DSL that maps closely to SQL. 
* Functional builder methods that map closely to collection methods.
* Support for advanced aggregation methods on different databases
    * *json_agg*
* Functional Wrappers
    * Executors that return types instead of exceptions
    * [Arrow](https://arrow-kt.io) Wrapper
* Can operate as a type safe string builder
* Support for kotlin features
    * Suspend non-blocking connections
    * Multi column primary keys (*Pair, Triple*)   
* Generation of common SQL operations
    * Get by ID
    * update by id
    * etc.
* SQL for now more data bases coming soon. 
* Compiled queries
    * Don't waste cycles using the DSL, have the query as a string ready for the connection layer.
    * Pre build join or aggregate function data classes.
* Designed with Flyway in mind for ease of migrations.

**SQL DSL**

```kotlin
selectJ(Users.id, Users.firstName, Users.lastName, jsonAgg {
    jsonObj {
        "orderPlaced" to UserOrders.orderPlaced
        "inventory" to jsonObj {
            "id" to Inventory.id
            "name" to Inventory.item
            "price" to Inventory.price
        }
    }
}) from Users join UserOrders on {
    UserOrders.userId equal Users.id
} join Inventory on {
    Inventory.id equal UserOrders.inventoryId
} groupBy listOf(
        Users.id, Users.firstName, Users.lastName
)
```
**Functional**

```kotlin
 Users
    .withColumns(Users.id, Users.firstName, Users.lastName, UserProfile.email)
    .join(UserProfile) {
        UserProfile.id equal Users.id
    }
    .filter { Users.firstName iLike "%ja%" }
    .mapInto(executor, UserProfileJoinRecord::class) {
        val sorted = it.sortedBy { item -> item.id }
        assertTrue { sorted[0].id == 1 && sorted[0].email == "jdoe@google.com" }
        assertTrue { sorted[1].id == 2 && sorted[1].email == "jsmith@google.com" }
        assertTrue { sorted[2].id == 4 && sorted[2].email == "jblack@hotmail.com" }
    }
```

## Building Blocks

The core of the project is divided into three separate parts.

* Schema - Reflects the schema building types via a gradle plugin.
* Query - Functions that build out the query from the reflected types.
* Executor - A wrapper that passes the query to an execution library.

The schema generator can generate multi-platform code. A majority of the query module is `common` and 
does not target a specific platform. 

The executor is the platform specific module. It takes in a compiled query, and executes it. This
query is not tied to JDBC, JaSync or anything else. So we can easily change the underlying executor 
layer. 

The building of queries can be expensive. I always fail white-boarding interviews so I can't tell you
the `O` notation. But the query is represented by a data class. With a number of maps, lists, and other
properties. There is no need to re-build this data over and over, which is why we support compiling queries.
The compiled queries can even be output into JSON or Yaml so that other languages can use the query.
