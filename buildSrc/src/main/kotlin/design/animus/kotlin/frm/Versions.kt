package design.animus.kotlin.frm

object Versions {
    object Plugins {
        const val kotlin = Dependencies.kotlin
        const val gradlePluginPublish = "0.10.1"
    }

    object Dependencies {
        const val kotlin = "1.3.61"
        const val serialization = "0.14.0"
        const val coroutine = "1.3.3"
        const val vertx = "3.8.5"
        const val postgreSQL = "42.2.5"
        const val junit = "4.12"
        const val kotlinPoet = "1.4.0"
        const val jasync = "1.0.12"
        const val kotlinLogging = "1.7.9-SNAPSHOT"
        const val logback = "1.2.3"
        const val MPDataTypes = "0.1.2"
        const val mariaDBDriver = "2.5.2"
    }
}