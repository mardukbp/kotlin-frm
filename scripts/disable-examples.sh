#!/usr/bin/env bash
if [[ $(grep -E '^include ":sql:example:jvm:vertx-postgres"' settings.gradle) ]]; then sed -i s'/include ":sql:example:jvm:vertx-postgres"/\/\/include ":sql:example:jvm:vertx-postgres"/g' settings.gradle; fi
if [[ $(grep -E '^include ":sql:example:jvm:jasync-postgres"' settings.gradle) ]]; then sed -i s'/include ":sql:example:jvm:jasync-postgres"/\/\/include ":sql:example:jvm:jasync-postgres"/g' settings.gradle; fi
if [[ $(grep -E '^include ":sql:example:jvm:jasync-mariadb"' settings.gradle) ]]; then sed -i s'/include ":sql:example:jvm:jasync-mariadb"/\/\/include ":sql:example:jvm:jasync-mariadb"/g' settings.gradle; fi
if [[ $(grep -E '^include ":sql:example:jvm:vertx-mariadb"' settings.gradle) ]]; then sed -i s'/include ":sql:example:jvm:vertx-mariadb"/\/\/include ":sql:example:jvm:vertx-mariadb"/g' settings.gradle; fi
