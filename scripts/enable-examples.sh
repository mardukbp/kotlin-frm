#!/usr/bin/env bash
sed -i 's/\/\/include ":sql:example:jvm:vertx-postgres"/include ":sql:example:jvm:vertx-postgres"/g' settings.gradle
sed -i 's/\/\/include ":sql:example:jvm:jasync-postgres"/include ":sql:example:jvm:jasync-postgres"/g' settings.gradle
sed -i 's/\/\/include ":sql:example:jvm:jasync-mariadb"/include ":sql:example:jvm:jasync-mariadb"/g' settings.gradle
sed -i 's/\/\/include ":sql:example:jvm:vertx-mariadb"/include ":sql:example:jvm:vertx-mariadb"/g' settings.gradle
