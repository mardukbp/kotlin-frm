docker run -d -p 3306:3306 --name mariadb -e MYSQL_ROOT_PASSWORD=mariadb -e MYSQL_DATABASE=example -e MYSQL_USER=mariadb -e MYSQL_PASSWORD=mariadb mariadb
docker run -d --name=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=example -e POSTGRES_USER=postgres  -p 5432:5432 -v example_db:/var/lib/postgresql/data postgres
