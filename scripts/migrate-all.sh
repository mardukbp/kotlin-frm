#!/usr/bin/env bash
echo "drop schema if exists vertx_example cascade; CREATE SCHEMA IF NOT EXISTS vertx_example AUTHORIZATION postgres;"| docker exec -i postgres psql --username=postgres -w
echo "drop schema if exists jasync_example cascade; CREATE SCHEMA IF NOT EXISTS jasync_example AUTHORIZATION postgres;"| docker exec -i postgres psql --username=postgres -w
echo "drop database if exists example_jasync; create database example_jasync;" | docker exec -i mariadb mysql -u root -pmariadb
echo "drop database if exists example_vertx; create database example_vertx;" | docker exec -i mariadb mysql -u root -pmariadb
echo "Migrating"
echo "================"
./gradlew :sql:example:jvm:JaSyncPostgres:flywayMigrate
./gradlew :sql:example:jvm:VertxPostgres:flywayMigrate
./gradlew :sql:example:jvm:JaSyncMariaDB:flywayMigrate
./gradlew :sql:example:jvm:VertxMariaDB:flywayMigrate
echo "Reflecting Schema"
echo "================"
./gradlew :sql:example:jvm:JaSyncPostgres:buildSchema
./gradlew :sql:example:jvm:VertxPostgres:buildSchema
./gradlew :sql:example:jvm:JaSyncMariaDB:buildSchema
./gradlew :sql:example:jvm:VertxMariaDB:buildSchema

